import { Component,OnInit,ViewChild,ElementRef  } from '@angular/core';
import {LocalService} from '../../_services/local.service';
import {HeaderService} from '../../_services/header.service';
import {Local} from '../../_models/local';
import { PaginatedResult } from '../../_models/pagination';
import {Subscription} from 'rxjs/Subscription';
import { AuthService } from '../../_services/auth.service';
import { CookieService, CookieOptionsArgs } from 'angular2-cookie/core';

declare function require(path: string);
@Component({
  selector: 'app-side-bar',
  templateUrl: './sideBar.component.html',
  styleUrls: ['./sideBar.component.scss']
})
export class SideBarComponent implements OnInit{
  title = 'app';
  imagePath = require('../../content/img/StockItLogo.png');
  imagePathMin = require('../../content/img/StockItLogoMin.png');
  @ViewChild('myButton')  
  private myButtonRef: ElementRef;
  constructor(private headerService: HeaderService, private localService: LocalService, public authService: AuthService, public cookieService: CookieService) {}

  locals: Local[];
  selectedLocal: Local =this.cookieService.getObject("local") as Local;
  status: boolean;
  subscription:Subscription;
  isCollapsed: boolean = true;

  ngOnInit() {
    this.subscription = this.authService.authNavStatus$.subscribe(status =>{ this.status = status
      if(status){
        this.getLocals();
          }
      });

  }
  closeReportsDropdown(){
    this.myButtonRef.nativeElement.click();
  }
  changeLocal(item:Local){
    this.selectedLocal = item;
    this.localService.setLocal(this.selectedLocal);
  }
  getLocals(){
    this.localService.getLocals(this.headerService.createHeader('','','','',0,0,0,true))
    .subscribe((result :PaginatedResult<Local[]>) => {
               this.locals = result.result;
                if(this.locals.length > 0){
                    if (this.selectedLocal != null) {
                        this.selectedLocal = this.locals.find(x => x.id == this.selectedLocal.id);
                    } else {
                        this.selectedLocal =this.locals[0];
                    }
                    this.localService.setLocal(this.selectedLocal);
                }
           },
           error => {             
               console.log(<any>error);
           }
       );
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
  }
  localChange(event) {
    this.localService.setLocal(event.value);
    this.localService.selectedLocal = this.selectedLocal;
  }

  logout() {
     this.authService.logout();       
  }

  ngOnDestroy() {
    // prevent memory leak when component is destroyed
    this.subscription.unsubscribe();
  }

}


