import { Component } from '@angular/core';
declare function require(path: string);
@Component({
  selector: 'app-nav-bar',
  templateUrl: './navBar.component.html',
  styleUrls: ['./navBar.component.scss']
})
export class NavBarComponent {
  title = 'app';
  imagePath = require('../../content/img/StockItLogo.png');
}