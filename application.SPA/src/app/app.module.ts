import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavBarComponent } from './common/navBar/navBar.component';
import { LoadingComponent } from './common/loading/loading.component';
import { SideBarComponent } from './common/sideBar/sideBar.component';
import { CategoryComponent } from './components/category/category.component';
import { ProductComponent } from './components/product/product.component';
import { ProductBarCodeComponent } from './components/productBarCode/productBarCode.component';
import { ProductEditorComponent } from './components/productEditor/productEditor.component';
import { ProductDetailComponent } from './components/productDetail/productDetail.component';
import { ProductFromFileEditorComponent } from './components/productFromFileEditor/productFromFileEditor.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProviderComponent } from './components/provider/provider.component';
import { ProviderDetailComponent } from './components/providerDetail/providerDetail.component';
import { SaleComponent } from './components/sale/sale.component';
import { SaleEditorComponent } from './components/saleEditor/saleEditor.component';
import { ProviderEditorComponent } from './components/providerEditor/providerEditor.component';
import { SaleDetailComponent } from './components/saleDetail/saleDetail.component';
import { UserComponent } from './components/user/user.component';
import { UserEditorComponent } from './components/userEditor/userEditor.component';
import { LoginComponent } from './components/login/login.component';
import { ExpenseComponent } from './components/expense/expense.component';
import { StockControlEditorComponent } from './components/stockControlEditor/stockControlEditor.component';
import { StockControlComponent } from './components/stockControl/stockControl.component';
import { ProductLessStockReportComponent } from './components/reports/productLessStock/productLessStock.component';
import { ProductsReportComponent } from './components/reports/productsReport/productsReport.component';
import { ProviderDoubtsReportComponent } from './components/reports/providerDoubts/providerDoubts.component';
import { ProvidersReportComponent } from './components/reports/providersReport/providersReport.component';
import { StocktakingReportComponent } from './components/reports/stocktakingReport/stocktakingReport.component';

import {AlertifyService} from './_services/alertify.service';
import {ProductService} from './_services/product.service';
import {LocalService} from './_services/local.service';
import {HeaderService} from './_services/header.service';
import {CategoryService} from './_services/category.service';
import {ProviderService} from './_services/provider.service';
import {SaleService} from './_services/sale.service';
import {PayMethodService} from './_services/payMethod.service';
import {ProviderDoubtService} from './_services/providerDoubt.service';
import {UserService} from './_services/user.service';
import {RoleService} from './_services/role.service';
import {AuthService} from './_services/auth.service';
import {DashboardService} from './_services/dashboard.service';
import {ExpenseService} from './_services/expense.service';
import {StockControlService} from './_services/stockControl.service';

import {NavDropdownDirective,NavDropdownToggleDirective} from './_directives/sideBar.dropdown.directive';

import {ErrorInterceptor,ErrorInterceptorProvider} from './_services/error.interceptor';

import {AuthGuard} from './_guards/auth.guard';
import {AdministratorGrantGuard} from './_guards/AdministratorGrant.Guard';

import {ProductEditorResolver} from './_resolver/product.editor.resolver';
import {CategoryListResolver} from './_resolver/category.list.resolver';
import {ProviderListResolver} from './_resolver/provider.list.resolver';
import {ProviderEditorResolver} from './_resolver/provider.editor.resolver';
import {StockControlEditorResolver} from './_resolver/stockcontrol.editor.resolver';
import {SaleEditorResolver} from './_resolver/sale.editor.resolver';
import {UserEditorResolver} from './_resolver/user.editor.resolver';
import {PayMethodListResolver} from './_resolver/payMethod.list.resolver';
import {RoleListResolver} from './_resolver/role.list.resolver';
import {DashboardMetricResolver} from './_resolver/dashboard.metric.resolver';

import {RouterModule} from '@angular/router';
import {appRoutes} from './route';

import {DataTableModule} from 'primeng/datatable';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {CheckboxModule} from 'primeng/checkbox';

import { CookieService } from 'angular2-cookie/services/cookies.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { ChartsModule } from 'ng2-charts';
import { NgxBarcodeModule } from 'ngx-barcode';
import {CalendarModule} from 'primeng/calendar';
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    SideBarComponent,
    SaleComponent,
    ProviderComponent,
    CategoryComponent,
    DashboardComponent,
    ProductComponent,
    ProductDetailComponent,
    ProductEditorComponent,
    LoadingComponent,
    SaleEditorComponent,
    SaleDetailComponent,
    ProviderDetailComponent,
    ProviderEditorComponent,
    UserComponent,
    UserEditorComponent,
    LoginComponent,
    ExpenseComponent,
    StockControlComponent,
    StockControlEditorComponent,
    ProductBarCodeComponent,
    ProductFromFileEditorComponent,
    ProductLessStockReportComponent,
    ProductsReportComponent,
    ProviderDoubtsReportComponent,
    ProvidersReportComponent,
    StocktakingReportComponent,
    NavDropdownDirective,
    NavDropdownToggleDirective
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    DataTableModule,
    DropdownModule,
    ReactiveFormsModule,
    DialogModule,
    AutoCompleteModule,
    ToggleButtonModule,
    ChartsModule,
    CheckboxModule,
    NgxBarcodeModule,
    FileUploadModule,
    CalendarModule
  ],
  providers: [
    CookieService,
    ProductService,
    AlertifyService,
    HeaderService,
    LocalService,
    CategoryService,
    ProviderService,
    SaleService,
    PayMethodService,
    UserService,
    RoleService,
    AuthService,
    DashboardService,
    ExpenseService,
    ProviderDoubtService,
    StockControlService,
    ProductEditorResolver,
    CategoryListResolver,
    ProviderListResolver,
    SaleEditorResolver,
    PayMethodListResolver,
    ProviderEditorResolver,
    UserEditorResolver,
    RoleListResolver,
    DashboardMetricResolver,
    StockControlEditorResolver,
    AuthGuard,
    AdministratorGrantGuard,
    ErrorInterceptorProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
