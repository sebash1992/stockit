
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {IRole} from '../_models/role';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import { environment } from '../../environments/environment';

@Injectable()
export class RoleService extends BaseService  {
    url = environment.baseUrl + 'api/Accounts';
    constructor(public http: HttpClient) {
        super();
    }

    getRoles() {
        return this.http.get<IRole[]>(this.url+"/GetRoles",  {headers: this.addAuthHeader(new HttpHeaders()),observe: 'response'})
        .map(response => {             
            return response.body;
        });
    }
}