import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ProductList, ProductDetail} from '../_models/product';
import {IStockControl,IStockControlDetail} from '../_models/stockControl';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import { environment } from '../../environments/environment';

@Injectable()
export class StockControlService extends BaseService {
    url =  environment.baseUrl + 'api/StockControl';
    constructor(public http: HttpClient) {    
        super();
    }

    getStockControls(header: HttpHeaders) {
     header = header.append('Content-Type', 'application/json');
        const paginatedResult: PaginatedResult<IStockControl[]> = new PaginatedResult<IStockControl[]>();
        return this.http.get<IStockControl[]>(this.url,  {headers: this.addAuthHeader(header),observe:'response'})
        .map(response => {
            paginatedResult.result = response.body;
            paginatedResult.totalRows = JSON.parse(response.headers.get('Pagination')).totalItems;
            return paginatedResult;
        });
    }

     getStockControl(id): Observable<IStockControlDetail> {
         return this.http.get<IStockControlDetail>(this.url + '/GetStockControl/' + id,{headers: this.addAuthHeader(new HttpHeaders())});
     }

    createRandomStockControl(stockControl: IStockControl) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(this.url+'/CreateRandomStockControl' , stockControl, {headers: this.addAuthHeader(headers) });
    }
     createStockControl(stockControl: IStockControl) {
         let headers = new HttpHeaders().set('Content-Type', 'application/json')
         return this.http.post(this.url , stockControl, {headers: this.addAuthHeader(headers) });
     }
     removeStockControl(id: number){
        return this.http.delete<ProductDetail>(this.url + '/' + id,{headers: this.addAuthHeader(new HttpHeaders())});
    }

    save(controls: IStockControlDetail) {
         let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(this.url+'/saveStockControl' , controls, {headers: this.addAuthHeader(headers) });
    }
    finish(id: number) {
         let headers = new HttpHeaders().set('Content-Type', 'application/json')
         return this.http.post(this.url+'/finishStockControl/'+id ,null,  {headers: this.addAuthHeader(headers) });
    }
}