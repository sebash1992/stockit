
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {DashboardMetrics,DashboardChart,Stocktaking} from '../_models/dashboardMetrics';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import {LocalService} from "./local.service";
import { environment } from '../../environments/environment';
import {ProviderDoubtSearch,IProviderDoubtDetail} from '../_models/providerDoubt';

@Injectable()
export class DashboardService extends BaseService {
    url = environment.baseUrl + 'api/Dashboard';
    constructor(public http: HttpClient, private localService: LocalService) {
        super();
    }
    getLocalHeaders (){
        let header = new HttpHeaders();
        let localId = 0;
        if(this.localService.selectedLocal != null){
            localId = this.localService.selectedLocal.id;
        }
        header =   header.append('localId', '' + localId);
        return header;
    }
    getMetrics(): Observable<DashboardMetrics> {
        let header = this.getLocalHeaders();
        return this.http.get<DashboardMetrics>(this.url + '/GetMetrics',{headers: this.addAuthHeader(header)});
    }

    getSalesExpenses(period):Observable<DashboardChart>{
        let header = this.getLocalHeaders();
        return this.http.get<DashboardChart>(this.url + '/GetSalesExpenses/'+period,{headers: this.addAuthHeader(header)});
      }
    

      
    getStocktaking(period: ProviderDoubtSearch) {        
        let headers = this.getLocalHeaders(); 
        return this.http.post<Stocktaking[]>(this.url+'/GetStocktaking' , period,{headers: this.addAuthHeader(headers), observe: 'response'})
        .map(response => {
            return response.body;
        });
    }

}