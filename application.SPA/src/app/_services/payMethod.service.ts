import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {IPayMethodList} from '../_models/payMethod';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import { environment } from '../../environments/environment';

@Injectable()
export class PayMethodService extends BaseService {
    url = environment.baseUrl + 'api/PayMethod';
    constructor(public http: HttpClient) {  
        super();
    }
    getPayMethods(header: HttpHeaders) {
        const paginatedResult: PaginatedResult<IPayMethodList[]> = new PaginatedResult<IPayMethodList[]>();
        return this.http.get<IPayMethodList[]>(this.url,  {headers: this.addAuthHeader(header), observe: 'response'})
        .map(response => {
            paginatedResult.result = response.body;
            paginatedResult.totalRows = JSON.parse(response.headers.get('Pagination')).totalItems;
            return paginatedResult;
        });
    }
}