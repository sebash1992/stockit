import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {CategoryList} from '../_models/category';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import {LocalService} from "./local.service";
import { environment } from '../../environments/environment';

@Injectable()
export class CategoryService extends BaseService {
    url = environment.baseUrl + 'api/Category';
    constructor(public http: HttpClient,private localService: LocalService) {
        super();
    }
    getCategories(header: HttpHeaders) {
        const paginatedResult: PaginatedResult<CategoryList[]> = new PaginatedResult<CategoryList[]>();
        return this.http.get<CategoryList[]>(this.url,  {headers: this.addAuthHeader(header), observe: 'response'})
        .map(response => {
            paginatedResult.result = response.body;
            paginatedResult.totalRows = JSON.parse(response.headers.get('Pagination')).totalItems;
            return paginatedResult;
        });
    }
}