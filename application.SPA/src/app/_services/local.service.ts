import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Local} from '../_models/local';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import { environment } from '../../environments/environment';
import { Observable, Subject } from 'rxjs/Rx';
import { CookieService, CookieOptionsArgs } from 'angular2-cookie/core';
@Injectable()
export class LocalService extends BaseService {
    url =  environment.baseUrl + 'api/Local';
    public configObservable = new Subject<number>();
    constructor(public http: HttpClient, private cookieService: CookieService) {
        super();
    }
    selectedLocal:Local;
    locals:Local[];
    getLocals(header: HttpHeaders){
        const paginatedResult:PaginatedResult<Local[]> = new PaginatedResult<Local[]>();
        return this.http.get<Local[]>(this.url,  {headers: this.addAuthHeader(header),observe:'response'})
        .map(response => {
            paginatedResult.result = response.body;
            this.locals = response.body;
            paginatedResult.totalRows = JSON.parse(response.headers.get('Pagination')).totalItems;
            return paginatedResult;
        });            
    }
    setLocal(local:Local){
        this.selectedLocal = local;   
        let opts: CookieOptionsArgs = {
            expires: new Date(new Date().getFullYear() + 100, new Date().getMonth(), new Date().getDay())
        };
        this.cookieService.putObject("local", this.selectedLocal, opts);     
        this.configObservable.next(this.selectedLocal.id);
    }
}