import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ProductList, ProductDetail} from '../_models/product';
import {IStockEditor, IMoveStock} from '../_models/stock';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import { environment } from '../../environments/environment';
import { LocalService } from './local.service'


@Injectable()
export class ProductService extends BaseService {
    url =  environment.baseUrl + 'api/Product';
    constructor(public http: HttpClient, private localService:LocalService) {    
        super();
    }

    getProducts(header: HttpHeaders) {
     header = header.append('Content-Type', 'application/json');
        const paginatedResult: PaginatedResult<ProductList[]> = new PaginatedResult<ProductList[]>();
        return this.http.get<ProductList[]>(this.url,  {headers: this.addAuthHeader(header),observe:'response'})
        .map(response => {
            paginatedResult.totalRows = JSON.parse(response.headers.get('Pagination')).totalItems;
            paginatedResult.result = response.body;
            return paginatedResult;
        });
    }
    getProductsDetailList(header: HttpHeaders,categoryId:number, providerId:number) {
     header = header.append('Content-Type', 'application/json');
        const paginatedResult: PaginatedResult<ProductDetail[]> = new PaginatedResult<ProductDetail[]>();
        return this.http.get<ProductDetail[]>(this.url+'/GetProductsDetailList/'+categoryId+'/'+providerId,  {headers: this.addAuthHeader(header),observe:'response'})
        .map(response => {
            paginatedResult.result = response.body;
            return paginatedResult;
        });
    }

    getProductsWithLessStock() {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        headers =   headers.append('localId', '' + this.localService.selectedLocal.id);
        const paginatedResult: PaginatedResult<ProductDetail[]> = new PaginatedResult<ProductDetail[]>();
        return this.http.get<ProductDetail[]>(this.url+'/GetProductWithMinimumStock',  {headers: this.addAuthHeader(headers),observe:'response'})
        .map(response => {
            paginatedResult.result = response.body;
            return paginatedResult;
        });
    }

    getBarCode() {
        return this.http.get<string>(this.url+ '/GenerateBarcode/', {headers: this.addAuthHeader(new HttpHeaders()),observe:'response'})
        .map(response => {
            return response.body;
        });
    }

    getProductsDetail(search: string) {
        const paginatedResult: PaginatedResult<ProductDetail[]> = new PaginatedResult<ProductDetail[]>();
        return this.http.get<ProductDetail[]>(this.url+ '/GetProducts/' + search, {headers: this.addAuthHeader(new HttpHeaders()),observe:'response'})
        .map(response => {
            paginatedResult.result = response.body;
            return paginatedResult;
        });
    }
    getProduct(id): Observable<ProductDetail> {
        return this.http.get<ProductDetail>(this.url + '/GetProduct/' + id,{headers: this.addAuthHeader(new HttpHeaders())});
    }
    getProductByBarcode(barcode): Observable<ProductDetail> {
        return this.http.get<ProductDetail>(this.url+'/GetByBarCode/' + barcode,{headers: this.addAuthHeader(new HttpHeaders())});
    }

    persistProduct(product: ProductList) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(this.url , product, {headers: this.addAuthHeader(headers) });
    }
    createStock(newstock: IStockEditor) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(this.url + '/CreateStock', newstock, {headers: this.addAuthHeader(headers) });
    }

    moveStock(moveStock: IMoveStock) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(this.url + '/MoveStock', moveStock, {headers: this.addAuthHeader(headers) });
    }

    removeProduct(id: number){
        return this.http.delete<ProductDetail>(this.url + '/' + id,{headers: this.addAuthHeader(new HttpHeaders())});
    }
}