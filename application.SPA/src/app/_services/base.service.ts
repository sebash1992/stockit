import { Observable } from 'rxjs/Rx';
import { AlertifyService } from './alertify.service';
import { LocalService } from './local.service';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
export abstract class BaseService {  
    
    constructor() { }

    addAuthHeader(header:HttpHeaders ){
           let authToken = sessionStorage.getItem('auth_token');
           header = header.append('Authorization', `Bearer ${authToken}`);
        //   header = header.append('LocalId',this.localService.selectedLocal.id+);
           return header;
     }
}