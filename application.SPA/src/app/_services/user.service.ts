
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {IUser,IUserEditor} from '../_models/user';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import { environment } from '../../environments/environment';

@Injectable()
export class UserService extends BaseService {
    url = environment.baseUrl + 'api/Accounts';
    constructor(public http: HttpClient) {
        super();
    }

    getUsers(header: HttpHeaders) {
        const paginatedResult: PaginatedResult<IUser[]> = new PaginatedResult<IUser[]>();
        return this.http.get<IUser[]>(this.url+'/GetUsers',  {headers: this.addAuthHeader(header), observe: 'response'})
        .map(response => {
            paginatedResult.result = response.body;
            paginatedResult.totalRows = response.body.length;
            return paginatedResult;
        });
    }
    getUser(id): Observable<IUserEditor> {
        return this.http.get<IUserEditor>(this.url + '/GetUser/' + id,{headers: this.addAuthHeader(new HttpHeaders())});
    }
    removeUser(id: string){
        return this.http.delete<IUser>(this.url + '/' + id);
    }
    persistUser(user: IUserEditor) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        
        return this.http.post(this.url , user,{headers: this.addAuthHeader(headers) });
    }
}