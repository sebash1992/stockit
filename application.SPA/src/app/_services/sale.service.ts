
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ISale} from '../_models/sale';
import {SaleList} from '../_models/saleList';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import { environment } from '../../environments/environment';

@Injectable()
export class SaleService extends BaseService {
    url =environment.baseUrl + 'api/Sales';
    constructor(public http: HttpClient) {
        super();
    }

    getSales(header: HttpHeaders) {
        const paginatedResult: PaginatedResult<SaleList[]> = new PaginatedResult<SaleList[]>();
        return this.http.get<SaleList[]>(this.url,  {headers: this.addAuthHeader(header),observe:'response'})
        .map(response => {
            paginatedResult.result = response.body;
            paginatedResult.totalRows = JSON.parse(response.headers.get('Pagination')).totalItems;
            return paginatedResult;
        });
    }
    getSale(id): Observable<ISale> {
        return this.http.get<ISale>(this.url + '/' + id,{headers: this.addAuthHeader(new HttpHeaders())});
    }

    persistSale(sale: ISale) {
       let headers = new HttpHeaders().set('Content-Type', 'application/json')
       return this.http.post(this.url , sale, {headers: this.addAuthHeader(headers) });
    }
}