
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ProviderDoubt,IProviderDoubt,ProviderDoubtSearch,IProviderDoubtDetail} from '../_models/providerDoubt';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import { environment } from '../../environments/environment';

@Injectable()
export class ProviderDoubtService extends BaseService {
    url = environment.baseUrl + 'api/ProviderDoubt';
    constructor(public http: HttpClient) {
        super();
    }

    persistDoubt(doubt: ProviderDoubt) {
         let headers = new HttpHeaders().set('Content-Type', 'application/json')
         return this.http.post(this.url , doubt, {headers: this.addAuthHeader(headers) });
    }
    getProviderDoubt(provider: ProviderDoubtSearch) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')  
        return this.http.post<IProviderDoubtDetail[]>(this.url+'/GetProviderDubt' , provider,{headers: this.addAuthHeader(headers), observe: 'response'})
        .map(response => {
            return response.body;
        });
    }
}