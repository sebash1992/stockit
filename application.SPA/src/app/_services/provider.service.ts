
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ProviderList} from '../_models/providerList';
import {ProviderDoubt,ProviderDoubtSearch,IProviderDoubtDetail} from '../_models/providerDoubt';
import {Provider} from '../_models/provider';
import { PaginatedResult } from '../_models/pagination';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import { environment } from '../../environments/environment';

@Injectable()
export class ProviderService extends BaseService {
    url = environment.baseUrl + 'api/Provider';
    constructor(public http: HttpClient) {
        super();}

    getProviders(header: HttpHeaders) {
        const paginatedResult: PaginatedResult<ProviderList[]> = new PaginatedResult<ProviderList[]>();
        return this.http.get<ProviderList[]>(this.url,   {headers: this.addAuthHeader(header), observe: 'response'})
        .map(response => {
            paginatedResult.result = response.body;
            paginatedResult.totalRows = JSON.parse(response.headers.get('Pagination')).totalItems;
            return paginatedResult;
        });
    }
    getProvidersDetailList() {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')    
        const paginatedResult: PaginatedResult<Provider[]> = new PaginatedResult<Provider[]>();
        return this.http.get<Provider[]>(this.url+'/GetProviders',   {headers: this.addAuthHeader(headers), observe: 'response'})
        .map(response => {
            paginatedResult.result = response.body;
            return paginatedResult;
        });
    }
    getProvider(id): Observable<Provider> {
        return this.http.get<Provider>(this.url + '/GetProvider/' + id,{headers: this.addAuthHeader(new HttpHeaders())});
    }
    removeProvider(id: number){
        return this.http.delete<Provider>(this.url + '/' + id,{headers: this.addAuthHeader(new HttpHeaders())});
    }
    persistProvider(provider: Provider)
     {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')        
        return this.http.post(this.url , provider,  {headers: this.addAuthHeader(headers)});
    }

    getProviderDoubt(provider: ProviderDoubtSearch) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')  
        return this.http.post<IProviderDoubtDetail[]>(this.url+'/GetProviderDubt' , provider,{headers: this.addAuthHeader(headers), observe: 'response'})
        .map(response => {
            return response.body;
        });
    }
}