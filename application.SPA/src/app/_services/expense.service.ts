import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {BaseService} from "./base.service";
import { environment } from '../../environments/environment';
import { Expense, IExpense, IMonthlyExpense, MonthlyExpense} from '../_models/expense';
import {PaginatedResult} from '../_models/pagination';

@Injectable()
export class ExpenseService extends BaseService {
    url =  environment.baseUrl + 'api/Expense';
    constructor(public http: HttpClient) {    
        super();
    }

    getExpenses(header: HttpHeaders) {
     header = header.append('Content-Type', 'application/json');
        const paginatedResult: PaginatedResult<IExpense[]> = new PaginatedResult<IExpense[]>();
        return this.http.get<IExpense[]>(this.url,  {headers: this.addAuthHeader(header),observe:'response'})
        .map(response => {
            paginatedResult.result = response.body;
            paginatedResult.totalRows = JSON.parse(response.headers.get('Pagination')).totalItems;
            return paginatedResult;
        });
    }  
    getMonthlyExpenses(header: HttpHeaders) {
     header = header.append('Content-Type', 'application/json');
        const paginatedResult: PaginatedResult<IMonthlyExpense[]> = new PaginatedResult<IMonthlyExpense[]>();
        return this.http.get<IMonthlyExpense[]>(this.url+"/GetMonthlyExpense",  {headers: this.addAuthHeader(header),observe:'response'})
        .map(response => {
            paginatedResult.result = response.body;
            paginatedResult.totalRows = JSON.parse(response.headers.get('Pagination')).totalItems;
            return paginatedResult;
        });
    }

    createMonthlyExpense(entity: IMonthlyExpense) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(this.url + '/CreateME' , entity, {headers: this.addAuthHeader(headers) });
    }
    payAll() {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(this.url +'/PayAllMonthlyExpense', {headers: this.addAuthHeader(headers) });
    }
    payMonthlyExpense(entity: IMonthlyExpense) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(this.url+'/PayMonthlyExpense', entity, {headers: this.addAuthHeader(headers) });
    }
    removeMonthlyExpense(entity: IMonthlyExpense) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(this.url+'/RemoveMonthlyExpense', entity, {headers: this.addAuthHeader(headers) });
    }
    createExpense(entity: IExpense) {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
        return this.http.post(this.url, entity, {headers: this.addAuthHeader(headers) });
    }
}