import { Component, OnInit } from '@angular/core';
import {SaleService} from '../../_services/sale.service';
import {HeaderService} from '../../_services/header.service';
import {ISale} from '../../_models/sale';
import {ISaleDetail, SaleDetail,SaleServiceDetail} from '../../_models/saleDetail';
import {ProductDetail} from '../../_models/product';
import {PaginatedResult} from '../../_models/pagination';
import {IPayMethodList} from '../../_models/payMethod';
import {HttpHeaders} from '@angular/common/http';
import {FormBuilder, FormGroup, FormControl, Validators, NgForm} from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import {LocalService} from '../../_services/local.service';
import {AlertifyService} from '../../_services/alertify.service';
import {ProductService} from '../../_services/product.service';

@Component({
  selector: 'app-saleEditor',
  templateUrl: './saleEditor.component.html',
  styleUrls: ['./saleEditor.component.scss']
})
export class SaleEditorComponent implements OnInit {
  quantityToSale: number = 1;
  productbarCode: string;
  saleToEdit: ISale;  
  saleElements: ISaleDetail[];
  payMethods: IPayMethodList[];
  selectedPayMethod: IPayMethodList;
  searchedProducts: ProductDetail[];
  searchedProduct: ProductDetail;
  isLoading: boolean = false;
  public saleServiceForm: FormGroup; 
  displaySaleServicekModal:boolean = false;

    constructor(private frmbuilder: FormBuilder, private router: ActivatedRoute, private route: Router,
              private localService: LocalService, private saleService: SaleService,private alertifyService: AlertifyService,
              private productService: ProductService){
            this.localService.configObservable.subscribe(value => {
                    this.route.navigate(['/sales']);
            });
            this.saleServiceForm = frmbuilder.group({
                'price': [0, Validators.compose([Validators.required])],
                'service': ["", Validators.compose([Validators.required, Validators.maxLength(128)])],                
            });
    }

    ngOnInit() {
        this.router.data.subscribe(data => {
           this.saleToEdit = data['sale'];
           this.payMethods = data['payMethods'].result;
           if(this.payMethods.length > 0){
               this.selectedPayMethod = this.payMethods[0];
           }
           if(this.saleToEdit.id != 0){
            this.selectedPayMethod = this.payMethods.filter(s => s.id == this.saleToEdit.payMethod)[0];
           }
        });
        
    }

    addToSale() {
        let barcode = this.productbarCode;
        let quantity = this.quantityToSale;
        this.productService.getProductByBarcode(barcode).subscribe(
            result => {
            this.addProductToSale(result);
          },
            error => {
                console.log(<any>error);
            }
        );
    }

    showServiceDialog(event){
        this.displaySaleServicekModal = true
    }

    addService(service:SaleServiceDetail){
        let saleSerDetail = new SaleServiceDetail(
                service.service,
                service.price
        );
        this.saleToEdit.subtotal += service.price;
        this.saleToEdit.servicesSent[this.saleToEdit.servicesSent.length] = saleSerDetail;
        this.displaySaleServicekModal = false;
    }

    addProductToSale(product:ProductDetail) {
        let barcode = product.barCode;
        let quantity = this.quantityToSale;
         if(product != null){
                let stocks = product.stocks.filter(stock => stock.localId === this.localService.selectedLocal.id);
                let localQuantity = 0;
                if (stocks.length > 0){
                  localQuantity = product.stocks.filter(stock => stock.localId === this.localService.selectedLocal.id)[0].quantity;
                }
              let productInlist =  this.saleToEdit.productsSent.filter(product => product.productBarCode === barcode)
              if (productInlist.length > 0) {
                if (productInlist[0].quantity + quantity < localQuantity) {
                    this.saleToEdit.productsSent.filter(product => product.productBarCode === barcode)[0].quantity += quantity;
                    this.saleToEdit.subtotal += quantity * productInlist[0].unityPrice;
                }else{
                  this.alertifyService.warning('No hay tantos articulos de ' + product.name);
                }
              }else{
                if (localQuantity >= quantity) {
                    let saleDetail = new SaleDetail(
                        product.barCode,
                        product.id,
                        product.name,
                        quantity,
                        product.salePrice                        
                    );
                    this.saleToEdit.subtotal += quantity * product.salePrice;
                    this.saleToEdit.productsSent[this.saleToEdit.productsSent.length] = saleDetail;
                    ;
                    this.quantityToSale = 1;
                    this.productbarCode = "";
                } else {
                    this.alertifyService.warning('No hay tantos articulos de ' + product.name);
                }
              } 
            }else{
            this.alertifyService.warning('No existe el producto ' + barcode);
          }
      }
      searchProduct(event) {
          this.productService.getProductsDetail(event.query)
          .subscribe((result:PaginatedResult<ProductDetail[]>) => {
              this.searchedProducts = result.result;
          },
          error => {
              console.log(<any>error);
          }
      );
  }
  productSelection(event){
    this.addProductToSale(event);
    this.searchedProduct = null;
  }
  //contacto visual
  submitSale(){
        this.isLoading = true;
        let productSold =[];
        if (this.saleToEdit.productsSent.length > 0 || this.saleToEdit.servicesSent.length > 0) {
            this.saleToEdit.payMethod = this.selectedPayMethod.id;
            this.saleToEdit.discount = this.saleToEdit.discount;
            this.saleToEdit.subtotal = this.saleToEdit.subtotal;
            this.saleToEdit.date = new Date;
            this.saleToEdit.localId = this.localService.selectedLocal.id;

            this.saleService.persistSale(this.saleToEdit).subscribe(
                resp => {
                    this.route.navigate(['/sales']);
                },
                err => {
                       this.alertifyService.error('Ocurrio un Error al realizar la venta');                    
                });      
        } else {
            this.alertifyService.warning('No hay elementos en la venta');
        }
        this.isLoading = false;
  }
  removeItemFromSale(barcode: string) {
        let element = this.saleToEdit.productsSent.find(i => i.productBarCode == barcode);
        this.saleToEdit.productsSent.splice(this.saleToEdit.productsSent.indexOf(element), 1);
        this.saleToEdit.subtotal -= element.quantity * element.unityPrice;
    }
    removeServiceFromSale(service: string) {
        let element = this.saleToEdit.servicesSent.find(i => i.service == service);
        this.saleToEdit.servicesSent.splice(this.saleToEdit.servicesSent.indexOf(element), 1);
        this.saleToEdit.subtotal -=  element.price;
    }
}