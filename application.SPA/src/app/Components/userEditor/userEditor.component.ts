import {IUserEditor} from '../../_models/User';
import {IRole} from '../../_models/role';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../../_services/header.service';
import {UserService} from '../../_services/user.service';
import {AlertifyService} from '../../_services/alertify.service';
import { ActivatedRoute,Router } from '@angular/router';
import {FormBuilder, FormGroup, FormControl, Validators, NgForm} from '@angular/forms';

@Component({
  selector: 'app-userEditor',
  templateUrl: './userEditor.component.html',
  styleUrls: ['./userEditor.component.scss']
})
export class UserEditorComponent  implements OnInit {
   isloading:boolean = false;
  usertoEdit: IUserEditor;
  roles: IRole[];
  selectedRole: IRole;
  public userEditorForm: FormGroup;


  constructor(private frmbuilder: FormBuilder, private router: ActivatedRoute, private route: Router,
               private userService: UserService,private alertifyService: AlertifyService){
  }
  ngOnInit() {
        this.router.data.subscribe(data => {
        this.usertoEdit = data['user'];
        this.roles = data['roles'];
        if (this.roles.length > 0) {
          this.selectedRole = this.roles[0];
        }
        if(this.usertoEdit.id != null){
          this.userEditorForm = this.frmbuilder.group({
            'firstName': [this.usertoEdit.firstName, Validators.compose([Validators.required, Validators.maxLength(64)])],
            'lastName': [this.usertoEdit.lastName,  Validators.compose([Validators.required, Validators.maxLength(64)])],
            'email': [this.usertoEdit.email, Validators.compose([Validators.maxLength(128)])],
          });
        }else{
          this.userEditorForm = this.frmbuilder.group({
            'firstName': [this.usertoEdit.firstName, Validators.compose([Validators.required, Validators.maxLength(64)])],
            'lastName': [this.usertoEdit.lastName,  Validators.compose([Validators.required, Validators.maxLength(64)])],
            'email': [this.usertoEdit.email, Validators.compose([Validators.maxLength(128)])],
            'password': ["", Validators.compose([Validators.required, Validators.minLength(5)])],
            'confirmPassword': ["", Validators.compose([Validators.required])],
          }, { validator: this.checkPasswords });
        }

        });
    }
    checkPasswords(group: FormGroup) {
       return group.get('password').value === group.get('confirmPassword').value ? null : { 'notSame': true };
    
    }
    editUser(value) {
      this.isloading = true;
    this.usertoEdit.firstName = value.firstName;
    this.usertoEdit.lastName = value.lastName;
    this.usertoEdit.email = value.email;
    this.usertoEdit.role = this.selectedRole.name;
    if(this.usertoEdit.id == null){
    this.usertoEdit.password = value.password;

    }

    this.userService.persistUser(this.usertoEdit).subscribe(
              Cat => {
                  if(this.usertoEdit.id == null){
                    this.alertifyService.success("Usuario " + this.usertoEdit.email + " creado correctamente.")
                  }else{
                    this.alertifyService.success("Usuario " + this.usertoEdit.email + " editado correctamente.")
                  }
                  this.isloading = false;
                  this.route.navigate(['/users']); 
              },
              err => {
                  this.isloading = false;
                    for (var v in err.error) // for acts as a foreach  
                    {  
                        alert(err.error[v]);  
                        if(err.error[v].code == "DuplicateUserName"){
                          this.userEditorForm.controls['email'].setErrors({'exist': true});
                        }
                    }  
              });
  }

}