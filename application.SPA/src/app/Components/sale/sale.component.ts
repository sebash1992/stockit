import { Component, OnInit } from '@angular/core';
import {SaleService} from '../../_services/sale.service';
import {HeaderService} from '../../_services/header.service';
import {SaleList} from '../../_models/saleList';
import {Sale} from '../../_models/sale';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import { ActivatedRoute,Router } from '@angular/router';
import {LocalService} from '../../_services/local.service';
import {AlertifyService} from '../../_services/alertify.service';
import {LazyLoadEvent} from 'primeng/primeng';
@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.scss']
})
export class SaleComponent {

  sales: SaleList[];
  selectedSale: SaleList;
  selectedSaleDetail: Sale;
  isloading: boolean;
  totalRecords: number = 0;
  selectedPage:number;
      constructor(private router: ActivatedRoute, private route: Router, private headerService: HeaderService,
              private localService: LocalService, private saleService: SaleService,private alertifyService: AlertifyService){
            this.localService.configObservable.subscribe(value => {
            this.getSales(this.headerService.createHeader('','','','',this.localService.selectedLocal.id,0,10,true));
        })
    }

  ngOnInit() {
    this.isloading = false;
  }
  onRowSelect(event) {
            this.saleService.getSale(this.selectedSale.id).subscribe(
            result => {
                 this.selectedSaleDetail = result;
            },
            error => {
                console.log(<any>error);
            }
        );
  }
    doubleClick(event) {
         this.route.navigate(['/newSale/'+ this.selectedSale.id]);
    }
  getSales(header: HttpHeaders){
    this.setLoading(true);
    this.saleService.getSales(header)
    .subscribe((result:PaginatedResult<SaleList[]>) => {
                this.sales = result.result;
                if (this.sales.length > 0) {
                  this.selectedSale = this.sales[0];
                  this.onRowSelect(null);
                }
                this.totalRecords = result.totalRows;
                this.setLoading(false);
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );
  }

  changePage(event: LazyLoadEvent) {
    this.isloading = true;
    this.selectedPage = (event.first / event.rows) + 1;
    let sortField = '';
    let sortOrder = '';
    if( event.sortField != null){
      sortField = event.sortField;
      if(sortOrder == '1'){
        sortOrder = "ASC";
      }else{
        sortOrder = "DESC";
      }
    }
    this.getSales(this.headerService.createHeader('','',sortField,sortOrder,0, this.selectedPage,event.rows,true));
    this.isloading = false;
  }

  reloadGrid(reload:boolean){
    this.getSales(this.headerService.createHeader('','','','',0, this.selectedPage,10,true));
  }

  setLoading(newState:boolean){
    this.isloading = newState;
  }

}