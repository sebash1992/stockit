import {ProviderList} from '../../../_models/providerList';
import {Provider} from '../../../_models/provider';
import {PaginatedResult} from '../../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../../../_services/header.service';
import {ProviderService} from '../../../_services/provider.service';
import {AlertifyService} from '../../../_services/alertify.service';
import {LocalService} from '../../../_services/local.service';
import {AuthService} from '../../../_services/auth.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  templateUrl: './providersReport.component.html',
  styleUrls: ['./providersReport.component.scss']
})
export class ProvidersReportComponent implements OnInit {
  isloading:boolean = false;
  providers: Provider[];
  constructor(private providerService: ProviderService) {

  }

  ngOnInit() {
    this.getProviders(); 
  }
  setLoading(newState:boolean){
    this.isloading = newState;
  }
  getProviders(){
    this.setLoading(true);
    this.providerService.getProvidersDetailList()
    .subscribe((result:PaginatedResult<Provider[]>) => {
                this.providers = result.result;
                this.setLoading(false);
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );
  }
  
 
}