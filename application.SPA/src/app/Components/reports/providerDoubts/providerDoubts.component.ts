import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import {ProviderList,ProviderL} from '../../../_models/providerList';
import {ProviderDoubtSearch,IProviderDoubtDetail,IProviderDoubt} from '../../../_models/providerDoubt';
import {ProviderDoubtService} from '../../../_services/providerDoubt.service';


@Component({
  templateUrl: './providerDoubts.component.html',
  styleUrls: ['./providerDoubts.component.scss']
})
export class ProviderDoubtsReportComponent implements OnInit {
  isloading:boolean = false;
  providers: ProviderList[];
  selectedProvider: ProviderList;
    to: Date;

    from: Date;

    maxDate: Date;

    es: any;
    doubt : IProviderDoubtDetail[];
    previousDoubt : IProviderDoubtDetail;

  constructor(private providerDoubtService: ProviderDoubtService, private router: ActivatedRoute, private route: Router) {

  }
    ngOnInit() {
      this.router.data.subscribe(data => {
          this.providers = data['providers'].result;
          if (this.providers.length > 0) {
            this.selectedProvider = this.providers[0];
          }
      
        this.es = {
            firstDayOfWeek: 1,
            dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
            dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
            dayNamesMin: [ "D","L","M","X","J","V","S" ],
            monthNames: [ "enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre" ],
            monthNamesShort: [ "ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic" ],
            today: 'Hoy',
            clear: 'Borrar'
        }
        this.to = new Date();
        this.from = new Date();
        this.maxDate = new Date();
        this.getDoubt();
     });
    }
    getDoubt(){
      let search = new ProviderDoubtSearch(this.from,this.to,this.selectedProvider.id)
    this.providerDoubtService.getProviderDoubt(search)
    .subscribe((result:IProviderDoubtDetail[]) => {
        this.previousDoubt = result[0];
        result.shift();
        this.doubt = result;
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );

    }
  getTotal(index:number){
      var subtotal = this.previousDoubt.ammount;
    for (let i = 0; i<= index;i++){
      subtotal += this.doubt[i].ammount;    
  }
  return subtotal;  
}

  setLoading(newState:boolean){
    this.isloading = newState;
  }
}
 