import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import {ProviderDoubtSearch,IProviderDoubtDetail,IProviderDoubt} from '../../../_models/providerDoubt';
import {DashboardService} from '../../../_services/dashboard.service';
import {Stocktaking} from '../../../_models/dashboardMetrics';
@Component({
  templateUrl: './stocktakingReport.component.html',
  styleUrls: ['./stocktakingReport.component.scss']
})
export class StocktakingReportComponent implements OnInit {
 isloading:boolean = false;
    to: Date;

    from: Date;

    maxDate: Date;

    es: any;
    stocktaking :Stocktaking[];
  constructor(private dashboardService: DashboardService, private router: ActivatedRoute, private route: Router) {

  }
    ngOnInit() {
     
        this.es = {
            firstDayOfWeek: 1,
            dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
            dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
            dayNamesMin: [ "D","L","M","X","J","V","S" ],
            monthNames: [ "enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre" ],
            monthNamesShort: [ "ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic" ],
            today: 'Hoy',
            clear: 'Borrar'
        }
        this.to = new Date();
        this.from = new Date();
        this.maxDate = new Date();
       // this.getDoubt();
     
    }
    
    getStockTaking(){
      let search = new ProviderDoubtSearch(this.from,this.to,0)
    this.dashboardService.getStocktaking(search)
    .subscribe((result:Stocktaking[]) => {
          this.stocktaking = result;
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );

    }

  setLoading(newState:boolean){
    this.isloading = newState;
  }
}