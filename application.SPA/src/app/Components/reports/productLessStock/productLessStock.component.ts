import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import {HeaderService} from '../../../_services/header.service';
import {ProductService} from '../../../_services/product.service';
import {ProductList, ProductDetail} from '../../../_models/product';
import {PaginatedResult} from '../../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';

@Component({
  templateUrl: './productLessStock.component.html',
  styleUrls: ['./productLessStock.component.scss']
})
export class ProductLessStockReportComponent implements OnInit {
  isloading:boolean = false;
  products: ProductDetail[];
  constructor(private productService: ProductService,private headerService: HeaderService, private router: ActivatedRoute, private route: Router,) {

  }

  ngOnInit() {
     this.getProducts();

  }

    getProducts(){
     let header = this.headerService.createHeader('','','','',0, 0,0,true);
    this.setLoading(true);
    this.productService.getProductsWithLessStock()
    .subscribe((result:PaginatedResult<ProductDetail[]>) => {
                this.products = result.result;
                this.setLoading(false);
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );
  }
    setLoading(newState:boolean){
    this.isloading = newState;
  }
 
}