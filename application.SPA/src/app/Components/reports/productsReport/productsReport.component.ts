import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import {HeaderService} from '../../../_services/header.service';
import {ProductService} from '../../../_services/product.service';
import {ProductList, ProductDetail} from '../../../_models/product';
import {PaginatedResult} from '../../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {CategoryList, Category} from '../../../_models/category';
import {ProviderList,ProviderL} from '../../../_models/providerList';
@Component({
  templateUrl: './productsReport.component.html',
  styleUrls: ['./productsReport.component.scss']
})
export class ProductsReportComponent implements OnInit {
  isloading:boolean = false;
  categoryId:number = 0;
  providerId:number = 0;
  products: ProductDetail[];
  categories: CategoryList[] =  [ {id: 0, name: "Todas"} ];
  selectedCategory: Category;
  providers: ProviderList[] =  [ {id: 0, name: "Todos"} ];;
  selectedProvider: ProviderList;
  constructor(private productService: ProductService,private headerService: HeaderService, private router: ActivatedRoute, private route: Router) {

  }

  ngOnInit() {
    this.router.data.subscribe(data => {
        this.categories = this.categories.concat(data['categories'].result);
        if (this.categories.length > 0) {
          this.selectedCategory = this.categories[0];
        }
        this.providers = this.providers.concat(data['providers'].result);
        if (this.providers.length > 0) {
          this.selectedProvider = this.providers[0];
        }
     this.getProducts();
    });
  }

    getProducts(){
      let header = this.headerService.createHeader('','','','',0, 0,0,true);
    this.setLoading(true);
    this.productService.getProductsDetailList(header, this.selectedCategory.id,this.selectedProvider.id)
    .subscribe((result:PaginatedResult<ProductDetail[]>) => {
                this.products = result.result;
                this.setLoading(false);
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );
  }
    setLoading(newState:boolean){
    this.isloading = newState;
  }
 
}