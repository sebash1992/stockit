import {IUser} from '../../_models/User';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../../_services/header.service';
import {UserService} from '../../_services/user.service';
import {AlertifyService} from '../../_services/alertify.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent  implements OnInit {
  isloading:boolean = false;
  users: IUser[];
  selectedUser: IUser;
  //selectedProviderDetail: Provider;
  totalRecords: number;
  selectedPage:number;
  constructor(private headerService: HeaderService, private userService: UserService,private alertifyService: AlertifyService,private route: Router) {}

  ngOnInit() {
    this.isloading = false;
    this.getUsers(this.headerService.createHeader('','','','',0, this.selectedPage,10,true));
  }
  onRowSelect(event) {
            this.userService.getUser(this.selectedUser.id).subscribe(
            result => {
                 //this.selectedProviderDetail = result;
            },
            error => {
                console.log(<any>error);
            }
        );
  }

  getUsers(header: HttpHeaders){
    this.setLoading(true);
    this.userService.getUsers(header)
    .subscribe((result:PaginatedResult<IUser[]>) => {
                this.users = result.result;
                if (this.users.length > 0) {
                  this.selectedUser = this.users[0];
                 // this.onRowSelect(null);
                }
                this.totalRecords = result.totalRows;
                this.setLoading(false);
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );
  }

  removeUser(){
       this.setLoading(true);
        this.userService.removeUser(this.selectedUser.id)
       .subscribe(result => {
             this.getUsers(this.headerService.createHeader('','','','',0, this.selectedPage,10,true));
           },
           error => {
              this.alertifyService.error("Problema al eliminar el usuario.");
              this.setLoading(false);
               console.log(<any>error);
           }
       );
  }
  reloadGrid(reload:boolean){
    this.getUsers(this.headerService.createHeader('','','','',0, this.selectedPage,10,true));
  }

  setLoading(newState:boolean){
    this.isloading = newState;
  }
  doubleClick(event) {
    this.route.navigate(['/newUser/'+ this.selectedUser.id]);
  }


}