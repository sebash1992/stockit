import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../../_services/header.service';
import {ExpenseService} from '../../_services/expense.service';
import {AlertifyService} from '../../_services/alertify.service';
import {LocalService} from '../../_services/local.service';
import {AuthService} from '../../_services/auth.service';
import { ActivatedRoute,Router } from '@angular/router';
import {FormBuilder, FormGroup, FormControl, Validators, NgForm} from '@angular/forms';
import { Expense, IExpense, IMonthlyExpense, MonthlyExpense} from '../../_models/expense';
@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.scss']
})
export class ExpenseComponent  implements OnInit {
  isloading:boolean = false;  
  public newExpenseForm: FormGroup; 
  isMonthly: boolean = false;
  expenses: Expense[];
  monthlyExpenses: IMonthlyExpense[];
  totalRecords: number;
  selectedPage:number;
  monthlyTotalRecords:number;
  constructor(private headerService: HeaderService,private localService:LocalService, private expenseService:ExpenseService, private alertifyService: AlertifyService,private route: Router,private fb: FormBuilder, public authService:AuthService) 
  {
            this.localService.configObservable.subscribe(value => {
            this.getExpenses(this.headerService.createHeader('','','','',this.localService.selectedLocal.id, 0,10,true));
            this.getMonthlyExpenses(this.headerService.createHeader('','','','',this.localService.selectedLocal.id,0,10,true));
            });


  }

  ngOnInit() {
    this.isloading = false;
    this.newExpenseForm = this.fb.group({
    amount: ['', Validators.required],
    description: ['', Validators.maxLength(128)],
    });      
  }
    changePage(event: LazyLoadEvent) {
    this.isloading = true;
    this.selectedPage = (event.first / event.rows) + 1;
    let sortField = '';
    let sortOrder = '';
    if( event.sortField != null){
      sortField = event.sortField;
      if(sortOrder == '1'){
        sortOrder = "ASC";
      }else{
        sortOrder = "DESC";
      }
    }
    this.getExpenses(this.headerService.createHeader('','',sortField,sortOrder,this.localService.selectedLocal.id, this.selectedPage,event.rows,true));
    this.isloading = false;
  }

  changeMonthlyPage(event: LazyLoadEvent) {
    this.isloading = true;
    this.selectedPage = (event.first / event.rows) + 1;
    let sortField = '';
    let sortOrder = '';
    if( event.sortField != null){
      sortField = event.sortField;
      if(sortOrder == '1'){
        sortOrder = "ASC";
      }else{
        sortOrder = "DESC";
      }
    }
    this.getMonthlyExpenses(this.headerService.createHeader('','',sortField,sortOrder,this.localService.selectedLocal.id, this.selectedPage,event.rows,true));
    this.isloading = false;
  }



  getExpenses(header: HttpHeaders){
    this.setLoading(true);
    this.expenseService.getExpenses(header)
    .subscribe((result:PaginatedResult<IExpense[]>) => {
                this.expenses = result.result;
                this.totalRecords = result.totalRows;
                this.setLoading(false);
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );
  }
  
    getMonthlyExpenses(header: HttpHeaders){
    this.setLoading(true);
    this.expenseService.getMonthlyExpenses(header)
    .subscribe((result:PaginatedResult<IMonthlyExpense[]>) => {
                this.monthlyExpenses = result.result;
                this.monthlyTotalRecords = result.totalRows;
                this.setLoading(false);
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );
  }
  setLoading(newState:boolean){
    this.isloading = newState;
  }

    paidAll() {
      this.setLoading(true);
          this.expenseService.payAll()
          .subscribe(res=>{
                      this.getExpenses(this.headerService.createHeader('','','','',this.localService.selectedLocal.id, this.selectedPage,10,true));
                },
                error => {
                    this.alertifyService.error("Hubo un problema al pagar los gastos.")
                    console.log(<any>error);
                    this.setLoading(false);
                }
            );
    }

    paid(expense) {
      this.setLoading(true);
          this.expenseService.payMonthlyExpense(expense)
          .subscribe(res=>{
                      this.getExpenses(this.headerService.createHeader('','','','',this.localService.selectedLocal.id, this.selectedPage,10,true));
                },
                error => {
                    this.alertifyService.error("Hubo un problema al pagar el gasto.")
                    console.log(<any>error);
                    this.setLoading(false);
                }
            );    
    }
    

    remove(expense) {
      this.setLoading(true);
          this.expenseService.removeMonthlyExpense(expense)
          .subscribe(res=>{
                      this.getMonthlyExpenses(this.headerService.createHeader('','','','',this.localService.selectedLocal.id, this.selectedPage,10,true));
                },
                error => {
                    console.log(<any>error);
                    this.alertifyService.error("Hubo un problema al eliminar el gasto.")
                    this.setLoading(false);
                }
            );    
        
  }
  

      submitnewExpense(value: any) {
        if(value.amount <= 0){
          this.alertifyService.error("El Valor tiene que ser mayor a 0.")
        }else{
          if(value.description.trim()!= ""){
          this.isloading = true;
          if (this.isMonthly) {
              let expense = new MonthlyExpense(0, value.amount, value.description, this.localService.selectedLocal.id);
              this.expenseService.createMonthlyExpense(expense).subscribe(
                  res => {
                      this.isloading = false;
                      this.getMonthlyExpenses(this.headerService.createHeader('','','','',this.localService.selectedLocal.id, this.selectedPage,10,true));
                      this.newExpenseForm.reset();
                  },
                  err => {
                      this.isloading = false;
                  });
          } else {
              let expense = new Expense(0, new Date(), value.amount, value.description, false,this.localService.selectedLocal.id);
              this.expenseService.createExpense(expense).subscribe(
                  res => {
                      this.isloading = false;
                      this.getExpenses(this.headerService.createHeader('','','','',this.localService.selectedLocal.id, this.selectedPage,10,true));
                      this.newExpenseForm.reset();
                  },
                  err => {
                      this.isloading = false;
                  });
          }
        }else{
          this.alertifyService.error("Debe agregar una descripcion.")
        }
        
      }
    }
}