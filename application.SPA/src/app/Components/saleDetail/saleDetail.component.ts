import { Component, OnInit,Input } from '@angular/core';
import {SaleService} from '../../_services/sale.service';
import {HeaderService} from '../../_services/header.service';
import {SaleList} from '../../_models/saleList';
import {Sale} from '../../_models/sale';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import { ActivatedRoute,Router } from '@angular/router';
import {LocalService} from '../../_services/local.service';
import {AlertifyService} from '../../_services/alertify.service';
@Component({
  selector: 'app-sale-detail',
  templateUrl: './saleDetail.component.html',
  styleUrls: ['./saleDetail.component.scss']
})
export class SaleDetailComponent {
  @Input()saleSelected: Sale;
  
    constructor(private router: ActivatedRoute, private route: Router, private headerService: HeaderService,
              private localService: LocalService, private saleService: SaleService,private alertifyService: AlertifyService){
    }

  ngOnInit() {
  }

}