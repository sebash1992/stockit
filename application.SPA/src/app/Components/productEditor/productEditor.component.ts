import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../_services/product.service';
import {HeaderService} from '../../_services/header.service';
import {ProductDetail} from '../../_models/product';
import {StockDetail} from '../../_models/stock';
import {CategoryList, Category} from '../../_models/category';
import {ProviderList,ProviderL} from '../../_models/providerList';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import {FormBuilder, FormGroup, FormControl, Validators, NgForm} from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import {LocalService} from '../../_services/local.service';
import {AlertifyService} from '../../_services/alertify.service';

@Component({
  selector: 'app-product-editor',
  templateUrl: './productEditor.component.html',
  styleUrls: ['./productEditor.component.scss']
})
export class ProductEditorComponent implements OnInit {
 isloading:boolean = false;
 producttoEdit: ProductDetail;
 categories: CategoryList[];
 selectedCategory: Category;
 providers: ProviderList[];
 selectedProvider: ProviderList;

 public productEditorForm: FormGroup;

  constructor(private frmbuilder: FormBuilder, private router: ActivatedRoute, private route: Router,
              private localService: LocalService, private productService: ProductService,private alertifyService: AlertifyService){
          this.localService.configObservable.subscribe(value => {
                this.route.navigate(['/products']); 
            });
  }

  ngOnInit() {
        this.router.data.subscribe(data => {
        this.producttoEdit = data['product'];
        this.categories = data['categories'].result;
        if (this.categories.length > 0) {
          this.selectedCategory = this.categories[0];
        }
        this.providers = data['providers'].result;
        if (this.providers.length > 0) {
          this.selectedProvider = this.providers[0];
        }
        this.productEditorForm = this.frmbuilder.group({
          'barCode': [this.producttoEdit.barCode, Validators.compose([Validators.required, Validators.maxLength(128)])],
          'name': [this.producttoEdit.name,  Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(64)])],
          'description': [this.producttoEdit.description, Validators.compose([Validators.maxLength(128)])],
          'category': null,
          'provider': null,
          'productLink': this.producttoEdit.productLink,
          'salePrice': this.producttoEdit.salePrice,
          'minQuantity': this.producttoEdit.minQuantity,
          'costPrice': 0,
          'quantity': 0
        });
        this.productService.getBarCode().subscribe(
          Cat => {
            this.productEditorForm.controls["barCode"].setValue(Cat);
          },
          err => {
          });
        });
    }
  
  editProduct(value) {
    this.isloading = true;
   if (this.selectedCategory.id == null)
   {
     this.selectedCategory = new Category(0, this.selectedCategory.toString());
   }
   if (this.selectedProvider.id == null)
   {
     this.selectedProvider = new ProviderL(0, this.selectedProvider.toString());
   }
   this.producttoEdit.barCode = value.barCode;
   this.producttoEdit.category = this.selectedCategory;
   this.producttoEdit.provider = this.selectedProvider;
   this.producttoEdit.name = value.name;
   this.producttoEdit.productLink = value.productLink;
   this.producttoEdit.salePrice = value.salePrice;
   this.producttoEdit.description = value.description;
   this.producttoEdit.minQuantity = value.minQuantity;
   this.producttoEdit.costPrice = value.costPrice;
   this.producttoEdit.stocks = [];
   this.producttoEdit.stocks.push(new StockDetail(value.quantity, this.localService.selectedLocal.name, this.localService.selectedLocal.id));
   this.productService.persistProduct(this.producttoEdit).subscribe(
            Cat => {
                if(this.producttoEdit.id == 0){
                  this.alertifyService.success("Producto " + this.producttoEdit.name + " creado correctamente.")
                }else{
                  this.alertifyService.success("Producto " + this.producttoEdit.name + " editado correctamente.")
                }
                this.isloading = false;
                this.route.navigate(['/products']); 
            },
            err => {
                this.isloading = false;
                if(this.producttoEdit.id == 0){
                  this.alertifyService.error("Producto " + this.producttoEdit.name + " no pudo ser creado correctamente.")
                }else{
                  this.alertifyService.error("Producto " + this.producttoEdit.name + " no pudo ser editado correctamente.")
                }
            });
  }
}
