import {ProductList, ProductDetail} from '../../_models/product';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../../_services/header.service';
import {ProductService} from '../../_services/product.service';
import {AlertifyService} from '../../_services/alertify.service';
import {LocalService} from '../../_services/local.service';
import {Resolve, Router,ActivatedRoute, RouterStateSnapshot,ParamMap} from '@angular/router';
@Component({
  selector: 'app-product-bar-code',
  templateUrl: './productBarCode.component.html',
  styleUrls: ['./productBarCode.component.scss']
})
export class ProductBarCodeComponent implements OnInit {
  isloading:boolean = false;
  barCode:string = "";
  elementType = 'svg';
  value = 'someValue12340987';
  format = 'CODE128';
  lineColor = '#000000';
  width = 1;
  height = 15;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 10;
  background = '#ffffff';
  margin = 0;
  marginTop = 30;
  marginBottom = 0;
  marginLeft = 20;
  marginRight = 0;

  numberOfRows:number[]=[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4];
  numberOfCols:number[]=[4,4,4,4];
  rows=8;
  constructor(private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      console.log(params.get('username'));
      this.barCode = params.get('barCode');
    });
  }

  print() {
    var innerContents = document.getElementById('printSectionId').innerHTML;
    var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    popupWinindow.document.write(`<html>
                                    <head>
                                      <title>Print tab</title>
                                      <style>
                                      .page {
                                        background: white;
                                        display: block;
                                        margin: 0 auto;
                                        margin-bottom: 0.5cm;
                                        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
                                      }
                                      .A4 {  
                                        width: 21cm;
                                        height: 29.7cm; 
                                      }
                                      @media print {
                                        body, .page {
                                          margin: 0;
                                          box-shadow: 0;
                                        }
                                      }
                                      .container {
                                        padding-right: 0px;
                                        padding-left: 0px;
                                        margin-right: 0px;
                                        margin-left: 0px;
                                        display: grid;
                                        grid-template-columns: 5.3cm 5.3cm 5.2cm 5.2cm;
                                      }
                                      .item {  height: 2.13cm;
                                      }
                                      .a {
                                        grid-column-start: 2;
                                        grid-column-end: 3;
                                        grid-row-start: 2;
                                        grid-row-end: 3;
                                      }
                                      </style>
                                    </head>
                                <body onload="window.print();window.close()">${innerContents}</body>
                                  </html>`);
    popupWinindow.document.close();
}

}