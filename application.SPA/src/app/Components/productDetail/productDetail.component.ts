import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {ProductService} from '../../_services/product.service';
import {LocalService} from '../../_services/local.service';
import {AlertifyService} from '../../_services/alertify.service';
import {AuthService} from '../../_services/auth.service';
import {ProductList, ProductDetail} from '../../_models/product';
import { StockDetail, StockEditor, MoveStock} from '../../_models/stock';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './productDetail.component.html',
  styleUrls: ['./productDetail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  @Input()productSelected: ProductDetail;
  @Output()reloadGrid = new EventEmitter();
  @Output()loading = new EventEmitter();
  displayStockModal: boolean = false;
  displayMoveStockModal: boolean = false;
  public newStocktForm: FormGroup; 
  public moveStocktForm: FormGroup; 

  constructor(private authService:AuthService,private productService: ProductService, public localService: LocalService, private alertifyService: AlertifyService,private fb: FormBuilder,private route: Router) {
            this.newStocktForm = fb.group({
            'salePrice': 0,
            'costPrice': 0,
            'quantity': 0
        });

        this.moveStocktForm = fb.group({
            'fromLocal':1,
            'toLocal':1,
            'quantity': 0
        });

  }
  ngOnInit() {
  }

  showcostPrice(){
    this.alertifyService.message("$"+this.productSelected.costPrice);
  }
  goToProductLink() {
    window.open("http://"+this.productSelected.productLink, "_blank");
  }
    showAddStockDialog(event) {
      this.newStocktForm = this.fb.group({
          'salePrice': 0,
          'costPrice': 0,
          'quantity': 0
      });
      this.displayStockModal = true;
  }


  showMoveStockDialog(event) {
      this.newStocktForm = this.fb.group({
          'salePrice': 0,
          'costPrice': 0,
          'quantity': 0
      });
      this.displayMoveStockModal = true;
  }
  printBarCode(event){
    this.route.navigate(['/product/'+this.productSelected.barCode]); 
  }

      submitnewStockForm(value) {
        let minumumSalePrice = ((value.costPrice * 40) / 100) + value.costPrice
        if (minumumSalePrice <= value.salePrice) {       
           this.loading.emit(true);
          let stock = new StockEditor(value.quantity, value.costPrice, value.salePrice, this.productSelected.id,this.localService.selectedLocal.id);        
          this.productService.createStock(stock).subscribe(
              resp => {
                this.displayStockModal = false;
                this.alertifyService.success('Stock agregado correctamente');
                this.reloadGrid.emit(true);
              },
              err => {     
                   this.loading.emit(false);
                   this.alertifyService.error('Hubo un Problema al agregar el stock.');
              });
        } else {
            this.alertifyService.warning('El precio de venta debe ser un 40% que el de compra.');
        }
    }

    submitMoveStockForm(value) {
        if (value.toLocal == this.localService.selectedLocal.id) {
             this.alertifyService.warning('Desde y Hacia no pueden ser el mismo local.');
        } else {
            let pr = this.productSelected.stocks;
            let stock = this.productSelected.stocks.filter(s => s.localId == this.localService.selectedLocal.id);
            if (stock.length == 0 || stock[0].quantity < value.quantity || value.quantity == 0) {
                this.alertifyService.warning('No puede llevar mas de lo que posee.');
            } else {                     
                this.loading.emit(true);
                let stock = new MoveStock(value.quantity, this.localService.selectedLocal.id, value.toLocal.id, this.productSelected.id);
                this.productService.moveStock(stock).subscribe(
                    Cat => {
                        this.displayMoveStockModal = false;
                        this.reloadGrid.emit(true);
                        this.alertifyService.success('El stock se movio correctametne.');
                    },
                    err => {                             
                        this.loading.emit(false);
                        this.alertifyService.warning('Hubo un Problema al mover los productos de local.');
                    });
            }
        }
    }



}