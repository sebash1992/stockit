
import { FileUploader } from 'ng2-file-upload';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../../_services/header.service';
import {ProductService} from '../../_services/product.service';
import {AlertifyService} from '../../_services/alertify.service';
import { ActivatedRoute,Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import {LocalService} from '../../_services/local.service';

@Component({
  selector: 'app-product-from-file-editor',
  templateUrl: './productFromFileEditor.component.html',
  styleUrls: ['./productFromFileEditor.component.scss']
})
export class ProductFromFileEditorComponent  implements OnInit {
  isloading:boolean = false;
  uploader: FileUploader = new FileUploader({});
  hasBaseDropZoneOver:boolean = false;
  productsWithError:string[] = [];
  showInstructions:boolean = true;
  constructor(private productService: ProductService,private alertifyService: AlertifyService,private route: Router, private localService:LocalService) {}
  baseUrl = environment.baseUrl;
  ngOnInit() {
    this.isloading = false;
    this.initializeUploader();
  }
  fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
  initializeUploader(){
    this.uploader =  new FileUploader({
      url:this.baseUrl + 'api/Product/CreateProductsFromFile/' + this.localService.selectedLocal.id,
      authToken: 'Bearer ' + sessionStorage.getItem('auth_token'),
      isHTML5: true,
      allowedFileType: ['xlsx', 'xls', 'csv'],
      allowedMimeType: [
        'application/pdf',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'image/png'       
    ],
      removeAfterUpload:true,
      autoUpload: true,
      maxFileSize: 10*1024*1024
    });

    this.uploader.onSuccessItem = (item,response,status,header) =>{
      this.isloading = false;
      if(response){
        this.productsWithError = response.replace('"','').split(';').splice(-1);
      }
      if(this.productsWithError.length == 0 ||(this.productsWithError.length == 1 && this.productsWithError[0] == '"' )){
        if(this.productsWithError.length == 1 && this.productsWithError[0] == '"' ){
          this.productsWithError = this.productsWithError.splice(1, 1);
        }
        this.alertifyService.success("Todos los productos se crearon con exito");
      }else{
        this.showInstructions = false;
        this.alertifyService.success("Algunos productos no se pudieron cargar exitosamente");        
      }
    }
    this.uploader.onErrorItem = (item,response,status,header) =>{
      this.isloading = false;
      this.alertifyService.error("Hubo un error al cargar los productos")
    }
    this.uploader.onBeforeUploadItem = (file) => {
      this.productsWithError = [];
      this.isloading = true;
    }
  }
}