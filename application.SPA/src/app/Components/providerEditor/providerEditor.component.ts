import { Component, OnInit } from '@angular/core';
import {ProviderService} from '../../_services/provider.service';
import {HeaderService} from '../../_services/header.service';
import {StockDetail} from '../../_models/stock';
import {CategoryList, Category} from '../../_models/category';
import {Provider} from '../../_models/provider';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import {FormBuilder, FormGroup, FormControl, Validators, NgForm} from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import {LocalService} from '../../_services/local.service';
import {AlertifyService} from '../../_services/alertify.service';

@Component({
  selector: 'app-provider-editor',
  templateUrl: './providerEditor.component.html',
  styleUrls: ['./providerEditor.component.scss']
})
export class ProviderEditorComponent implements OnInit {
 isloading:boolean = false;
 providertoEdit: Provider;

 public providerEditorForm: FormGroup;

  constructor(private frmbuilder: FormBuilder, private router: ActivatedRoute, private route: Router,
              private localService: LocalService, private alertifyService: AlertifyService,private providerService: ProviderService ){
            this.localService.configObservable.subscribe(value => {
                this.route.navigate(['/providers']); 
            });
  }

  ngOnInit() {
        this.router.data.subscribe(data => {
        this.providertoEdit = data['provider'];

        this.providerEditorForm = this.frmbuilder.group({
          'name': [this.providertoEdit.name, Validators.compose([Validators.required, Validators.maxLength(64)])],
          'phone': [this.providertoEdit.phone,  Validators.compose([Validators.maxLength(64)])],
          'email': [this.providertoEdit.email, Validators.compose([Validators.maxLength(254)])],
          'address': [this.providertoEdit.address, Validators.compose([Validators.maxLength(128)])],
          'website': [this.providertoEdit.website, Validators.compose([Validators.maxLength(256)])]
        });
        });
    }
  
  editProvider(value) {
    this.providertoEdit.name = value.name;
    this.providertoEdit.address = value.address;
    this.providertoEdit.phone = value.phone;
    this.providertoEdit.email = value.email;
    this.providertoEdit.website = value.website;
      this.providerService.persistProvider(this.providertoEdit).subscribe(
            Cat => {
                if(this.providertoEdit.id == 0){
                  this.alertifyService.success("Proveedor " + this.providertoEdit.name + " creado correctamente.")
                }else{
                  this.alertifyService.success("Proveedor " + this.providertoEdit.name + " editado correctamente.")
                }
                this.isloading = false;
                this.route.navigate(['/providers']); 
            },
            err => {
                this.isloading = false;
                if(this.providertoEdit.id == 0){
                  this.alertifyService.error("Proveedor " + this.providertoEdit.name + " no pudo ser creado correctamente.")
                }else{
                  this.alertifyService.error("Proveedor " + this.providertoEdit.name + " no pudo ser editado correctamente.")
                }
            });
  }
}
