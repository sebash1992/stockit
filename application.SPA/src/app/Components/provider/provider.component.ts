import {ProviderList} from '../../_models/providerList';
import {Provider} from '../../_models/provider';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../../_services/header.service';
import {ProviderService} from '../../_services/provider.service';
import {AlertifyService} from '../../_services/alertify.service';
import {LocalService} from '../../_services/local.service';
import {AuthService} from '../../_services/auth.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent  implements OnInit {
  isloading:boolean = false;
  providers: ProviderList[];
  selectedProvider: ProviderList;
  selectedProviderDetail: Provider;
  totalRecords: number;
  selectedPage:number;
  searchProvider:string = '';
  constructor(public authService:AuthService,private localService:LocalService,private headerService: HeaderService, private providerService: ProviderService,private alertifyService: AlertifyService,private route: Router) {
                this.localService.configObservable.subscribe(value => {
            this.getProviders(this.headerService.createHeader(this.searchProvider,'name','','',0, 0,10,true));
            });
  }

  ngOnInit() {
    this.isloading = false;
  }
  onEnter(){
            this.getProviders(this.headerService.createHeader(this.searchProvider,'name','','',0, 0,10,true));
  }
  onRowSelect(event) {
            this.providerService.getProvider(this.selectedProvider.id).subscribe(
            result => {
                 this.selectedProviderDetail = result;
            },
            error => {
                console.log(<any>error);
            }
        );
  }

  getProviders(header: HttpHeaders){
    this.setLoading(true);
    this.providerService.getProviders(header)
    .subscribe((result:PaginatedResult<ProviderList[]>) => {
                this.providers = result.result;
                if (this.providers.length > 0) {
                  this.selectedProvider = this.providers[0];
                  this.onRowSelect(null);
                }
                this.totalRecords = result.totalRows;
                this.setLoading(false);
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );
  }

  changePage(event: LazyLoadEvent) {
    this.isloading = true;
    this.selectedPage = (event.first / event.rows) + 1;
    let sortField = '';
    let sortOrder = '';
    if( event.sortField != null){
      sortField = event.sortField;
      if(sortOrder == '1'){
        sortOrder = "ASC";
      }else{
        sortOrder = "DESC";
      }
    }
    this.getProviders(this.headerService.createHeader(this.searchProvider,'name',sortField,sortOrder,0, this.selectedPage,event.rows,true));
    this.isloading = false;
  }
  removeProvider(){
       this.setLoading(true);
        this.providerService.removeProvider(this.selectedProvider.id)
       .subscribe(result => {
             this.getProviders(this.headerService.createHeader(this.searchProvider,'name','','',0, this.selectedPage,10,true));
           },
           error => {
              this.alertifyService.success("Problema al eliminar el proveedor.");
              this.setLoading(false);
               console.log(<any>error);
           }
       );
  }
  reloadGrid(reload:boolean){
    this.getProviders(this.headerService.createHeader(this.searchProvider,'name','','',0, this.selectedPage,10,true));
  }

  setLoading(newState:boolean){
    this.isloading = newState;
  }
  doubleClick(event) {
    this.route.navigate(['/newProvider/'+ this.selectedProvider.id]);
  }


}