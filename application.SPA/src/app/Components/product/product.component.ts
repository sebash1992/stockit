import {ProductList, ProductDetail} from '../../_models/product';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../../_services/header.service';
import {ProductService} from '../../_services/product.service';
import {AlertifyService} from '../../_services/alertify.service';
import {AuthService} from '../../_services/auth.service';
import {LocalService} from '../../_services/local.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  isloading:boolean = false;
  products: ProductList[];
  selectedProduct: ProductList;
  selectedProductDetail: ProductDetail;
  loading: boolean;
  totalRecords: number;
  selectedPage:number;
  searchProduct:string = "";
  constructor(public authService:AuthService,private localService:LocalService,private headerService: HeaderService, private productService: ProductService,private alertifyService: AlertifyService,private route: Router) {
                this.localService.configObservable.subscribe(value => {
                this.getProducts(this.headerService.createHeader(this.searchProduct,'barCode;name','','',0, 0,10,true));
            });
  }

  ngOnInit() {
    this.loading = false;
  }
  onRowSelect(event) {
            this.productService.getProduct(this.selectedProduct.id).subscribe(
            result => {
                 this.selectedProductDetail = result;
            },
            error => {
                console.log(<any>error);
            }
        );
  }
  onEnter(){
    this.getProducts(this.headerService.createHeader(this.searchProduct,'barCode;name','','',0, 0,10,true));
  }
  getProducts(header: HttpHeaders){
    this.setLoading(true);
    this.productService.getProducts(header)
    .subscribe((result:PaginatedResult<ProductList[]>) => {
                this.products = result.result;
                if (this.products.length > 0) {
                  this.selectedProduct = this.products[0];
                  this.onRowSelect(null);
                }
                this.totalRecords = result.totalRows;
                this.setLoading(false);
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );
  }

  changePage(event: LazyLoadEvent) {
    this.loading = true;
    this.selectedPage = (event.first / event.rows) + 1;
    let sortField = '';
    let sortOrder = '';
    if( event.sortField != null){
      sortField = event.sortField;
      if(sortOrder == '1'){
        sortOrder = "ASC";
      }else{
        sortOrder = "DESC";
      }
    }
    this.getProducts(this.headerService.createHeader(this.searchProduct,'barCode;name',sortField,sortOrder,0, this.selectedPage,event.rows,true));
    this.loading = false;
  }
  removeProduct(){

         this.setLoading(true);
        this.productService.removeProduct(this.selectedProduct.id)
       .subscribe(result => {
             this.getProducts(this.headerService.createHeader(this.searchProduct,'barCode;name','','',0, this.selectedPage,10,true));
           },
           error => {
              this.alertifyService.success("Problema al eliminar el producto");
              this.setLoading(false);
               console.log(<any>error);
           }
       );
  }
  reloadGrid(reload:boolean){
    this.getProducts(this.headerService.createHeader(this.searchProduct,'barCode;name','','',0, this.selectedPage,10,true));
  }

  setLoading(newState:boolean){
    this.isloading = newState;
  }
    doubleClick(event) {
         this.route.navigate(['/newProduct/'+ this.selectedProduct.id]);
    }

    

}