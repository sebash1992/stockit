import { Component, OnInit,ViewChild } from '@angular/core';
import {AlertifyService} from '../../_services/alertify.service';
import {DashboardService} from '../../_services/dashboard.service';
import {LocalService} from '../../_services/local.service';
import { ActivatedRoute,Router } from '@angular/router';
import {DashboardMetrics,DashboardChart} from '../../_models/dashboardMetrics';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild("baseChart")chart: BaseChartDirective;
  metrics : DashboardMetrics;
  isLoading: boolean = true;
  today: Date;
  constructor(private alertifyService:AlertifyService,private dashboardService:DashboardService,private router: ActivatedRoute, private route: Router, private localService: LocalService) {
            this.localService.configObservable.subscribe(value => {
                  this.isLoading = true;
                    this.dashboardService.getMetrics()
                    .subscribe(res => {
                       this.metrics = res;
                       this.isLoading = false;
                    });
                    this.createChart(1);
            });
            this.today = new Date();
   }

  chartOptions = {
    responsive: true
  };

  chartData = [
    { data: [], label: 'Ventas' },
    { data: [], label: 'Gastos' },
  ];

  chartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

  onChartClick(event) {
    console.log(event);
  }


  ngOnInit() {
      this.router.data.subscribe(data => {
      this.metrics = data['metrics'];
    });
    this.createChart(1);
  }

    public mainChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          drawOnChartArea: false,
        },
        ticks: {
                display: false,
        }
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,        
        }
      }]
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
        hoverBorderWidth: 3,
      }
    },
    legend: {
      display: false
    }
  };


    public mainChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: 'transparent',
      borderColor: '#5CB85C',
      pointHoverBackgroundColor: '#fff'
    },
    { // brandSuccess
      backgroundColor: 'transparent',
      borderColor: '#FF3333',
      pointHoverBackgroundColor: '#fff'
    },
  ];

  createChart(period){
    this.isLoading = true;
    this.dashboardService.getSalesExpenses(period)
    .subscribe(res => {
        this.chartLabels.length = 0;
      for (let i = res.labels.length - 1; i >= 0; i--) {
          this.chartLabels.push( res.labels[i]);
        }
      this.chart.chart.update(); 
      this.chartData = [
        { data: res.sales, label: 'Ventas' },
        { data: res.expenses, label: 'Gastos' },
      ];

      this.isLoading = false;
      this.chart.chart.update(); 
    })
  }
}