import { Component, OnInit,Input,Output, EventEmitter } from '@angular/core';
import {ProviderDoubtService} from '../../_services/providerDoubt.service';
import {Provider} from '../../_models/provider';
import {IProviderDoubt,ProviderDoubt} from '../../_models/providerDoubt';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {ActivatedRoute,Router } from '@angular/router';
import {AlertifyService} from '../../_services/alertify.service';
@Component({
  selector: 'app-provider-detail',
  templateUrl: './providerDetail.component.html',
  styleUrls: ['./providerDetail.component.scss']
})
export class ProviderDetailComponent {
  @Input()providerSelected: Provider;
  @Output()reloadGrid = new EventEmitter();
  @Output()loading = new EventEmitter();
  addDoubtDialog:boolean = false;
  doubtType: boolean;
  ammount: number;
  description: string;
    constructor(private alertifyService:AlertifyService,private providerDoubtService:ProviderDoubtService){}

  ngOnInit() {
  }
  goToProviderLink() {
    window.open("http://"+this.providerSelected.website, "_blank");
  }
  addDoubt(){
    this.ammount = 0;
    this.description = "";
    this.doubtType = false;
    this.addDoubtDialog = true;
  }
  saveDoubt(){
    if(this.ammount == 0){
      this.alertifyService.warning("Debe ingresar algun valor");
    }else{
      this.loading.emit(true);
      let payType = "Pago";
      if(!this.doubtType){
          this.ammount *= -1;
          payType = "Deuda"
      }
      let doubt = new ProviderDoubt(this.providerSelected.id, this.ammount, this.description);
      this.providerDoubtService.persistDoubt(doubt).subscribe(
              resp => {
                this.addDoubtDialog = false;
                this.alertifyService.success(payType + ' efectuado correctamente');
                this.reloadGrid.emit(true);
              },
              err => {     
                   this.loading.emit(false);
                   this.alertifyService.error('Hubo un Problema al efectuado el ' + payType +'.');
              });
    }
  }
}