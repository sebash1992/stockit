import {IStockControl, StockControl} from '../../_models/stockControl';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../../_services/header.service';
import {StockControlService} from '../../_services/stockControl.service';
import {AlertifyService} from '../../_services/alertify.service';
import {LocalService} from '../../_services/local.service';
import {AuthService} from '../../_services/auth.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-stock-control',
  templateUrl: './stockControl.component.html',
  styleUrls: ['./stockControl.component.scss']
})
export class StockControlComponent implements OnInit {
  isloading:boolean = false;
  displayStockModal: boolean = false;
  stockControls: IStockControl [];
  selectedStock: IStockControl;
  stockCounterName: string;
  totalRecords: number;
  selectedPage:number;
  searchStockControl:string = '';
  constructor(public authService:AuthService,private headerService: HeaderService, private stockControlService: StockControlService,private alertifyService: AlertifyService,private route: Router,private localService:LocalService ) {
            this.localService.configObservable.subscribe(value => {
                       this.getStockControls(this.headerService.createHeader(this.searchStockControl,'name','','',this.localService.selectedLocal.id, 0,10,true));
            });
  }

  ngOnInit() {
    this.setLoading(true);
  }
    onEnter (){
        this.getStockControls(this.headerService.createHeader(this.searchStockControl,'name','','',this.localService.selectedLocal.id, 0,10,true));
     }    
  getStockControls(header: HttpHeaders){
    this.setLoading(true);
    this.stockControlService.getStockControls(header)
    .subscribe((result:PaginatedResult<IStockControl[]>) => {
                this.stockControls = result.result;
                if (this.stockControls.length > 0) {
                  this.selectedStock = this.stockControls[0];
                }
                this.totalRecords = result.totalRows;
                this.setLoading(false);
           },
           error => {
               console.log(<any>error);
               this.setLoading(false);
           }
       );
  }

  changePage(event: LazyLoadEvent) {
    this.setLoading(true);
    this.selectedPage = (event.first / event.rows) + 1;
    let sortField = '';
    let sortOrder = '';
    if( event.sortField != null){
      sortField = event.sortField;
      if(sortOrder == '1'){
        sortOrder = "ASC";
      }else{
        sortOrder = "DESC";
      }
    }
    this.getStockControls(this.headerService.createHeader(this.searchStockControl,'name',sortField,sortOrder,this.localService.selectedLocal.id, this.selectedPage,event.rows,true));
  }

  reloadGrid(reload:boolean){
    this.getStockControls(this.headerService.createHeader(this.searchStockControl,'name','','',this.localService.selectedLocal.id, this.selectedPage,10,true));
  }

  setLoading(newState:boolean){
    this.isloading = newState;
  }
   
  AddStockControl(event) {
    this.displayStockModal = true;
}
CreateStockControl() {
    this.isloading = true;
    if (this.stockCounterName != null) {
        let newStockControl = new StockControl(this.stockCounterName, true, false, this.localService.selectedLocal.id);
        this.stockControlService.createStockControl(newStockControl).subscribe(
            Cat => {
                this.isloading = false;
                this.displayStockModal = false;
                this.route.navigate(['/newStockControl/'+Cat]); 
            },
            err => {
                this.isloading = false;
                this.displayStockModal = false;
            });
    } else {
        this.isloading = false;
        this.alertifyService.error('El Nombre es Obligatiorio');
    }
}

AddRandomStockControl() {
    this.isloading = true;
        let stockCounterName = new Date().toLocaleDateString() +" "+this.localService.selectedLocal.name;
        let newStockControl = new StockControl(stockCounterName, true, false, this.localService.selectedLocal.id);
        this.stockControlService.createRandomStockControl(newStockControl).subscribe(
            Cat => {
                this.isloading = false;
                this.route.navigate(['/newStockControl/'+Cat]); 
            },
            err => {
                this.isloading = false;
                this.displayStockModal = false;
            });
}




removeStockCounter() {
    this.isloading = true;
    this.stockControlService.removeStockControl(this.selectedStock.id).subscribe(
        Cat => {
            this.reloadGrid(true);
        },
        err => {
            this.isloading = false;
            this.alertifyService.error('No se pudo eliminar el control de stock.');
        });
            
}
editCounter() {
    this.isloading = true;
    this.route.navigate(['/newStockControl/'+ this.selectedStock.id]);
  }
doubleClick(event) {
    if (!this.selectedStock.isFinished){
        this.editCounter();
}
}







}