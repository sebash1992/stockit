import {IStockControl, StockControl, IStockControlDetail} from '../../_models/stockControl';
import {PaginatedResult} from '../../_models/pagination';
import {HttpHeaders} from '@angular/common/http';
import {LazyLoadEvent} from 'primeng/primeng';
import { Component, OnInit } from '@angular/core';
import {HeaderService} from '../../_services/header.service';
import {StockControlService} from '../../_services/stockControl.service';
import {AlertifyService} from '../../_services/alertify.service';
import {LocalService} from '../../_services/local.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-stock-control-editor',
  templateUrl: './stockControlEditor.component.html',
  styleUrls: ['./stockControlEditor.component.scss']
})
export class StockControlEditorComponent implements OnInit {
      searchValue: string = "";
    stockCounterName: string;
    quantityCount: number = 1;
    isSaved: boolean = false;
  isloading:boolean = false;
  stockControl:IStockControlDetail;
  constructor(private headerService: HeaderService, private stockControlService: StockControlService,private alertifyService: AlertifyService, private router: ActivatedRoute, private route: Router,private localService:LocalService ) {
                  this.localService.configObservable.subscribe(value => {
                    this.route.navigate(['/stockControls']);
            });
  }

  ngOnInit() {
        this.router.data.subscribe(data => {
        this.stockControl = data['stockControl'];

        });
    }


  
  setLoading(newState:boolean){
    this.isloading = newState;
  }

  onEnter() {
    this.isloading = true;
    if (this.searchValue == "") {
    }
    else {
        let products = this.stockControl.stockControlDetail.filter(x => x.barCode == this.searchValue);
        if (products.length > 0) {
            let updatedStock = products[0];
            updatedStock.countedStock += this.quantityCount;
            this.alertifyService.success('Se han agregado ' + this.quantityCount + 'u. de ' + updatedStock.name);
          } else {
            this.alertifyService.error('Producto no registrado a la hora de crear el control.');
        }
        this.isSaved = false;
        this.searchValue = "";
        this.quantityCount = 1;
        this.isloading = false;
    }
  }
    Save() {
        this.isloading = true;
        this.stockControlService.save(this.stockControl).subscribe(
            Cat => {
                this.isSaved = true;
                this.alertifyService.success('El control de stock se guardo exitosamente.');
            },
            err => {
                this.alertifyService.error('Error al guardar el control de stock.');
            });

        this.isloading = false;
    }

    SaveAndFinish() {
        this.isloading = true;
        if (this.isSaved) {
            this.Save();
        }
        this.stockControlService.finish(this.stockControl.id).subscribe(
            Cat => {
                this.route.navigate(['/stockControls']); 
            },
            err => {
                this.alertifyService.error('Error al guardar el control de stock.');
            });
        this.isloading = false;
    }



}