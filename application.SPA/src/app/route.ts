import {Routes} from '@angular/router';
import { CategoryComponent } from './components/category/category.component';
import { ProductComponent } from './components/product/product.component';
import { ProductBarCodeComponent } from './components/productBarCode/productBarCode.component';
import { ProductEditorComponent } from './components/productEditor/productEditor.component';
import { ProductFromFileEditorComponent } from './components/productFromFileEditor/productFromFileEditor.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProviderComponent } from './components/provider/provider.component';
import { SaleComponent } from './components/sale/sale.component';
import { SaleEditorComponent } from './components/saleEditor/saleEditor.component';
import { ProviderEditorComponent } from './components/providerEditor/providerEditor.component';
import { UserComponent } from './components/user/user.component';
import { UserEditorComponent } from './components/userEditor/userEditor.component';
import { LoginComponent } from './components/login/login.component';
import { ExpenseComponent } from './components/expense/expense.component';
import { StockControlComponent } from './components/stockControl/stockControl.component';
import { StockControlEditorComponent } from './components/stockControlEditor/stockControlEditor.component';
import { ProductLessStockReportComponent } from './components/reports/productLessStock/productLessStock.component';
import { ProductsReportComponent } from './components/reports/productsReport/productsReport.component';
import { ProviderDoubtsReportComponent } from './components/reports/providerDoubts/providerDoubts.component';
import { ProvidersReportComponent } from './components/reports/providersReport/providersReport.component';
import { StocktakingReportComponent } from './components/reports/stocktakingReport/stocktakingReport.component';

import {ProductEditorResolver} from './_resolver/product.editor.resolver';
import {CategoryListResolver} from './_resolver/category.list.resolver';
import {ProviderListResolver} from './_resolver/provider.list.resolver';
import {ProviderEditorResolver} from './_resolver/provider.editor.resolver';
import {StockControlEditorResolver} from './_resolver/stockcontrol.editor.resolver';
import {SaleEditorResolver} from './_resolver/sale.editor.resolver';
import {SalesListResolver} from './_resolver/sales.list.resolver';
import {PayMethodListResolver} from './_resolver/payMethod.list.resolver';
import {UserEditorResolver} from './_resolver/user.editor.resolver';
import {RoleListResolver} from './_resolver/role.list.resolver';
import {DashboardMetricResolver} from './_resolver/dashboard.metric.resolver';
import {AuthGuard} from './_guards/auth.guard';
import {AdministratorGrantGuard} from './_guards/AdministratorGrant.Guard';

export const appRoutes: Routes = [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },            
            {
                path: '',
                runGuardsAndResolvers: 'always',
                canActivate: [AuthGuard],
                children: [
                        { path: 'productLessStockReport', component: ProductLessStockReportComponent},
                        { path: 'productsReport', component: ProductsReportComponent,
                             resolve: {categories: CategoryListResolver, providers: ProviderListResolver}},
                        { path: 'providerDoubtsReport', component: ProviderDoubtsReportComponent,
                             resolve: {providers: ProviderListResolver}},
                        { path: 'providersReport', component: ProvidersReportComponent},
                        { path: 'stocktakingReport', component: StocktakingReportComponent},
                        { path: 'product/:barCode', component: ProductBarCodeComponent},
                        { path: 'products', component: ProductComponent},
                        { path: 'newProductFromFile', component: ProductFromFileEditorComponent},
                        { path: 'expenses', component: ExpenseComponent},
                        { path: 'newProduct/:id', component: ProductEditorComponent,
                            resolve: {product: ProductEditorResolver, categories: CategoryListResolver, providers: ProviderListResolver},
                            canActivate: [AdministratorGrantGuard] 
                        },
                        { path: 'newSale/:id', component: SaleEditorComponent,
                            resolve: {sale: SaleEditorResolver,payMethods: PayMethodListResolver}},
                        { path: 'newProvider/:id', component: ProviderEditorComponent,
                            resolve: {provider: ProviderEditorResolver}},
                        { path: 'sales', component: SaleComponent},
                        { path: 'stockControls', component: StockControlComponent},
                        { path: 'newStockControl/:id', component: StockControlEditorComponent,
                            resolve: {stockControl: StockControlEditorResolver},
                            canActivate: [AdministratorGrantGuard] },
                        { path: 'categories', component: CategoryComponent},
                        { path: 'dashboard', component: DashboardComponent,
                             resolve: {metrics: DashboardMetricResolver}},
                        { path: 'providers', component: ProviderComponent},
                        { path: 'newUser/:id', component: UserEditorComponent,
                            resolve: {user: UserEditorResolver, roles: RoleListResolver},
                            canActivate: [AdministratorGrantGuard] },
                        { path: 'users', component: UserComponent,
                        canActivate: [AdministratorGrantGuard] },
                ]
              },  
            { path: 'login', component: LoginComponent},  
            { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
]
