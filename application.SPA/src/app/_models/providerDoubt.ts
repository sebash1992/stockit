export interface IProviderDoubt {
    providerId: number;
    amount: number;
}

export class ProviderDoubt implements IProviderDoubt {

    constructor(
        public providerId: number,
        public amount: number,
        public description: string,
    ) { }
}

export class ProviderDoubtSearch  {

    constructor(
        public from: Date,
        public to: Date,
        public providerId: number
    ) { }
}
export interface IProviderDoubtDetail {
    date: Date;
    ammount: number;
    description:string;
}