export interface IPayMethodList {
    id: number;
    name: string;
}

export class PayMethodList implements IPayMethodList {

    constructor(
        public id: number,
        public name: string,
    ) { }
}