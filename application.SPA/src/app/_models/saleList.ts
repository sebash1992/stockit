

export interface SaleList {
    id?: number;
    date: Date;
    total: number;
    payMethod: string;

}

