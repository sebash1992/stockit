export interface ProviderList {
    id?: number;
    name: string;
    phone?: string;
    email?: string;

}

export class ProviderL implements ProviderList {

    constructor(
        public id: number,
        public name: string,
        public phone?: string,
        public email?: string,
    ) { }
}