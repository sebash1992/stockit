export interface IProvider {
    id: number;
    name: string;
    phone?: string;
    email?: string;
    address?: string;
    website?: string;
    doubt?: number;

}

export class Provider implements IProvider {

    constructor(
        public id: number,
        public name: string,
        public phone?: string,
        public email?: string,
        public address?: string,
        public website?: string,
        public doubt?: number
    ) { }
}
   