import {StockDetail} from './stock';
import {CategoryList} from './category';
import {ProviderList} from './providerList';
export interface ProductList {
    id: number;
    barCode: string;
    name: string;
    salePrice: number;
}
export interface ProductDetail {
    id: number;
    barCode: string;
    name: string;
    description: string;
    category: CategoryList;
    provider: ProviderList;
    productLink: string;
    salePrice: number;
    minQuantity: number;
    costPrice: number;
    stocks: StockDetail[];
}

