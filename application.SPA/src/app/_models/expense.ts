export interface IExpense {
    id: number;
    date: Date;
    amount: number;
    description: string;
    isMonthly: boolean;
    localId:number

}

export class Expense implements IExpense {

    constructor(
        public id: number,
        public date: Date,
        public amount: number,
        public description: string,
        public isMonthly: boolean,
        public localId: number,
    ) { }
}

export interface IMonthlyExpense {
    id: number;
    amount: number;
    description: string;
    LocalId: number;

}

export class MonthlyExpense implements IMonthlyExpense {

    constructor(
        public id: number,
        public amount: number,
        public description: string,
        public LocalId: number,
    ) { }
}


export class searchDate{

    constructor(
        public from: Date,
        public to: Date,
    ) { }
}