export interface IStockControl {
    id?: number;
    name: string;
    createdDate?: Date;
    isActive: boolean;
    isFinished: boolean;
    localId: number;
}

export class StockControl implements IStockControl {

    constructor(
        public name: string,
        public isActive: boolean,
        public isFinished: boolean,
        public localId: number,
        public id?: number,
        public createdDate?: Date,
    ) { }
}

export interface IStockControlDetail {
    id: number;
    createdDate: Date;
    stockControlDetail: IStockControlStock[];
    name: string;
    tenantId: string;
    localId: number;

}
export interface IStockControlStock {
    id: number;
    stockControlId: number;
    productId: number;
    actualStock: number;
    countedStock: number;
    name: string;
    barCode: string;

}
