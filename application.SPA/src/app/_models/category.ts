export interface CategoryList {
    id: number;
    name: string;
}

export class Category implements CategoryList {

    constructor(
        public id: number,
        public name: string,
    ) { }
}