export interface ISaleDetail {
    id?: number;
    productBarCode: string;
    productId:number,
    productName: string;
    quantity: number;
    unityPrice: number;
}

export class SaleDetail implements ISaleDetail {

    constructor(
      public productBarCode: string,
      public productId:number,
      public productName: string,
      public quantity: number,
      public unityPrice: number,
      public id?: number,
    ) { }
}

export interface ISaleServiceDetail {
    id?: number;
    service: string;
    price: number;
}

export class SaleServiceDetail implements ISaleServiceDetail {

    constructor(
      public service: string,
      public price:number,
      public id?: number,
    ) { }
}