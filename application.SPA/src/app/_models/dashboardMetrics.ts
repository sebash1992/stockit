export interface DashboardMetrics {
    todaySales: string;
    todayExpenses: string;
    productWithMinimumStock: string;
    productWithMostSales: string;
}

export interface DashboardChart {
    sales: string[];
    expenses: string[];
    labels: string[];
}

export interface Stocktaking {
    sales: number;
    expenses: number;
    date: Date;
}

