export interface IStockDetail {
    quantity: number;
    local: string;
    localId: number;
}

export class StockDetail implements IStockDetail {

    constructor(
        public quantity: number,
        public local: string,
        public localId: number
    ) { }
}

export interface IStockEditor {
    quantity: number;
    costPrice: number;
    salePrice: number;
    productId: number;
    localId: number;
}
export class StockEditor implements IStockEditor {

    constructor(
        public quantity: number,
        public costPrice: number,
        public salePrice: number,
        public productId: number,
        public localId: number
    ) { }
}

export interface IMoveStock {
    quantity: number;
    fromLocal: number;
    toLocal: number;
    productId: number;
}

export class MoveStock implements IMoveStock {

    constructor(
        public quantity: number,
        public fromLocal: number,
        public toLocal: number,
        public productId: number,
    ) { }
}