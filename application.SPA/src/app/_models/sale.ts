import { ISaleDetail, ISaleServiceDetail } from './saleDetail';

export interface ISale {
    id?: number;
    date: Date;
    subtotal: number;
    discount: number;
    total: number;
    cost: number;
    payMethod: number;
    localId: number;
    productsSent: ISaleDetail[];
    servicesSent: ISaleServiceDetail[];

}

export class Sale implements ISale {

    constructor(
       public date: Date,
       public subtotal: number,
       public discount: number,
       public total: number,
       public cost: number,
       public payMethod: number,
       public localId:number,
       public productsSent: ISaleDetail[],
       public servicesSent: ISaleServiceDetail[],
       public id?: number
    ) { }
}