import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { SaleList } from '../_models/saleList';
import { PaginatedResult } from '../_models/pagination';
import { AlertifyService } from '../_services/alertify.service';
import { SaleService } from '../_services/sale.service';
import { HeaderService } from '../_services/header.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class SalesListResolver implements Resolve<PaginatedResult<SaleList>> {
    constructor(private headerService: HeaderService, private saleService: SaleService,
                 private router: Router, private alertify: AlertifyService ) {

    }
    resolve(route: ActivatedRouteSnapshot): Observable<PaginatedResult<SaleList>> {
        return this.saleService.getSales(this.headerService.createHeader('', '', '', '', 0, 0, 0, true)).catch(error => {
            this.alertify.error('A ocurrido un problema');
            this.router.navigate(['/sales']);
            return Observable.of(null);
        });
    }
}
