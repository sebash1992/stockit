import {Resolve, Router,ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Provider} from '../_models/provider';
import {AlertifyService} from '../_services/alertify.service';
import { StockControlService} from '../_services/stockControl.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import {IStockControl} from '../_models/stockControl';

@Injectable()
export class StockControlEditorResolver implements Resolve<Provider>{
    constructor(private stockControlService: StockControlService, private router:Router, private alertify: AlertifyService ){

    }
    resolve(route:ActivatedRouteSnapshot):Observable<Provider>{
        return this.stockControlService.getStockControl(route.params['id']).catch(error =>{
            this.alertify.error('A ocurrido un problema');
            this.router.navigate(['/stockControls']);
            return Observable.of(null);
        })
    }
}

