import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { ProviderList } from '../_models/providerList';
import { PaginatedResult } from '../_models/pagination';
import { AlertifyService } from '../_services/alertify.service';
import { ProviderService } from '../_services/provider.service';
import { HeaderService } from '../_services/header.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProviderListResolver implements Resolve<PaginatedResult<ProviderList>> {
    constructor(private headerService: HeaderService, private providerService: ProviderService,
                 private router: Router, private alertify: AlertifyService ) {

    }
    resolve(route: ActivatedRouteSnapshot): Observable<PaginatedResult<ProviderList>> {
        return this.providerService.getProviders(this.headerService.createHeader('', '', '', '', 0, 0, 0, true)).catch(error => {
            this.alertify.error('A ocurrido un problema');
            this.router.navigate(['/product']);
            return Observable.of(null);
        });
    }
}
