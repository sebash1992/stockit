import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { IRole } from '../_models/role';
import { PaginatedResult } from '../_models/pagination';
import { AlertifyService } from '../_services/alertify.service';
import { RoleService } from '../_services/role.service';
import { HeaderService } from '../_services/header.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class RoleListResolver implements Resolve<PaginatedResult<IRole>> {
    constructor(private headerService: HeaderService, private roleService: RoleService,
                 private router: Router, private alertify: AlertifyService ) {

    }
    resolve(route: ActivatedRouteSnapshot): Observable<PaginatedResult<IRole>> {
        return this.roleService.getRoles().catch(error => {
            this.alertify.error('A ocurrido un problema');
            this.router.navigate(['/users']);
            return Observable.of(null);
        });
    }
}
