import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { CategoryList } from '../_models/category';
import { PaginatedResult } from '../_models/pagination';
import { AlertifyService } from '../_services/alertify.service';
import { CategoryService } from '../_services/category.service';
import { HeaderService } from '../_services/header.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class CategoryListResolver implements Resolve<PaginatedResult<CategoryList>> {
    constructor(private headerService: HeaderService, private categoryService: CategoryService,
                 private router: Router, private alertify: AlertifyService ) {

    }
    resolve(route: ActivatedRouteSnapshot): Observable<PaginatedResult<CategoryList>>{
        return this.categoryService.getCategories(this.headerService.createHeader('', '', '', '', 0, 0, 0, true)).catch(error => {
            this.alertify.error('A ocurrido un problema');
            this.router.navigate(['/product']);
            return Observable.of(null);
        });
    }
}
