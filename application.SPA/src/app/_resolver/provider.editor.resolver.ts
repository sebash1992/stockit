import {Resolve, Router,ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Provider} from '../_models/provider';
import {AlertifyService} from '../_services/alertify.service';
import {ProviderService} from '../_services/provider.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProviderEditorResolver implements Resolve<Provider>{
    constructor(private providerService: ProviderService, private router:Router, private alertify: AlertifyService ){

    }
    resolve(route:ActivatedRouteSnapshot):Observable<Provider>{
        return this.providerService.getProvider(route.params['id']).catch(error =>{
            this.alertify.error('A ocurrido un problema');
            this.router.navigate(['/providers']);
            return Observable.of(null);
        })
    }
}

