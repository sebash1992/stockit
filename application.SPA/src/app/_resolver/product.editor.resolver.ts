import {Resolve, Router,ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {ProductList,ProductDetail} from '../_models/product';
import {AlertifyService} from '../_services/alertify.service';
import {ProductService} from '../_services/product.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProductEditorResolver implements Resolve<ProductList>{
    constructor(private productService: ProductService, private router:Router, private alertify: AlertifyService ){

    }
    resolve(route:ActivatedRouteSnapshot):Observable<ProductList>{
        return this.productService.getProduct(route.params['id']).catch(error =>{
            this.alertify.error('A ocurrido un problema');
            this.router.navigate(['/product']);
            return Observable.of(null);
        })
    }
}
