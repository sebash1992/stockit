import {Resolve, Router,ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {DashboardMetrics} from '../_models/dashboardMetrics';
import {AlertifyService} from '../_services/alertify.service';
import {DashboardService} from '../_services/dashboard.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class DashboardMetricResolver implements Resolve<DashboardMetrics>{
    constructor(private dashboardService: DashboardService, private router:Router, private alertify: AlertifyService ){

    }
    resolve(route:ActivatedRouteSnapshot):Observable<DashboardMetrics>{
        return this.dashboardService.getMetrics().catch(error =>{
            this.alertify.error('A ocurrido un problema obteniendo el dashboard');
            this.router.navigate(['/products']);
            return Observable.of(null);
        })
    }
}
