import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { PayMethodList } from '../_models/payMethod';
import { PaginatedResult } from '../_models/pagination';
import { AlertifyService } from '../_services/alertify.service';
import { PayMethodService } from '../_services/payMethod.service';
import { HeaderService } from '../_services/header.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class PayMethodListResolver implements Resolve<PaginatedResult<PayMethodList>> {
    constructor(private headerService: HeaderService, private payMethodService: PayMethodService,
                 private router: Router, private alertify: AlertifyService ) {

    }
    resolve(route: ActivatedRouteSnapshot): Observable<PaginatedResult<PayMethodList>>{
        return this.payMethodService.getPayMethods(this.headerService.createHeader('', '', '', '', 0, 0, 0, true)).catch(error => {
            this.alertify.error('A ocurrido un problema');
            this.router.navigate(['/sales']);
            return Observable.of(null);
        });
    }
}
