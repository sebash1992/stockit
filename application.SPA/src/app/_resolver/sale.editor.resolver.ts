import {Resolve, Router,ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {ISale} from '../_models/sale';
import {AlertifyService} from '../_services/alertify.service';
import {SaleService} from '../_services/sale.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class SaleEditorResolver implements Resolve<ISale>{
    constructor(private saleService: SaleService, private router:Router, private alertify: AlertifyService ){

    }
    resolve(route:ActivatedRouteSnapshot):Observable<ISale>{
        return this.saleService.getSale(route.params['id']).catch(error =>{
            this.alertify.error('A ocurrido un problema');
            this.router.navigate(['/product']);
            return Observable.of(null);
        })
    }
}
