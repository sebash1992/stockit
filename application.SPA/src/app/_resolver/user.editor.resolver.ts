import {Resolve, Router,ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {IUserEditor} from '../_models/user';
import {AlertifyService} from '../_services/alertify.service';
import {UserService} from '../_services/user.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class UserEditorResolver implements Resolve<IUserEditor>{
    constructor(private userService: UserService, private router:Router, private alertify: AlertifyService ){

    }
    resolve(route:ActivatedRouteSnapshot):Observable<IUserEditor>{
        return this.userService.getUser(route.params['id']).catch(error =>{
            this.alertify.error('A ocurrido un problema');
            this.router.navigate(['/users']);
            return Observable.of(null);
        })
    }
}
