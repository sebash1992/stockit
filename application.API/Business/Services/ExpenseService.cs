﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Dtos;
using System.Globalization;

namespace application.API.Business.Services
{
    public class ExpenseService : IExpenseService
    {
        private IRepository<Expense> _expenseRepositoryse;

        public ExpenseService(IRepository<Expense> ExpenseRepository, IRepository<Product> productRepository, IRepository<Stock> stockRepository)
        {
            _expenseRepositoryse = ExpenseRepository ?? throw new ArgumentNullException(nameof(ExpenseRepository));
        }

        public int Count(string filterExpression)
        {
            return _expenseRepositoryse.Count(filterExpression);
        }

        public void Create(Expense entity)
        {
            _expenseRepositoryse.Create(entity);
            _expenseRepositoryse.SaveChanges();

        }

        public void Delete(object id)
        {
            _expenseRepositoryse.Delete(id);
        }

        public IEnumerable<GraphDto> GetExpensesInPeriod(int id, int localId)
        {
            string filterExpression = "";
            if (id == 1)
            {
                DateTime date = DateTime.UtcNow.AddMonths(-1);
                TimeSpan ts = new TimeSpan(23, 59, 59);
                date = date.Date + ts;
                filterExpression = string.Format("(Date > \"{0}\")", date.GetDateTimeFormats(new CultureInfo("en-US")));
            }
            else
            {
                DateTime date = DateTime.UtcNow.AddYears(-1);
                TimeSpan ts = new TimeSpan(23, 59, 59);
                date = date.Date + ts;
                filterExpression = string.Format("(Date > \"{0}\")", date.GetDateTimeFormats(new CultureInfo("en-US")));
            }
            filterExpression += " AND LocalId == " + localId;
            if (id == 1)
            {
                return Search(filterExpression, null)
                                .GroupBy(s => s.Date.Day)
                                .Select(cl => new GraphDto
                                {
                                    Label = cl.Key,
                                    Value = cl.Sum(c => c.Amount)
                                }).OrderBy(x => x.Label);
            }
            else
            {
                return Search(filterExpression, null)
                                .GroupBy(s => s.Date.Month)
                                .Select(cl => new GraphDto
                                {
                                    Label = cl.Key,
                                    Value = cl.Sum(c => c.Amount)
                                }).OrderBy(x => x.Label);
            }
        }

        public double GetTodayExpenses(int localId)
        {
            DateTime date = DateTime.UtcNow.AddDays(-1);
            TimeSpan ts = new TimeSpan(23, 59, 59);
            date = date.Date + ts;


            string filterExpression = string.Format("(Date > \"{0}\")", date.GetDateTimeFormats(new CultureInfo("en-US")));
            filterExpression += "AND localId == " + localId;
            var Expenses = Search(filterExpression, null);

            var ExpensesSum = Expenses.Sum(s => s.Amount);
            return ExpensesSum;
        }

        public Expense Read(object id)
        {
            return _expenseRepositoryse.Read(id);
        }

        public Expense Read(string filterExpression)
        {
            return _expenseRepositoryse.Read(filterExpression);
        }
        
        public IEnumerable<Expense> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include )
        {
            return _expenseRepositoryse.Search(filterExpression, sortingExpression, startIndex, count,include);
        }
        public IEnumerable<Expense> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null, include);
        }
        public IEnumerable<Expense> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<Expense> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }

        public void Update(Expense entity)
        {
            _expenseRepositoryse.Update(entity);
            _expenseRepositoryse.SaveChanges();
        }
    }
}
