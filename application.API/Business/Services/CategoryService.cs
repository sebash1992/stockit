﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Services
{
    public class PayMethodService : IPayMethodService
    {
        private IReadOnlyRepository<PayMethod> _payMethodRepository;

        public PayMethodService(IReadOnlyRepository<PayMethod> PayMethodRepository)
        {
            _payMethodRepository = PayMethodRepository ?? throw new ArgumentNullException(nameof(PayMethodRepository));
        }

        public int Count(string filterExpression)
        {
            return _payMethodRepository.Count(filterExpression);
        }

        public PayMethod Read(object id)
        {
            return _payMethodRepository.Read(id);
        }

        public PayMethod Read(string filterExpression)
        {
            return _payMethodRepository.Read(filterExpression);
        }

        public IEnumerable<PayMethod> Search(string filterExpression, string sortingExpression, int? startIndex, int? count,string include)
        {
            return _payMethodRepository.Search(filterExpression, sortingExpression, startIndex, count, include);
        }
        public IEnumerable<PayMethod> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null,include);
        }
        public IEnumerable<PayMethod> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null,"");
        }

        public IEnumerable<PayMethod> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null);
        }

    }
}
