﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Services
{
    public class ProviderService : IProviderService
    {
        private IRepository<Provider> _providerRepository;

        public ProviderService(IRepository<Provider> ProviderRepository)
        {
            _providerRepository = ProviderRepository ?? throw new ArgumentNullException(nameof(ProviderRepository));
        }

        public int Count(string filterExpression)
        {
            return _providerRepository.Count(filterExpression);
        }

        public void Create(Provider entity)
        {
            entity.IsActive = true;
            _providerRepository.Create(entity);
            _providerRepository.SaveChanges();
        }

        public void Delete(object id)
        {
            _providerRepository.Delete(id);
        }

        public Provider Read(object id)
        {
            return _providerRepository.Read(id);
        }

        public Provider Read(string filterExpression)
        {
            return _providerRepository.Read(filterExpression);
        }

        public IEnumerable<Provider> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include)
        {
            return _providerRepository.Search(filterExpression, sortingExpression, startIndex, count,include);
        }
        public IEnumerable<Provider> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null, include);
        }
        public IEnumerable<Provider> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<Provider> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }

        public void ToggleActive(object id)
        {
            var entity = Read(id);
            entity.IsActive = !entity.IsActive;
            _providerRepository.SaveChanges();
        }

        public void Update(Provider entity)
        {
            _providerRepository.Update(entity);
            _providerRepository.SaveChanges();
        }
    }
}
