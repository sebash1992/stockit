﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Services
{
    public class CategoryService : ICategoryService
    {
        private IRepository<Category> _categoryRepository;

        public CategoryService(IRepository<Category> CategoryRepository)
        {
            _categoryRepository = CategoryRepository ?? throw new ArgumentNullException(nameof(CategoryRepository));
        }

        public int Count(string filterExpression)
        {
            return _categoryRepository.Count(filterExpression);
        }

        public void Create(Category entity)
        {
            entity.IsActive = true;
            _categoryRepository.Create(entity);
            _categoryRepository.SaveChanges();
        }

        public void Delete(object id)
        {
            _categoryRepository.Delete(id);
        }

        public Category Read(object id)
        {
            return _categoryRepository.Read(id);
        }

        public Category Read(string filterExpression)
        {
            return _categoryRepository.Read(filterExpression);
        }

        public IEnumerable<Category> Search(string filterExpression, string sortingExpression, int? startIndex, int? count,string include)
        {
            return _categoryRepository.Search(filterExpression, sortingExpression, startIndex, count, include);
        }
        public IEnumerable<Category> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null,include);
        }
        public IEnumerable<Category> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null,"");
        }

        public IEnumerable<Category> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null);
        }

        public void ToggleActive(object id)
        {
            var entity = Read(id);
            entity.IsActive = !entity.IsActive;
            _categoryRepository.SaveChanges();
        }

        public void Update(Category entity)
        {
            _categoryRepository.Update(entity);
        }
    }
}
