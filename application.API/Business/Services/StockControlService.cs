﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Dtos;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Services
{
    public class StockControlService : IStockControlService
    {
        private IRepository<StockControl> _stockControlRepository;
        private IRepository<StockControlDetail> _stockControlDetailRepository;
        private IRepository<Product> _productRepository;

        public StockControlService(IRepository<StockControl> StockControlRepository, IRepository<StockControlDetail> StockControlDetailRepository, IRepository<Product> productRepository)
        {
            _stockControlDetailRepository = StockControlDetailRepository ?? throw new ArgumentNullException(nameof(StockControlDetailRepository));
            _stockControlRepository = StockControlRepository ?? throw new ArgumentNullException(nameof(StockControlRepository));
            _productRepository = productRepository ?? throw new ArgumentNullException(nameof(productRepository));
        }

        public int Count(string filterExpression)
        {
            return _stockControlRepository.Count(filterExpression);
        }

        public void Create(StockControl entity)
        {
            entity.IsActive = true;
            entity.CreatedDate = DateTime.UtcNow;

            var prods = _productRepository.Search("(IsActive == true)", null, "Stock;Stock.Local");
            foreach (var product in prods)
            {
                var stockDetail = new StockControlDetail()
                {
                    ProductId = product.Id,
                    IsActive = true,
                    CountedStock = 0,
                    ActualStock = product.Stock.Where(x => x.LocalId == entity.LocalId).Sum(x => x.Quantity),

                };
                entity.StockControlDetail.Add(stockDetail);
            }

            _stockControlRepository.Create(entity);
            _stockControlRepository.SaveChanges();

        }

        public void CreateRandom(StockControl entity)
        {
            entity.IsActive = true;
            entity.CreatedDate = DateTime.UtcNow;

            var prodsCount = _productRepository.Count("(IsActive == true)");
            if(prodsCount < 10){
                var prods = _productRepository.Search("(IsActive == true)", null, "Stock;Stock.Local");
                foreach (var product in prods)
                {
                    var stockDetail = new StockControlDetail()
                    {
                        ProductId = product.Id,
                        IsActive = true,
                        CountedStock = 0,
                        ActualStock = product.Stock.Where(x => x.LocalId == entity.LocalId).Sum(x => x.Quantity),

                    };
                    entity.StockControlDetail.Add(stockDetail);
                }
            }
            else
            {
                Random rnd = new Random();
                int productPlace = 0;
                var prods = _productRepository.Search("(IsActive == true)", null, "Stock;Stock.Local");
                for (int i = 0; i < 10; i++)
                {
                    bool noExist = true;
                    while (noExist)
                    {
                        Product product = prods.ElementAt(productPlace);
                        productPlace = rnd.Next(0, prodsCount);
                        if (entity.StockControlDetail.Where(x => x.Id == product.Id).Count() == 0)
                        {
                            noExist = false;
                            var stockDetail = new StockControlDetail()
                            {
                                ProductId = product.Id,
                                IsActive = true,
                                CountedStock = 0,
                                ActualStock = product.Stock.Where(x => x.LocalId == entity.LocalId).Sum(x => x.Quantity),

                            };
                        }
                    }
                }
            }

            _stockControlRepository.Create(entity);
            _stockControlRepository.SaveChanges();
        }

        public void Delete(object id)
        {
            _stockControlRepository.Delete(id);
        }

        public StockControl Read(object id)
        {
            return _stockControlRepository.Read(id);
        }

        public StockControl Read(string filterExpression)
        {
            return _stockControlRepository.Read(filterExpression);
        }
        
        public IEnumerable<StockControl> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include )
        {
            return _stockControlRepository.Search(filterExpression, sortingExpression, startIndex, count,include);
        }
        public IEnumerable<StockControl> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null, include);
        }
        public IEnumerable<StockControl> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<StockControl> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }

        public void ToggleActive(object id)
        {
            var entity = Read(id);
            entity.IsActive = !entity.IsActive;
            _stockControlRepository.SaveChanges();
        }

        public void Update(StockControl entity)
        {
            foreach(var control in entity.StockControlDetail)
            {
                _stockControlDetailRepository.Update(control);
            }
            _stockControlRepository.Update(entity);
            _stockControlRepository.SaveChanges();
        }
        public void FinishControl(int id)
        {
            var entity = Read(id);
            entity.IsFinished = true;
            _stockControlRepository.Update(entity);
            _stockControlRepository.SaveChanges();
        }

    }
}
