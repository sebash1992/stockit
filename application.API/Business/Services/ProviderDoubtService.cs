﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Dtos;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Services
{
    public class ProviderDoubtService : IProviderDoubtService
    {
        private IRepository<ProviderDoubts> _providerDoubtRepository;

        public ProviderDoubtService(IRepository<ProviderDoubts> providerDoubtRepository)
        {
            _providerDoubtRepository = providerDoubtRepository ?? throw new ArgumentNullException(nameof(providerDoubtRepository));
        }

        public int Count(string filterExpression)
        {
            return _providerDoubtRepository.Count(filterExpression);
        }

        public void Create(ProviderDoubts entity)
        {
            _providerDoubtRepository.Create(entity);
            _providerDoubtRepository.SaveChanges();
        }

        public void Delete(object id)
        {
           
        }

        public ProviderDoubts Read(object id)
        {
            return _providerDoubtRepository.Read(id);
        }

        public ProviderDoubts Read(string filterExpression)
        {
            return _providerDoubtRepository.Read(filterExpression);
        }
        
        public IEnumerable<ProviderDoubts> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include )
        {
            return _providerDoubtRepository.Search(filterExpression, sortingExpression, startIndex, count,include);
        }
        public IEnumerable<ProviderDoubts> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null, include);
        }
        public IEnumerable<ProviderDoubts> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<ProviderDoubts> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<ProviderSearchDoubtDetailDto> GetProviderDoubt(ProviderSearchDoubtDto search)
        {
            //     string filterExpression = string.Format("(PaidDate < \"{0}\") AND ProviderId == {1}", search.To.GetDateTimeFormats(new CultureInfo("en-US")),search.ProviderId);
            string filterExpression = string.Format("(PaidDate <= \"{0}\")", search.To.ToUniversalTime().GetDateTimeFormats(new CultureInfo("en-US")));
            filterExpression += " AND ProviderId == " + search.ProviderId;
            string sortExpression = "PaidDate  ASC";
            var doubt = Search(filterExpression, sortExpression);
            double previousDoubt = doubt.Where(x => x.PaidDate.Date < search.From.ToUniversalTime().Date).Sum(y => y.Amount);
            List<ProviderSearchDoubtDetailDto> doubtCount = new List<ProviderSearchDoubtDetailDto>();
            doubtCount.Add(new ProviderSearchDoubtDetailDto() { Date = new DateTime(2000, 1, 1), Ammount = previousDoubt });
            var tor = doubtCount.Concat(doubt.Where(x => x.PaidDate.Date >= search.From.ToUniversalTime().Date).Select(t => new ProviderSearchDoubtDetailDto()
            {
                Date = t.PaidDate,
                Ammount = t.Amount,
                Description = t.Description
            }));
            return tor;

        }
        public void Update(ProviderDoubts entity)
        {
            _providerDoubtRepository.Update(entity);
            _providerDoubtRepository.SaveChanges();
        }
    }
}
