﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Services
{
    public class LocalService : ILocalService
    {
        private IRepository<Local> _localRepository;

        public LocalService(IRepository<Local> localRepository)
        {
            _localRepository = localRepository ?? throw new ArgumentNullException(nameof(localRepository));
        }

        public int Count(string filterExpression)
        {
            return _localRepository.Count(filterExpression);
        }

        public void Create(Local entity)
        {
            _localRepository.Create(entity);
        }

        public void Delete(object id)
        {
            _localRepository.Delete(id);
        }

        public Local Read(object id)
        {
            return _localRepository.Read(id);
        }

        public Local Read(string filterExpression)
        {
            return _localRepository.Read(filterExpression);
        }

        public IEnumerable<Local> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include)
        {
            return _localRepository.Search(filterExpression, sortingExpression, startIndex, count,include);
        }
        public IEnumerable<Local> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null, include);
        }
        public IEnumerable<Local> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<Local> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null,"");
        }

        public void ToggleActive(object id)
        {
            var entity = Read(id);
            entity.IsActive = !entity.IsActive;
            _localRepository.SaveChanges();
        }

        public void Update(Local entity)
        {
            _localRepository.Update(entity);
        }
    }
}
