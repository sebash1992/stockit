﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Dtos;
using System.Globalization;

namespace application.API.Business.Services
{
    public class MonthlyExpenseService : IMonthlyExpenseService
    {
        private IRepository<MonthlyExpenses> _onthlyExpenseRepository;
        private IRepository<Expense> _expenseRepository;

        public MonthlyExpenseService(IRepository<MonthlyExpenses> MonthlyExpenseRepository, IRepository<Expense> expenseRepository, IRepository<Product> productRepository, IRepository<Stock> stockRepository)
        {
            _onthlyExpenseRepository = MonthlyExpenseRepository ?? throw new ArgumentNullException(nameof(MonthlyExpenseRepository));
            _expenseRepository = expenseRepository ?? throw new ArgumentNullException(nameof(expenseRepository));
        }

        public int Count(string filterExpression)
        {
            return _onthlyExpenseRepository.Count(filterExpression);
        }

        public void Create(MonthlyExpenses entity)
        {            
            _onthlyExpenseRepository.Create(entity);
            _onthlyExpenseRepository.SaveChanges();

        }

        public void Delete(object id)
        {
            _onthlyExpenseRepository.Delete(id);
            _onthlyExpenseRepository.SaveChanges();
        }

        public void PayAll()
        {
            IEnumerable<MonthlyExpenses> expenses = Search(null,null);
            foreach (var expense in expenses)
            {
                Expense exToAdd = new Expense()
                {
                    Amount = expense.Amount,
                    Description = expense.Description,
                    IsMonthly = true,
                    Date = DateTime.UtcNow,
                    LocalId = expense.LocalId

                };
                _expenseRepository.Create(exToAdd);
            }
            _expenseRepository.SaveChanges();
        }

        public MonthlyExpenses Read(object id)
        {
            return _onthlyExpenseRepository.Read(id);
        }

        public MonthlyExpenses Read(string filterExpression)
        {
            return _onthlyExpenseRepository.Read(filterExpression);
        }
        
        public IEnumerable<MonthlyExpenses> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include )
        {
            return _onthlyExpenseRepository.Search(filterExpression, sortingExpression, startIndex, count,include);
        }
        public IEnumerable<MonthlyExpenses> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null, include);
        }
        public IEnumerable<MonthlyExpenses> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<MonthlyExpenses> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }

        public void Update(MonthlyExpenses entity)
        {
            _onthlyExpenseRepository.Update(entity);
            _onthlyExpenseRepository.SaveChanges();
        }
    }
}
