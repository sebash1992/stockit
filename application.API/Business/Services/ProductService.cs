﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Services
{
    public class ProductService : IProductService
    {
        private IRepository<Product> _productRepository;
        private IRepository<Stock> _stockRepository;
        private IRepository<Category> _categoryRepository;
        private IRepository<Provider> _providerRepository;

        public ProductService(IRepository<Product> productRepository, IRepository<Stock> stockRepository, IRepository<Category> categoryRepository, IRepository<Provider> providerRepository)
        {
            _productRepository = productRepository ?? throw new ArgumentNullException(nameof(productRepository));
            _stockRepository = stockRepository ?? throw new ArgumentNullException(nameof(stockRepository));
            _categoryRepository = categoryRepository ?? throw new ArgumentNullException(nameof(categoryRepository));
            _providerRepository = providerRepository ?? throw new ArgumentNullException(nameof(providerRepository));
        }

        public int Count(string filterExpression)
        {
            return _productRepository.Count(filterExpression);
        }

        public void Create(Product entity)
        {
            _productRepository.Create(entity);
            _productRepository.SaveChanges();
        }

        public void Delete(object id)
        {
            ToggleActive(id);
        }

        public IEnumerable<Product> ProductWithMinimumStock(int localId)
        {
            string filterExpression = string.Format("(IsActive == true)");
            IEnumerable<Product> prods = Search(filterExpression, null, "Provider;Stock;Stock.Local");
            return prods.Where(p => p.MinQuantity >= p.Stock.Sum(s => s.Quantity) && p.Stock.Any(s=> s.LocalId == localId));
        }

        public string ProductWithMostSales(int localId)
        {
            string filterExpression = string.Format("(IsActive == true)");
            IEnumerable<Product> prods = Search(filterExpression, null, "SaleDetail;Provider;Stock;Stock.Local");
            var t = prods.Where(p=> p.Stock.Any(s => s.LocalId == localId)).OrderByDescending(p => p.SaleDetail.Sum(s => s.Quantity));
            if(t.Count() == 0)
            {
                return "";
            }
            else
            {
                return t.FirstOrDefault().Name;
            }
        }

        public Product Read(object id)
        {
            return _productRepository.Read(id);
        }

        public Product Read(string filterExpression)
        {
            return _productRepository.Read(filterExpression);
        }
        
        public IEnumerable<Product> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include )
        {
            return _productRepository.Search(filterExpression, sortingExpression, startIndex, count,include);
        }
        public IEnumerable<Product> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null, include);
        }
        public IEnumerable<Product> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<Product> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }

        public void ToggleActive(object id)
        {
            var entity = Read(id);
            entity.IsActive = !entity.IsActive;
            _productRepository.SaveChanges();
        }

        public void Update(Product entity)
        {
            _productRepository.Update(entity);
            _productRepository.SaveChanges();
        }
        public void UpdateSalePrice(int id, float salePrice)
        {
            Product prod = Read(id);
            if(prod.SalePrice != salePrice)
            {
                prod.SalePrice = salePrice;
                _productRepository.Update(prod);
                _productRepository.SaveChanges();
            }
        }

        public string CreateProductFromFile(IFormFile file, int localId)
        {
            string productsFail = "";
            using (Stream stream = file.OpenReadStream())
            {
                using (var binaryReader = new StreamReader(stream))
                {
                    string productName = string.Empty;
                    //skip first line because is headers
                    string currentLine = binaryReader.ReadLine();
                    while ((currentLine = binaryReader.ReadLine()) != null && !currentLine.Contains(";;;"))
                    {
                        try
                        {
                            string[] productInfo = currentLine.Split(new char[] { ';' }, StringSplitOptions.None);
                            productName = productInfo[1] + " " + productInfo[2];
                            var costPrice = Double.Parse(productInfo[3].Replace("$", string.Empty));
                            var salePrice = Double.Parse(productInfo[4].Replace("$", string.Empty));
                            string filterExpression = string.Format("(Name == \"{0}\") AND (IsActive == true)", productInfo[1] + " " + productInfo[2]);
                            if (Count(filterExpression) != 0)
                            {
                                var productToUpdae = Search(filterExpression,null).FirstOrDefault();
                                //  var stockToAdd = mapper.getModelFromViewModel(entity);
                                if (productToUpdae != null)
                                {
                                    if (costPrice + (((costPrice) * 40) / 100) <= salePrice)
                                    {

                                        var stockToAdd = new Stock
                                        {
                                            CostPrice = costPrice,
                                            ProductId = productToUpdae.Id,
                                            Quantity = Int32.Parse(productInfo[5]),
                                            LocalId = localId,
                                            IsActive = true
                                        };

                                        if (productToUpdae.SalePrice != salePrice)
                                        {
                                            productToUpdae.SalePrice = salePrice;
                                            Update(productToUpdae);
                                        }
                                        _stockRepository.Create(stockToAdd);

                                    }
                                    else
                                    {
                                        productsFail +=productInfo[1] + " " + productInfo[2]+";";
                                    }
                                }
                            }
                            else
                            {
                                string categoryFilterExpression = string.Format("(Name == \"{0}\")", productInfo[7]);
                                var categrories = _categoryRepository.Search(categoryFilterExpression,null).FirstOrDefault();
                                string providerFilterExpression = string.Format("(Name == \"{0}\")", productInfo[6]);
                                var providers = _providerRepository.Search(providerFilterExpression, null).FirstOrDefault();
                                Category newCate = new Category();
                                Provider newProv = new Provider();
                                if (categrories == null)
                                {
                                    newCate.Name = productInfo[7];
                                    newCate.IsActive = true;
                                    _categoryRepository.Create(newCate);
                                    _categoryRepository.SaveChanges();
                                }
                                else
                                {
                                    newCate = categrories;
                                }
                                if (providers == null)
                                {
                                    newProv.Name = productInfo[6];
                                    newProv.IsActive = true;
                                    _providerRepository.Create(newProv);
                                    _providerRepository.SaveChanges();
                                }
                                else
                                {
                                    newProv = providers;
                                }
                                var barCode = GenerateBarcode();
                                var newProduct = new Product
                                {
                                    ProviderId = newProv.Id,
                                    CategoryId = newCate.Id,
                                    BarCode = barCode,
                                    Description = (productInfo.Length >= 9) ? productInfo[8] : "",
                                    MinQuantity = 2,
                                    Name = productInfo[1] + " " + productInfo[2],
                                    SalePrice = salePrice,
                                    IsActive = true,

                                };

                                if (costPrice + (((costPrice) * 40) / 100) <= salePrice)
                                {

                                    var stockToAdd = new Stock
                                    {
                                        CostPrice = costPrice,
                                        Quantity = Int32.Parse(productInfo[5]),
                                        LocalId = localId,
                                        IsActive = true,
                                    };
                                    newProduct.Stock.Add(stockToAdd);
                                    Create(newProduct);
                                }
                                else
                                {
                                    productsFail += productInfo[1] + " " + productInfo[2] + ";";
                                }


                            }
                        }
                        catch (Exception e)
                        {
                            productsFail += productName + ";";
                        }
                    }
                }
                _productRepository.SaveChanges();
            }
            return productsFail;

        }

        public string GenerateBarcode()
        {
            var newBarcode = string.Empty;
            bool exist = true;
            Random rnd = new Random();
            while (exist)
            {
                var Mynumber = rnd.Next(100000000, 999999999);
                newBarcode = "CS" + Mynumber;
                string filterExpression = string.Format("(BarCode == \"{0}\") AND (IsActive == true)", newBarcode);
                exist = Count(filterExpression) > 0;
            }
            return newBarcode;

        }
    }
}
