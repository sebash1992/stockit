﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Dtos;
using System.Globalization;

namespace application.API.Business.Services
{
    public class SaleService : ISaleService
    {
        private IRepository<Sale> _saleRepository;
        private IRepository<Stock> _stockRepository;
        private IRepository<Product> _productRepository;

        public SaleService(IRepository<Sale> SaleRepository, IRepository<Product> productRepository, IRepository<Stock> stockRepository)
        {
            _saleRepository = SaleRepository ?? throw new ArgumentNullException(nameof(SaleRepository));
            _stockRepository = stockRepository ?? throw new ArgumentNullException(nameof(stockRepository));
            _productRepository = productRepository ?? throw new ArgumentNullException(nameof(productRepository));
        }

        public int Count(string filterExpression)
        {
            return _saleRepository.Count(filterExpression);
        }

        public void Create(Sale entity)
        {

            double cost = 0;
            entity.IsActive = true;

            foreach (var productSale in entity.SaleDetail)
            {
                var remaininQuantity = productSale.Quantity;

                string filterExpression = string.Format("(ProductId == {0}) AND  (LocalId == {1}) AND (IsActive == true)", productSale.ProductId,entity.LocalId);
                var stocks = _stockRepository.Search(filterExpression, null);
                int i = 0;
                while (remaininQuantity > 0)
                {
                    var stock = stocks.ElementAt(i);
                    if (stock.Quantity > 0)
                    {
                        if (stock.Quantity >= remaininQuantity)
                        {
                            stock.Quantity -= remaininQuantity;
                            cost += stock.CostPrice * remaininQuantity;
                            _stockRepository.Update(stock);
                            remaininQuantity = 0;
                        }
                        else
                        {
                            var quant = stock.Quantity;
                            stock.Quantity = 0;
                            stock.IsActive = false;
                            cost += quant * stock.CostPrice;
                            remaininQuantity -= quant;
                            _stockRepository.Update(stock);
                        }
                    }
                    i++;
                }
            }
            entity.Cost = cost;
            _saleRepository.Create(entity);
            _saleRepository.SaveChanges();

        }

        public void Delete(object id)
        {
            _saleRepository.Delete(id);
        }

        public IEnumerable<GraphDto> GetSalesInPeriord(int id, int localId)
        {
            string filterExpression = "";
            if(id == 1)
            {
                DateTime date = DateTime.UtcNow.AddMonths(-1);
                TimeSpan ts = new TimeSpan(23, 59, 59);
                date = date.Date + ts;
                filterExpression = string.Format("(Date > \"{0}\")", date.GetDateTimeFormats(new CultureInfo("en-US")));
            }
            else
            {
                DateTime date = DateTime.UtcNow.AddYears(-1);
                TimeSpan ts = new TimeSpan(23, 59, 59);
                date = date.Date + ts;
                filterExpression = string.Format("(Date > \"{0}\")", date.GetDateTimeFormats(new CultureInfo("en-US")));
            }

            filterExpression += " AND LocalId == " + localId;
            if (id == 1)
            {
                return Search(filterExpression, null)
                                .GroupBy(s => s.Date.Day)
                                .Select(cl => new GraphDto
                                {
                                    Label = cl.Key,
                                    Value = cl.Sum(c => c.SubTotal - c.Discount)
                                }).OrderBy(x => x.Label);
            }
            else
            {
                return Search(filterExpression, null)
                                .GroupBy(s => s.Date.Month)
                                .Select(cl => new GraphDto
                                {
                                    Label = cl.Key,
                                    Value = cl.Sum(c => c.SubTotal - c.Discount)
                                }).OrderBy(x => x.Label);
            }
            
        }

        public double GetTodaySales(int localId)
        {
            DateTime date = DateTime.UtcNow.AddDays(-1);
            TimeSpan ts = new TimeSpan(23, 59, 59);
            date = date.Date + ts;


            string filterExpression = string.Format("(Date > \"{0}\")", date.GetDateTimeFormats(new CultureInfo("en-US")));
            filterExpression += "AND localId == " + localId;
            var sales = Search(filterExpression, null);

            var salesSum = sales.Sum(s => s.SubTotal - s.Discount);
            return salesSum;
        }

        public Sale Read(object id)
        {
            return _saleRepository.Read(id);
        }

        public Sale Read(string filterExpression)
        {
            return _saleRepository.Read(filterExpression);
        }
        
        public IEnumerable<Sale> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include )
        {
            return _saleRepository.Search(filterExpression, sortingExpression, startIndex, count,include);
        }
        public IEnumerable<Sale> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null, include);
        }
        public IEnumerable<Sale> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<Sale> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }

        public void ToggleActive(object id)
        {
            var entity = Read(id);
            entity.IsActive = !entity.IsActive;
            _saleRepository.SaveChanges();
        }

        public void Update(Sale entity)
        {
            string filterExpression = string.Format("(Id = {0}) AND (IsActive == true)", entity.Id);
            var saleToEdit = Search(filterExpression, null, "SaleDetail;Local;PayMethod;SaleDetail.Product").FirstOrDefault();
            saleToEdit.PayMethodId = entity.PayMethodId;
            saleToEdit.SubTotal = entity.SubTotal;
            saleToEdit.Discount = entity.Discount;
            saleToEdit.Total = entity.SubTotal - entity.Discount;
            double cost = saleToEdit.Cost;
            foreach (var productSale in saleToEdit.SaleDetail)
            {
                var salecount = entity.SaleDetail.Count(x => x.ProductId == productSale.ProductId);
                if (salecount == 0)
                {
                    productSale.IsActive = false;
                    string stockFilterExpression = string.Format("(ProductId = {0})", productSale.Id);
                    var stockAvailable = _stockRepository.Search(stockFilterExpression,null);
                    var stockToUpdate = stockAvailable.Last();
                    stockToUpdate.Quantity += productSale.Quantity;
                    cost -= productSale.Quantity * stockToUpdate.CostPrice;
                    //_saleDetailRepo.Update(productSale);
                }

            }
            foreach (var productSale in entity.SaleDetail)
            {
                if (productSale.Id != 0)
                {
                    var saleDetailToEdit = saleToEdit.SaleDetail.Where(sd => sd.Id == productSale.Id).FirstOrDefault();

                    if (productSale.Quantity < saleDetailToEdit.Quantity)
                    {
                        string stockFilterExpression = string.Format("(ProductId = {0})", productSale.ProductId);
                        var stockAvailable = _stockRepository.Search(stockFilterExpression,null);
                        var stockToUpdate = stockAvailable.Last();
                        var difQuantity = saleDetailToEdit.Quantity - productSale.Quantity;
                        stockToUpdate.Quantity += difQuantity;
                        cost -= difQuantity * stockToUpdate.CostPrice;
                    }
                    else
                    {
                        var remaininQuantitys = productSale.Quantity - saleDetailToEdit.Quantity;
                        string stockFilterExpression = string.Format("(ProductId = {0})", productSale.ProductId);
                        var stockAvailable = _stockRepository.Search(stockFilterExpression, null);
                        int j = 0;
                        while (remaininQuantitys > 0)
                        {
                            var stock = stockAvailable.ElementAt(j);
                            if (stock.Quantity > 0)
                            {
                                if (stock.Quantity >= remaininQuantitys)
                                {
                                    stock.Quantity -= remaininQuantitys;
                                    cost += stock.CostPrice * remaininQuantitys;
                                    _stockRepository.Update(stock);
                                    remaininQuantitys = 0;
                                }
                                else
                                {
                                    var quant = stock.Quantity;
                                    stock.Quantity = 0;
                                    cost += quant * stock.CostPrice;
                                    remaininQuantitys -= quant;
                                    _stockRepository.Update(stock);
                                }
                            }
                            j++;
                        }
                    }
                    saleDetailToEdit.Quantity = productSale.Quantity;
                    //_saleDetailRepo.Update(saleDetailToEdit);
                }
                else
                {
                    var remaininQuantity = productSale.Quantity;
                    string stockFilterExpression = string.Format("(ProductId = {0})", productSale.ProductId);
                    var stocks = _stockRepository.Search(stockFilterExpression, null);
                    int i = 0;
                    while (remaininQuantity > 0)
                    {
                        var stock = stocks.ElementAt(i);
                        if (stock.Quantity > 0)
                        {
                            if (stock.Quantity >= remaininQuantity)
                            {
                                stock.Quantity -= remaininQuantity;
                                cost += stock.CostPrice * remaininQuantity;
                                _stockRepository.Update(stock);
                                remaininQuantity = 0;
                            }
                            else
                            {
                                var quant = stock.Quantity;
                                stock.Quantity = 0;
                                cost += quant * stock.CostPrice;
                                remaininQuantity -= quant;
                                _stockRepository.Update(stock);
                            }
                        }
                        i++;
                    }

                    SaleDetail productSold = new SaleDetail();
                    productSold.Price = productSale.Price;
                    productSold.Quantity = productSale.Quantity;
                    productSold.ProductId = productSale.ProductId;
                    productSold.SaleId = entity.Id;
                    saleToEdit.SaleDetail.Add(productSold);

                }
            }
            saleToEdit.Cost = cost;
            _saleRepository.Update(saleToEdit);
            _saleRepository.SaveChanges();
        }
    }
}
