﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Dtos;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Services
{
    public class TenantService : ITenantService
    {
        private IRepository<Tenant> _tenantRepository;

        public TenantService(IRepository<Tenant> TenantRepository)
        {
            _tenantRepository = TenantRepository ?? throw new ArgumentNullException(nameof(TenantRepository));
        }

        public int Count(string filterExpression)
        {
            return _tenantRepository.Count(filterExpression);
        }

        public void Create(Tenant entity)
        {
            entity.IsActive = true;
            _tenantRepository.Create(entity);
            _tenantRepository.SaveChanges();
        }

        public void Delete(object id)
        {
            _tenantRepository.Delete(id);
        }

        public Tenant Read(object id)
        {
            return _tenantRepository.Read(id);
        }

        public Tenant Read(string filterExpression)
        {
            return _tenantRepository.Read(filterExpression);
        }
        
        public IEnumerable<Tenant> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include )
        {
            return _tenantRepository.Search(filterExpression, sortingExpression, startIndex, count,include);
        }
        public IEnumerable<Tenant> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null, include);
        }
        public IEnumerable<Tenant> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<Tenant> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }

        public void ToggleActive(object id)
        {
            var entity = Read(id);
            entity.IsActive = !entity.IsActive;
            _tenantRepository.SaveChanges();
        }

        public void Update(Tenant entity)
        {
            _tenantRepository.Update(entity);
        }

    }
}
