﻿using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Dtos;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Services
{
    public class StockService : IStockService
    {
        private IRepository<Stock> _stockRepository;

        public StockService(IRepository<Stock> StockRepository)
        {
            _stockRepository = StockRepository ?? throw new ArgumentNullException(nameof(StockRepository));
        }

        public int Count(string filterExpression)
        {
            return _stockRepository.Count(filterExpression);
        }

        public void Create(Stock entity)
        {
            entity.IsActive = true;
            _stockRepository.Create(entity);
            _stockRepository.SaveChanges();
        }

        public void Delete(object id)
        {
            _stockRepository.Delete(id);
        }

        public Stock Read(object id)
        {
            return _stockRepository.Read(id);
        }

        public Stock Read(string filterExpression)
        {
            return _stockRepository.Read(filterExpression);
        }
        
        public IEnumerable<Stock> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include )
        {
            return _stockRepository.Search(filterExpression, sortingExpression, startIndex, count,include);
        }
        public IEnumerable<Stock> Search(string filterExpression, string sortingExpression, string include)
        {
            return this.Search(filterExpression, sortingExpression, null, null, include);
        }
        public IEnumerable<Stock> Search(string filterExpression, string sortingExpression)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }
        public IEnumerable<Stock> Search(string filterExpression, string sortingExpression, int? startIndex, int? count)
        {
            return this.Search(filterExpression, sortingExpression, null, null, "");
        }

        public void ToggleActive(object id)
        {
            var entity = Read(id);
            entity.IsActive = !entity.IsActive;
            _stockRepository.SaveChanges();
        }

        public void Update(Stock entity)
        {
            _stockRepository.Update(entity);
        }

        public void MoveStock(MoveStockDto entity)
        {
            string filterExpression = "LocalId == " + entity.fromLocal;
            var stocks = Search(filterExpression, null);
            int remaininQuantity = entity.quantity;
            int i = 0;
            double costPrice = 0;
            while (remaininQuantity > 0)
            {
                var stock = stocks.ElementAt(i);
                if (stock.Quantity > 0)
                {
                    if (costPrice < stock.CostPrice)
                    {
                        costPrice = stock.CostPrice;
                    }
                    if (stock.Quantity >= remaininQuantity)
                    {
                        stock.Quantity -= remaininQuantity;
                        Update(stock);
                        remaininQuantity = 0;
                    }
                    else
                    {
                        var quant = stock.Quantity;
                        stock.Quantity = 0;
                        remaininQuantity -= quant;
                        Update(stock);
                    }
                }
                i++;
            }
            if (entity.toLocal != 0)
            {
                Stock newStock = new Stock()
                {
                    CostPrice = costPrice,
                    EnterDate = DateTime.UtcNow,
                    Quantity = entity.quantity,
                    LocalId = entity.toLocal,
                    ProductId = entity.productId,
                    IsActive = true
                };

                Create(newStock);
            }
            _stockRepository.SaveChanges();
        }
    }
}
