using application.API.Common.Definitions;
using application.API.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Principal;

namespace application.API.Business.Repositories
{
    public abstract class ReadOnlyRepository<T> where T : class, IModel
    {
        internal readonly DataContext context;
        internal DbSet<T> _dbSet;
        string errorMessage = string.Empty;
        public ReadOnlyRepository(DataContext context)
        {
            this.context = context;
            _dbSet = context.Set<T>();
        }

        public virtual IEnumerable<T> Search(string filterExpression, string sortingExpression)
        {
            return Search(filterExpression, sortingExpression, null, null,"");
        }
        public virtual IEnumerable<T> Search(string filterExpression, string sortingExpression,string include)
        {
            return Search(filterExpression, sortingExpression, null, null, include);
        }

        public virtual IEnumerable<T> Search(string filterExpression, string sortingExpression, int? startIndex, int? count, string include)
        {
            IQueryable<T> query = _dbSet;
            if (!string.IsNullOrEmpty(filterExpression))
            {
                query = query.Where(filterExpression);
            }
            if (!string.IsNullOrEmpty(sortingExpression))
            {
                query = query.OrderBy(sortingExpression);
            }
            if (!string.IsNullOrEmpty(include))
            {
                string[] includes = include.Split(';');
                foreach(var inc in includes)
                {
                    query = query.Include(inc);
                }
            }
            if (startIndex.HasValue && count.HasValue)
            {
                query = query
                        .Skip((startIndex.Value - 1) * count.Value)
                        .Take(count.Value);
            }
            return query.ToList();
        }

        public virtual int Count(string filterExpression)
        {
            IQueryable<T> query = _dbSet;
            if (string.IsNullOrEmpty(filterExpression))
                return query.Count();
            return query.Where(filterExpression).Count();
        }

        public virtual T Read(object id)
        {
            return _dbSet.Find(id);
        }

        public virtual T Read(string filterExpression)
        {
            IQueryable<T> query = _dbSet;
            if (!string.IsNullOrEmpty(filterExpression))
            {
                query = query.Where(filterExpression);
                return query.FirstOrDefault();
            }
            return null;
        }
    }
}