﻿using application.API.Common.Repositories;
using application.API.Data;
using application.API.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Repositories
{
    public class StockControlDetailRepository : Repository<StockControlDetail>, IRepository<StockControlDetail>
    {
        public StockControlDetailRepository(DataContext context, IHttpContextAccessor httpContext)
            : base(context, httpContext)
        {

        }
    }
}