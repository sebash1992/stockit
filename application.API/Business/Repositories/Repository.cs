﻿using application.API.Common.Definitions;
using application.API.Common.Helpers;
using application.API.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace application.API.Business.Repositories
{
    public class Repository<T> : ReadOnlyRepository<T> where T : class, IModel
    {
        DbContext _context;
        private readonly IHttpContextAccessor _httpContext;
        public Repository(DataContext context, IHttpContextAccessor httpContext) : base(context)
        {
            _context = context;
            _httpContext = httpContext;
        }

        public virtual void Create(T entityToInsert)
        {
            if (entityToInsert is ITenantEnableModel)
            {

                ((ITenantEnableModel)entityToInsert).TenantId = getTenant();
            }
                _dbSet.Add(entityToInsert);
        }

        public virtual void Delete(object id)
        {
            T entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(string filterExpression)
        {
            var idsToDelete = Search(filterExpression, string.Empty).Select(x => x.Id);
            foreach (var id in idsToDelete)
            {
                Delete(id);
            }
        }

        public virtual void Delete(T entityToDelete)
        {
            if (_context.Entry(entityToDelete).State == EntityState.Detached)
            {
                _dbSet.Attach(entityToDelete);
            }
            _dbSet.Remove(entityToDelete);
        }

        public virtual void Update(T entityToUpdate)
        {
            if (entityToUpdate is ITenantEnableModel)
            {

                ((ITenantEnableModel)entityToUpdate).TenantId = getTenant();
            }
            _dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        private string getTenant()
        {
            var tenant = _httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.TenantId);
            var selectedtenant = _httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.SelectedTenant);
            string tenantId = "";
            if (selectedtenant != null)
            {
                tenantId = selectedtenant.Value;
            }
            else if (tenant != null)
            {
                tenantId = tenant.Value;
            }
            return tenantId;
        }
    }
}