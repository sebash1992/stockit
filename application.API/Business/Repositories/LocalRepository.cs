﻿using application.API.Common.Repositories;
using application.API.Data;
using application.API.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Repositories
{
    public class LocalRepository : Repository<Local>, IRepository<Local>
    {
        public LocalRepository(DataContext context, IHttpContextAccessor httpContext)
            : base(context, httpContext)
        {

        }
    }
}