﻿using application.API.Common.Repositories;
using application.API.Data;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Repositories
{
    public class PayMethodRepository : ReadOnlyRepository<PayMethod>, IReadOnlyRepository<PayMethod>
    {
        public PayMethodRepository(DataContext context)
            : base(context)
        {

        }
    }
}