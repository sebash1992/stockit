﻿using application.API.Common.Repositories;
using application.API.Data;
using application.API.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Business.Repositories
{
    public class SalesRepository : Repository<Sale>, IRepository<Sale>
    {
        public SalesRepository(DataContext context, IHttpContextAccessor httpContext)
            : base(context, httpContext)
        {

        }
    }
}