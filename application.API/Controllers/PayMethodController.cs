﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Common.Helpers;
using application.API.Common.Services;
using application.API.Dtos;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace application.API.Controllers
{
    [Produces("application/json")]
    [Route("api/PayMethod")]
    public class PayMethodController : BaseController
    {
        private readonly IPayMethodService _payMethodService;
        private readonly IMapper _mapper;
        public PayMethodController(IPayMethodService payMethodService, IMapper mapper)
        {
            _mapper = mapper;
            _payMethodService = payMethodService ?? throw new ArgumentNullException(nameof(payMethodService));
        }
        // GET: api/Product
        [HttpGet]
        public IActionResult Get()
        {
            var payMethods = _payMethodService.Search(GetFilterExpression(Request), GetSortExpression(Request), GetPage(Request), GetItemsPerPage(Request), "");
            var productsToReturn = _mapper.Map<IEnumerable<PayMethodListDto>>(payMethods);
            Response.AddResponseHeaders(_payMethodService.Count(GetFilterExpression(Request)));
            return Ok(productsToReturn);
        }
    }
}
