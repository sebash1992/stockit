﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Data;
using application.API.Dtos;
using application.API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using application.API.Common.Helpers;

namespace application.API.Controllers
{
    [Route("api/[controller]")]
    public class AccountsController : Controller
    {
        private readonly DataContext _appDbContext;
        private readonly UserManager<AppUser> _userManager;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        public AccountsController(UserManager<AppUser> userManager, IMapper mapper, DataContext appDbContext, IHttpContextAccessor httpContext)
        {
            _userManager = userManager;
            _mapper = mapper;
            _appDbContext = appDbContext;
            _httpContext = httpContext;
        }

        // POST api/accounts
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]UserDetailDto model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try { 
            var userIdentity = _mapper.Map<AppUser>(model);
            var tenant = _httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.TenantId);
            var selectedtenant = _httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.SelectedTenant);
            string tenantId = "";
            if (selectedtenant != null)
            {
                tenantId = selectedtenant.Value;
            }
            else if (tenant != null)
            {
                tenantId = tenant.Value;
            }
                userIdentity.TenantId = tenantId;





                if (userIdentity.Id != null)
            {
                    var user = await _userManager.FindByIdAsync(userIdentity.Id);
                    user.FirstName = userIdentity.FirstName;
                    user.LastName = userIdentity.LastName;
                    var result = await _userManager.UpdateAsync(user);
                    if(!await _userManager.IsInRoleAsync(userIdentity, model.Role))
                    {
                        var roles = await _userManager.GetRolesAsync(user);
                        await _userManager.RemoveFromRolesAsync(user, roles);
                        var roleResult = await _userManager.AddToRoleAsync(user, model.Role);
                    }
            }
            else
            {
                var result = await _userManager.CreateAsync(userIdentity, model.Password);
                if (result == IdentityResult.Success)
                {
                    var roleResult = await _userManager.AddToRoleAsync(userIdentity, model.Role);
                }
                else
                {
                    return BadRequest(result.Errors);
                }

                //if (!result.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(result, ModelState));
                
                await _appDbContext.SaveChangesAsync();
            }


            return  Ok();
            }catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpGet]
        [Route("GetRoles")]
        public IActionResult Get()
        {
            var roles = _appDbContext.Roles.ToList();
            var rolesToReturn = _mapper.Map<IEnumerable<RoleListDto>>(roles);
            return Ok(rolesToReturn);
        }
        [HttpGet]
        [Route("GetUsers")]
        public  async Task<IActionResult> GetUsers()
        {
            var users = _userManager.Users
                                    .Select(u => new UserListDto
                                    {
                                        FirstName = u.FirstName,
                                        LastName = u.LastName,
                                        Email = u.Email,
                                        Id = u.Id
                                    });
            //await UserManager.FindByIdAsync("00b9c6aa-010c-4cbd-ac16-c72d05e7906a");
            List<UserListDto> tor = new List<UserListDto>();
            foreach (var user in users)
            {
                user.Role = (await _userManager.GetRolesAsync(await _userManager.FindByIdAsync(user.Id))).FirstOrDefault();
                tor.Add(user);

            }
            return Ok(tor.OrderBy(x => x.Email));
            
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                await _userManager.DeleteAsync(user);
                return Ok();
            }catch(Exception e)
            {
                return BadRequest();
            }
        }


        [HttpGet]
        [Route("GetUser/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            if (id != "0")
            {
                var result = await _userManager.FindByIdAsync(id);
                if (result != null)
                {
                    var userToReturn = _mapper.Map<UserDetailDto>(result);
                    return Ok(userToReturn);
                }
                return Ok(null);
            }
            {
                return Ok(new UserDetailDto());
            }
        }
    }
}