﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Common.Helpers;
using application.API.Common.Services;
using application.API.Dtos;
using application.API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace application.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Provider")]
    public class ProviderController : BaseController
    {
        private readonly IProviderService _providerService;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        public ProviderController(IHttpContextAccessor httpContext,IProviderService providerService, IMapper mapper)
        {
            _httpContext = httpContext;
            _mapper = mapper;
            _providerService = providerService ?? throw new ArgumentNullException(nameof(providerService));
        }
        // GET: api/Product
        [HttpGet]
        public IActionResult Get()
        {
            var categories = _providerService.Search(GetFilterExpression(Request), GetSortExpression(Request), GetPage(Request), GetItemsPerPage(Request), "");
            var productsToReturn = _mapper.Map<IEnumerable<ProviderListDto>>(categories);
            Response.AddResponseHeaders(_providerService.Count(GetFilterExpression(Request)));
            return Ok(productsToReturn);
        }

        [HttpGet]
        [Route("GetProvider/{id}")]
        public IActionResult GetProvider(int id)
        {
            if (id != 0)
            {
                string filterExpression = string.Format("(Id = {0}) AND (IsActive == true)", id);
                var prov = _providerService.Search(filterExpression, null, "ProviderDoubts").FirstOrDefault();
                if (prov != null)
                {
                    var productToReturn = _mapper.Map<ProviderDetailDto>(prov);
                    return Ok(productToReturn);
                }
                return Ok(null);
            }
            {
                return Ok(new ProviderDetailDto());
            }
        }


        [HttpGet]
        [Route("GetProviders")]
        public IActionResult GetProviders()
        {
                string filterExpression = string.Format("(IsActive == true)");
                var prov = _providerService.Search(filterExpression, null, "ProviderDoubts");
                var productToReturn = _mapper.Map<IEnumerable<ProviderDetailDto>>(prov);
                return Ok(productToReturn);                
        }

        [HttpPost]
        public IActionResult Post([FromBody]ProviderDetailDto provider)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            var prov = _mapper.Map<Provider>(provider);
            prov.IsActive = true;
            if (prov.Id == 0)
            {
                _providerService.Create(prov);
            }
            else
            {
                _providerService.Update(prov);
            }
            return Ok();
        }

        protected new string GetFilterExpression(HttpRequest request)
        {
            StringValues filter;
            StringValues filterValue;
            string filterExpression = "";
            if (Request.Headers["search"].Count() > 0 && Request.Headers["searchBy"].Count() > 0)
            {
                Request.Headers.TryGetValue("searchBy", out filter);
                Request.Headers.TryGetValue("search", out filterValue);

                var columns = filter.FirstOrDefault<string>().Split(';');
                foreach (var column in columns)
                {
                    if (filterExpression == "")
                    {
                        filterExpression = string.Format("{0}.Contains(\"{1}\")", column, filterValue.FirstOrDefault<string>());
                    }
                    else
                    {
                        filterExpression = string.Format("{0} AND {1}.Contains(\"{2}\")", filterExpression, column, filterValue.FirstOrDefault<string>());
                    }
                }


            }
            if (Request.Headers["localId"].Count() > 0)
            {
                Request.Headers.TryGetValue("localId", out filter);

                if (filterExpression == "")
                {
                    filterExpression = string.Format("LocalId == {0}", Int32.Parse(filter.FirstOrDefault<string>()));
                }
                else
                {
                    filterExpression = string.Format("{0} AND LocalId == {1}", filterExpression, Int32.Parse(filter.FirstOrDefault<string>()));
                }

            }
            if (filterExpression == "")
            {
                return null;
            }
            else
            {
                return filterExpression;
            }
        }

    }
}
