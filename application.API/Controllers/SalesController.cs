﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Common.Helpers;
using application.API.Common.Services;
using application.API.Dtos;
using application.API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace application.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Sales")]
    public class SalesController : BaseController
    {
        private readonly ISaleService _saleService;
        private readonly IMapper _mapper;
        public SalesController(ISaleService saleService, IMapper mapper)
        {
            _mapper = mapper;
            _saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
        }

        [HttpGet]
        public IActionResult Get()
        {
            var sales = _saleService.Search(GetFilterExpression(Request), GetSortExpression(Request), GetPage(Request), GetItemsPerPage(Request), "PayMethod");
            var salesToReturn = _mapper.Map<IEnumerable<SaleListDto>>(sales);
            Response.AddResponseHeaders(_saleService.Count(GetFilterExpression(Request)));
            return Ok(salesToReturn);
        }

        [HttpGet("{id}", Name = "GetSale")]
        public IActionResult Get(int id)
        {
            if (id != 0)
            {
                string filterExpression = string.Format("(Id = {0}) AND (IsActive == true)", id);
                var sale = _saleService.Search(filterExpression, null, "SaleDetail;Local;PayMethod;SaleDetail.Product;SaleServiceDetail").FirstOrDefault();
                if (sale != null)
                {
                    var saleToReturn = _mapper.Map<SaleDto>(sale);
                    return Ok(saleToReturn);
                }
                return Ok(null);
            }
            {
                return Ok(new SaleDto());
            }
        }

        // POST: api/Sales
        [HttpPost]
        public IActionResult Post([FromBody]SaleDto sale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var entity = _mapper.Map<Sale>(sale);
            if (sale.Id == 0)
            {
                _saleService.Create(entity);
            }
            else
            {
                _saleService.Update(entity);
            }
            return Ok();
        }
    }
}
