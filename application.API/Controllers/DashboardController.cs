﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Common.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using application.API.Dtos;
using System.Globalization;
using application.API.Models;
using Microsoft.Extensions.Primitives;

namespace application.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Dashboard")]
    public class DashboardController : Controller
    {
        private readonly ISaleService _saleService;
        private readonly IExpenseService _expenseService;
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        public DashboardController(ISaleService saleService, IMapper mapper, IExpenseService expenseService, IProductService productService)
        {
            _mapper = mapper;
            _saleService = saleService ?? throw new ArgumentNullException(nameof(saleService));
            _expenseService = expenseService ?? throw new ArgumentNullException(nameof(expenseService));
            _productService = productService ?? throw new ArgumentNullException(nameof(productService));

        }

        [HttpGet]
        [Route("GetMetrics")]
        public IActionResult Get()
        {
            StringValues filter;
            Request.Headers.TryGetValue("localId", out filter);
            int localId = Int32.Parse(filter.FirstOrDefault<string>());
            try { 
                DashbardMetricsDto metrics = new DashbardMetricsDto();

                metrics.TodaySales = _saleService.GetTodaySales(localId);
                metrics.TodayExpenses = _expenseService.GetTodayExpenses(localId);
                metrics.ProductWithMinimumStock = _productService.ProductWithMinimumStock(localId).Count();
                metrics.ProductWithMostSales = _productService.ProductWithMostSales(localId);
                return Ok(metrics);
            }catch(Exception e)
            {
                return BadRequest("Ups, Something happened");
            }
        }


        [HttpGet]
        [Route("GetSalesExpenses/{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                StringValues filter;
                Request.Headers.TryGetValue("localId", out filter);
                int localId = Int32.Parse(filter.FirstOrDefault<string>());
                var expenses = _expenseService.GetExpensesInPeriod(id, localId);
                var sales = _saleService.GetSalesInPeriord(id, localId);
                var tor = GetGraphData(expenses, sales, id);
                return Ok(tor);
            }
            catch (Exception e)
            {
                return BadRequest("Ups, Something happened");
            }
        }

        [HttpPost]
        [Route("GetStocktaking")]
        public IActionResult GetStocktaking([FromBody]ProviderSearchDoubtDto search)
        {
            StringValues filter;
            Request.Headers.TryGetValue("localId", out filter);
            int localId = Int32.Parse(filter.FirstOrDefault<string>());

            string filterExpressionFrom = string.Format("(Date >= \"{0}\") ", search.From.ToUniversalTime().GetDateTimeFormats(new CultureInfo("en-US")));
            string filterExpressionTo = string.Format("(Date < \"{0}\") ", search.To.ToUniversalTime().GetDateTimeFormats(new CultureInfo("en-US")));
            string filterExpression = string.Format("{0} AND {1} AND LocalId == {2}", filterExpressionFrom, filterExpressionTo,localId);

            IEnumerable<Sale> sales = _saleService.Search(filterExpression, null);
            IEnumerable<Expense> expenses = _expenseService.Search(filterExpression, null);

            ReportDto customMapper = new ReportDto();
            var tor = customMapper.mergeSalesAndExpenses(sales, expenses, search);


            return Ok(tor);
        }





        private SalesAndExpensesDto GetGraphData(IEnumerable<GraphDto> expenses, IEnumerable<GraphDto> sales, int id)
        {
            SalesAndExpensesDto tor = new SalesAndExpensesDto();

            if(id == 1)
            {
                int month = DateTime.DaysInMonth(DateTime.UtcNow.Year, DateTime.UtcNow.AddMonths(-1).Month);
                int today = DateTime.UtcNow.Day;
                for (int i = 1 ; i < 31; i++)
                {
                    double expenseToAdd = 0;
                    double saleToAdd = 0;

                    int search = ((today + i) % month) + 1;
                    var expense = expenses.Where(x => x.Label == search);
                    var sale = sales.Where(x => x.Label == search);
                    if (sale.Count() > 0)
                    {
                        saleToAdd = sale.FirstOrDefault().Value;
                    }
                    if (expense.Count() > 0)
                    {
                        expenseToAdd = expense.FirstOrDefault().Value;
                    }
                    tor.Expenses.Add(expenseToAdd);
                    tor.Sales.Add(saleToAdd);
                    tor.Labels.Add(search.ToString());
                }
            }
            else
            {
                int today = DateTime.UtcNow.Month;
                for (int i = 0; i < 12; i++)
                {
                    double expenseToAdd = 0;
                    double saleToAdd = 0;


                    int search = ((today + i) % 12) +1;
                    var expense = expenses.Where(x => x.Label == search);
                    var sale = sales.Where(x => x.Label == search);
                    if (sale.Count() > 0)
                    {
                        saleToAdd = sale.FirstOrDefault().Value;
                    }
                    if (expense.Count() > 0)
                    {
                        expenseToAdd = expense.FirstOrDefault().Value;
                    }
                    tor.Expenses.Add(expenseToAdd);
                    tor.Sales.Add(saleToAdd);
                    tor.Labels.Add(GetMonthName(search));
                }
            }



            return tor;

        }

        private string GetMonthName(int monthId)
        {
            switch (monthId)
            {
                case 1:
                    return "Enero";
                case 2:
                    return "Febrero";
                case 3:
                    return "Marzo";
                case 4:
                    return "Abril";
                case 5:
                    return "Mayo";
                case 6:
                    return "Junio";
                case 7:
                    return "Julio";
                case 8:
                    return "Agosto";
                case 9:
                    return "Septiembre";
                case 10:
                    return "Octubre";
                case 11:
                    return "Noviembre";
                case 12:
                    return "Diciembre";

            }
            return "";
        }
    }

}