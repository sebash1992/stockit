﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using application.API.Common.Helpers;
using AutoMapper;
using application.API.Dtos;

namespace application.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Local")]
    public class LocalController : BaseController
    {
        private readonly ILocalService _localService;
        private readonly IMapper _mapper;

        public LocalController(ILocalService localService, IMapper mapper)
        {
            _mapper = mapper;
            _localService = localService ?? throw new ArgumentNullException(nameof(localService));
        }


        // GET: api/Local
        [HttpGet]
        public IActionResult Get()
        {
           var prods = _localService.Search(GetFilterExpression(Request), GetSortExpression(Request), GetPage(Request), GetItemsPerPage(Request), "");
            var productsToReturn = _mapper.Map<IEnumerable<LocalDto>>(prods);
            Response.AddResponseHeaders(_localService.Count(GetFilterExpression(Request)));
            return Ok(productsToReturn);
        }


    }
}
