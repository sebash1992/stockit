﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using application.API.Common.Helpers;
using AutoMapper;
using application.API.Dtos;
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Authorization;

namespace application.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Expense")]
    public class ExpenseController : BaseController
    {
        private readonly IExpenseService _ExpenseService;
        private readonly IMonthlyExpenseService _monthlyExpenseService;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;

        public ExpenseController(IHttpContextAccessor httpContext, IExpenseService ExpenseService, IMonthlyExpenseService monthlyExpenseService, IMapper mapper)
        {
            _httpContext = httpContext;
            _mapper = mapper;
            _ExpenseService = ExpenseService ?? throw new ArgumentNullException(nameof(ExpenseService));
            _monthlyExpenseService = monthlyExpenseService ?? throw new ArgumentNullException(nameof(monthlyExpenseService));
        }


        // GET: api/Expense
        [HttpGet]
        public IActionResult Get()
        {

            string filterExpression = GetFilterExpression(Request);
            string monthlyFilterExpresion = "";
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                monthlyFilterExpresion = "IsMonthly == " + false;

                if (filterExpression == "")
                {
                    filterExpression = monthlyFilterExpresion;
                }
                else
                {
                    filterExpression = string.Format("{0} AND {1}", filterExpression, monthlyFilterExpresion);
                }
            }

            var expenses = _ExpenseService.Search(filterExpression, GetSortExpression(Request), GetPage(Request), GetItemsPerPage(Request), "");
            Response.AddResponseHeaders(_ExpenseService.Count(GetFilterExpression(Request)));
            return Ok(expenses);
        }


        [HttpGet]
        [Route("GetMonthlyExpense")]
        public IActionResult GetMonthlyExpense()
        {
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            string filterExpression = GetFilterExpression(Request);
            var monthlyExpenses = _monthlyExpenseService.Search(filterExpression, GetSortExpression(Request), GetPage(Request), GetItemsPerPage(Request), "");
            Response.AddResponseHeaders(_ExpenseService.Count(GetFilterExpression(Request)));
            return Ok(monthlyExpenses);
        }


        [HttpPost]
        public IActionResult Post([FromBody] Expense entity)
        {
            try { 
            if (entity == null)
            {
                return BadRequest();
            }
            entity.Date = entity.Date;
            _ExpenseService.Create(entity);
            return Ok();
            }catch(Exception e)
            {
                return BadRequest();
            }
        }
        [HttpPost]
        [Route("CreateME")]
        public IActionResult CreateME([FromBody] MonthlyExpenses entity)
        {
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            if (entity == null)
            {
                return BadRequest();
            }
            _monthlyExpenseService.Create(entity);
            return Ok();
        }


        [HttpPost]
        [Route("PayAllMonthlyExpense")]
        public IActionResult PayAllMonthlyExpense()
        {
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            try
            {
                _monthlyExpenseService.PayAll();
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

        [HttpPost]
        [Route("PayMonthlyExpense")]
        public IActionResult PayMonthlyExpense([FromBody]MonthlyExpenses me)
        {
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            try { 
            MonthlyExpenses expense = _monthlyExpenseService.Read(me.Id);
            Expense newExpense = new Expense()
            {
                Amount = expense.Amount,
                IsMonthly = true,
                LocalId = expense.LocalId,
                Description = me.Description,
                Date = DateTime.UtcNow

            };
            _ExpenseService.Create(newExpense);
            return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }
        [HttpPost]
        [Route("RemoveMonthlyExpense")]
        public IActionResult RemoveMonthlyExpense([FromBody]MonthlyExpenses me)
        {
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            try
            {
                _monthlyExpenseService.Delete(me.Id);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }


        protected new string GetFilterExpression(HttpRequest request)
        {
            StringValues filter;
            StringValues filterValue;
            string filterExpression = "";
            if (Request.Headers["search"].Count() > 0 && Request.Headers["searchBy"].Count() > 0)
            {
                Request.Headers.TryGetValue("searchBy", out filter);
                Request.Headers.TryGetValue("search", out filterValue);

                var columns = filter.FirstOrDefault<string>().Split(';');
                foreach (var column in columns)
                {
                    if (filterExpression == "")
                    {
                        filterExpression = string.Format("{0}.Contains(\"{1}\")", column, filterValue.FirstOrDefault<string>());
                    }
                    else
                    {
                        filterExpression = string.Format("{0} AND {1}.Contains(\"{2}\")", filterExpression, column, filterValue.FirstOrDefault<string>());
                    }
                }


            }
            if (Request.Headers["localId"].Count() > 0)
            {
                Request.Headers.TryGetValue("localId", out filter);

                if (filterExpression == "")
                {
                    filterExpression = string.Format("LocalId == {0}", Int32.Parse(filter.FirstOrDefault<string>()));
                }
                else
                {
                    filterExpression = string.Format("{0} AND LocalId == {1}", filterExpression, Int32.Parse(filter.FirstOrDefault<string>()));
                }

            }
            if (filterExpression == "")
            {
                return null;
            }
            else
            {
                return filterExpression;
            }
        }

    }
}
