﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using application.API.Common.Models;
using Newtonsoft.Json;

namespace application.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Base")]
    public class BaseController : Controller
    {
        protected string GetFilterExpression(HttpRequest request)
        {
            StringValues filter;
            StringValues filterValue;
            string filterExpression = "";
            if(Request.Headers["search"].Count() > 0 && Request.Headers["searchBy"].Count() > 0)
            {
                Request.Headers.TryGetValue("searchBy", out filter);
                Request.Headers.TryGetValue("search", out filterValue);

                var columns = filter.FirstOrDefault<string>().Split(';');
                foreach (var column in columns)
                {
                    if (filterExpression == "")
                    {
                        filterExpression = string.Format("{0}.Contains(\"{1}\")", column, filterValue.FirstOrDefault<string>());
                    }
                    else
                    {
                        filterExpression = string.Format("{0} OR {1}.Contains(\"{2}\")", filterExpression, column, filterValue.FirstOrDefault<string>());
                    }
                }

                
            }
            if (Request.Headers["localId"].Count() > 0)
            {
                Request.Headers.TryGetValue("localId", out filter);

                if(filterExpression == "")
                {
                    filterExpression = string.Format("LocalId == {0}", Int32.Parse(filter.FirstOrDefault<string>()));
                }
                else
                {
                    filterExpression = string.Format("{0} AND LocalId == {1}", filterExpression, Int32.Parse(filter.FirstOrDefault<string>()));
                }

            }
            if (Request.Headers["isActive"].Count() > 0)
            {
                Request.Headers.TryGetValue("isActive", out filter);
                string isActiveFilter = "(IsActive == true)";
                if (filter.FirstOrDefault<string>() == "0")
                {
                    isActiveFilter = "(IsActive == false)";
                }
                if (filterExpression == "")
                {
                    filterExpression = isActiveFilter;
                }
                else
                {
                    filterExpression = string.Format("{0} AND {1}", filterExpression, isActiveFilter);
                }

            }
            if (filterExpression == "")
            {
                return null;
            }
            else
            {
                return filterExpression;
            }
        }

        protected string GetSortExpression(HttpRequest request)
        {
            StringValues sortBy;
            StringValues sortOrder;
            string sortExpression = "";
            if (Request.Headers["orderBy"].Count() > 0 && Request.Headers["orderType"].Count() > 0)
            {
                Request.Headers.TryGetValue("orderBy", out sortBy);
                Request.Headers.TryGetValue("orderType", out sortOrder);
                sortExpression = sortBy.FirstOrDefault<string>() + " " + sortOrder.FirstOrDefault<string>();
            }
            if(sortExpression == "")
            {
                return null;
            }
            else
            {
                return sortExpression;
            }
        }




        protected int? GetPage(HttpRequest request)
        {
            StringValues page;
            int requestePage = 0;
            if (Request.Headers["pageNumber"].Count() > 0 && Request.Headers["itemsperPage"].Count() > 0)
            {
                 Request.Headers.TryGetValue("pageNumber", out page);
                requestePage = Int32.Parse(page.FirstOrDefault<string>());
            }
            if (requestePage == 0)
            {
                return null;
            }
            else
            {
                return requestePage;
            }
        }


        protected int? GetItemsPerPage(HttpRequest request)
        {
            StringValues page;
            int requestePage = 0;
            if (Request.Headers["pageNumber"].Count() > 0 && Request.Headers["itemsperPage"].Count() > 0)
            {
                Request.Headers.TryGetValue("itemsperPage", out page);
                requestePage = Int32.Parse(page.FirstOrDefault<string>());
            }
            if (requestePage == 0)
            {
                return null;
            }
            else
            {
                return requestePage;
            }
        }
    }
}

//if (search !== '' && searchBy !== '') {
//    header.set('Search', search);
//}
//if (orderBy !== '' && orderType !== '' ) {
//    header.set('orderBy', orderBy);
//    header.set('orderType', orderType);
//}
//if (pageNumber !== 0 && orderType !== 0 ) {
//    header.set('pageNumber', pageNumber + '');
//    header.set('itemsperPage', itemsperPage + '');
//}
//if (localId !== 0) {
//    header.set('localId', '' + localId);
//}
//if (isActive) {
//    header.set('isActive', '1');
//} else {
//    header.set('isActive', '0');
//}