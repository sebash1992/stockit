﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Common.Helpers;
using application.API.Common.Services;
using application.API.Dtos;
using application.API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace application.API.Controllers
{

    [Produces("application/json")]
    [Route("api/ProviderDoubt")]
    public class ProviderDoubtController : BaseController
    {

        private readonly IProviderDoubtService _providerService;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        public ProviderDoubtController(IHttpContextAccessor httpContext,IProviderDoubtService providerService, IMapper mapper)
        {
            _httpContext = httpContext;
            _mapper = mapper;
            _providerService = providerService ?? throw new ArgumentNullException(nameof(providerService));
        }

        // GET: api/ProviderDoubt
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ProviderDoubt/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/ProviderDoubt
        [HttpPost]
        public IActionResult Post([FromBody]ProviderDoubtDto value)
        {
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var doubt = _mapper.Map<ProviderDoubts>(value);
            _providerService.Create(doubt);
            return Ok();
        }
        
        // PUT: api/ProviderDoubt/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost]
        [Route("GetProviderDubt")]
        public IActionResult GetProviderDubt([FromBody]ProviderSearchDoubtDto provider)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(_providerService.GetProviderDoubt(provider));
        }
    }
}
