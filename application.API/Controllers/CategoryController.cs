﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Common.Helpers;
using application.API.Common.Services;
using application.API.Dtos;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace application.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Category")]
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;
        public CategoryController(ICategoryService categoryService, IMapper mapper)
        {
            _mapper = mapper;
            _categoryService = categoryService ?? throw new ArgumentNullException(nameof(categoryService));
        }
        // GET: api/Product
        [HttpGet]
        public IActionResult Get()
        {
            var categories = _categoryService.Search(GetFilterExpression(Request), GetSortExpression(Request), GetPage(Request), GetItemsPerPage(Request), "");
            var productsToReturn = _mapper.Map<IEnumerable<CategoryListDto>>(categories);
            Response.AddResponseHeaders(_categoryService.Count(GetFilterExpression(Request)));
            return Ok(productsToReturn);
        }
    }
}
