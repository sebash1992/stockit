﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Common.Services;
using application.API.Dtos;
using application.API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using application.API.Common.Helpers;
using Microsoft.AspNetCore.Authorization;

namespace application.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/StockControl")]
    public class StockControlController : BaseController
    {
        private readonly IStockControlService _stockControlService;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        public StockControlController(IHttpContextAccessor httpContext, IStockControlService stockControlService, IMapper mapper)
        {
            _httpContext = httpContext;
            _mapper = mapper;
            _stockControlService = stockControlService ?? throw new ArgumentNullException(nameof(stockControlService));
        }
        // GET: api/Product
        [HttpGet]
        public IActionResult Get()
        {
            var stocks = _stockControlService.Search(GetFilterExpression(Request), GetSortExpression(Request),GetPage(Request) , GetItemsPerPage(Request),"");
            Response.AddResponseHeaders(_stockControlService.Count(GetFilterExpression(Request)));
            return Ok(stocks);
        }

        // GET: api/Product/5
        [HttpGet]
        [Route("GetStockControl/{id}")]
        public IActionResult Get(int id)
        {
            if (id != 0)
            {
                string filterExpression = string.Format("(Id = {0}) AND (IsActive == true)", id);
                var stockControl = _stockControlService.Search(filterExpression, null, "StockControlDetail;StockControlDetail.Product").FirstOrDefault();
                var stockControlToReturn = _mapper.Map<StockControlDto>(stockControl);
                return Ok(stockControlToReturn);
            }
            {
                return BadRequest();
            }
        }

        // POST: api/Product
        [HttpPost]
        public IActionResult Post([FromBody]StockControl stockControl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _stockControlService.Create(stockControl);
            return Ok(stockControl.Id);
        }

        [HttpPost]
        [Route("SaveStockControl")]
        public IActionResult SaveStockControl([FromBody]StockControlDto stockControl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var stockC = _mapper.Map<StockControl>(stockControl);
            _stockControlService.Update(stockC);
            return Ok();
        }

        [HttpPost]
        [Route("FinishStockControl/{id}")]
        public IActionResult FinishStockControl(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
             _stockControlService.FinishControl(id);
            return Ok();
        }


        [HttpPost("[action]")]
        public IActionResult CreateRandomStockControl([FromBody] StockControl stockControl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _stockControlService.CreateRandom(stockControl);
            return Ok(stockControl.Id);
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            _stockControlService.ToggleActive(id);
            return Ok();
        }
    }
}
