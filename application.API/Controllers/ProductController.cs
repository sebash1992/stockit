﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using application.API.Common.Services;
using application.API.Dtos;
using application.API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using application.API.Common.Helpers;
using Microsoft.AspNetCore.Authorization;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Primitives;

namespace application.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Product")]
    public class ProductController : BaseController
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IProviderService _providerService;
        private readonly IStockService _stockService;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContext;
        public ProductController(IHttpContextAccessor httpContext,ICategoryService categoryService, IStockService stockService, IProviderService providerService, IProductService productService, IMapper mapper)
        {
            _httpContext = httpContext;
            _mapper = mapper;
            _productService = productService ?? throw new ArgumentNullException(nameof(productService));
            _categoryService = categoryService ?? throw new ArgumentNullException(nameof(categoryService));
            _providerService = providerService ?? throw new ArgumentNullException(nameof(providerService));
            _stockService = stockService ?? throw new ArgumentNullException(nameof(stockService));
        }
        // GET: api/Product
        [HttpGet]
        public IActionResult Get()
        {
            var prods =  _productService.Search(GetFilterExpression(Request), GetSortExpression(Request),GetPage(Request) , GetItemsPerPage(Request),"Category;Provider");
            var productsToReturn = _mapper.Map<IEnumerable<ProductListDto>>(prods);
            Response.AddResponseHeaders(_productService.Count(GetFilterExpression(Request)));
            return Ok(productsToReturn);
        }

        [HttpGet]
        [Route("GenerateBarcode")]
        public IActionResult GenerateBarcode()
        {
            var barCode = _productService.GenerateBarcode();

            return Ok(barCode);
        }

        // GET: api/Product/5
        [HttpGet]
        [Route("GetProduct/{id}")]
        public IActionResult Get(int id)
        {
            if(id != 0) { 
            string filterExpression = string.Format("(Id = {0}) AND (IsActive == true)", id);
            var prod = _productService.Search(filterExpression, null, "Category;Provider;Stock;Stock.Local").FirstOrDefault();
            if(prod!= null)
            {
                var productToReturn = _mapper.Map<ProductDetailDto>(prod);
                return Ok(productToReturn);
            }
            return Ok(null);
            }
            {
                return Ok(new ProductDetailDto());
            }
        }

        [HttpGet]
        [Route("GetProductsDetailList/{categoryId}/{providerId}")]
        public IActionResult Get(int categoryId, int providerId )
        {
                string filterExpression = string.Format("(IsActive == true)");
                if(categoryId != 0)
                {
                    filterExpression += " AND CategoryId == " + categoryId;
                }
                if (providerId != 0)
                {
                    filterExpression += " AND ProviderId == " + providerId;
                }
                var prod = _productService.Search(filterExpression, null, "Category;Provider;Stock;Stock.Local");

                var productToReturn = _mapper.Map<IEnumerable<ProductDetailDto>>(prod);
                return Ok(productToReturn);
                
        }

        [HttpGet]
        [Route("GetProductWithMinimumStock")]
        public IActionResult GetProductWithMinimumStock()
        {
            try
            {
                StringValues filter;
                Request.Headers.TryGetValue("localId", out filter);
                int localId = Int32.Parse(filter.FirstOrDefault<string>());
                IEnumerable<Product> prod = _productService.ProductWithMinimumStock(localId);
                var productToReturn = _mapper.Map<IEnumerable<ProductDetailDto>>(prod);
                return Ok(productToReturn);
            }
            catch (Exception e)
            {
                return BadRequest("Ups, Something happened");
            }
        }




        [HttpGet]
        [Route("GetProducts/{search}")]
        public IActionResult GetProducts(string search)
        {
            if (!string.IsNullOrEmpty(search.Trim()))
            {
                string filterExpression = string.Format("(barCode.Contains(\"{0}\") OR name.Contains(\"{1}\")) AND (IsActive == true)", search, search);
                var prod = _productService.Search(filterExpression, null, "Category;Provider;Stock;Stock.Local");
                if (prod != null)
                {
                    var productToReturn = _mapper.Map<ProductDetailDto[]>(prod);
                    Response.AddResponseHeaders(_productService.Count(GetFilterExpression(Request)));
                    return Ok(productToReturn);
                }
                return Ok(null);
            }
            {
                return Ok(new ProductDetailDto());
            }
        }
        
        [HttpPost]
        [Route("CreateProductsFromFile/{localId}")]
        public IActionResult CreateProductsFromFile(IFormFile file,int localId)
        {
            try
            {
                if (file == null) return BadRequest("El archivo no es compatible");
                if (file.Length == 0) return BadRequest("El archivo se encuentra vacio");
                var products = _productService.CreateProductFromFile(file, localId);

                Regex reg = new Regex("[*'\",_&#^@]");
                products = reg.Replace(products, string.Empty);
                return Ok(products);
            }
            catch (Exception)
            {
                return BadRequest("Hubo un problema al cargar los productos.");
            }
        }
        [HttpGet("{barcode}", Name = "GetByBarCode")]
        [Route("GetByBarCode/{barcode}")]
        public IActionResult GetByBarCode(string barcode)
        {
            if (!string.IsNullOrEmpty(barcode.Trim()))
            {
                string filterExpression = string.Format("(barCode == \"{0}\") AND (IsActive == true)", barcode);
                var prod = _productService.Search(filterExpression, null, "Category;Provider;Stock;Stock.Local").FirstOrDefault();
                if (prod != null)
                {
                    var productToReturn = _mapper.Map<ProductDetailDto>(prod);
                    return Ok(productToReturn);
                }
                return Ok(null);
            }
            {
                return Ok(new ProductDetailDto());
            }
        }

        // POST: api/Product
        [HttpPost]
        public IActionResult Post([FromBody]ProductDetailDto product)
        {
            if(_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }           

            var prod = _mapper.Map<Product>(product);
            prod.IsActive = true;
            if(prod.CategoryId == 0)
            {
                Category newCategory = _mapper.Map<Category>(product.Category);
                _categoryService.Create(newCategory);
                prod.CategoryId = newCategory.Id;
            }
            if (prod.ProviderId == 0)
            {
                Provider newProvider = _mapper.Map<Provider>(product.Provider);
                _providerService.Create(newProvider);
                prod.ProviderId = newProvider.Id;
            }
            if(prod.Id == 0)
            {
                Stock newStock = _mapper.Map<Stock>(product.Stocks.FirstOrDefault());
                newStock.IsActive = true;
                newStock.CostPrice = product.CostPrice;
                prod.Stock = new List<Stock>() { newStock };
                _productService.Create(prod);
            }
            else
            {
                _productService.Update(prod);
            }
            return Ok();
        }

        [HttpPost("[action]")]
        public IActionResult CreateStock([FromBody] ProductStockEditorDto entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            Stock newStock = _mapper.Map<Stock>(entity);
            _stockService.Create(newStock);
            _productService.UpdateSalePrice(entity.ProductId, entity.SalePrice);
            return Ok();
        }


        [HttpPost("[action]")]
        public IActionResult MoveStock([FromBody] MoveStockDto entity)
        {
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            _stockService.MoveStock(entity);
            return Ok();
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (_httpContext.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.UserRole).Value != Constants.Strings.UserRoles.Administrator)
            {
                return Unauthorized();
            }
            _productService.Delete(id);
            return Ok();
        }
    }
}
