﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class ProviderSearchDoubtDto
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int ProviderId { get; set; }
    }
}
