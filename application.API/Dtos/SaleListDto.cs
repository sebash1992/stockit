﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class SaleListDto
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string PayMethod { get; set; }
        public double Total { get; set; }
    }
}
