﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class MoveStockDto
    {
        public int quantity { get; set; }
        public int fromLocal { get; set; }
        public int toLocal { get; set; }
        public int productId { get; set; }

    }
}
