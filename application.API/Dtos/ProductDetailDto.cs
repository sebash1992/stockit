﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class ProductDetailDto
    {
        public int Id { get; set; }
        public string BarCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public CategoryListDto Category { get; set; }
        public ProviderListDto Provider { get; set; }
        public string ProductLink { get; set; }
        public double SalePrice { get; set; }
        public double CostPrice { get; set; }
        public int MinQuantity { get; set; }
        public IEnumerable<ProductStockDto> Stocks{get;set;}
    }
}
