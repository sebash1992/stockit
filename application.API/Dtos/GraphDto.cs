﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class GraphDto
    {
        public int Label { get; set; }
        public double Value { get; set; }
    }

    public class SalesAndExpensesDto
    {
        public SalesAndExpensesDto()
        {
            Labels = new List<string>();
            Sales = new List<double>();
            Expenses = new List<double>();
        }

        public List<string> Labels { get; set; }
        public List<double> Sales { get; set; }
        public List<double> Expenses { get; set; }
    }
}
