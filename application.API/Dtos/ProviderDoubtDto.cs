﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class ProviderDoubtDto
    {
        public int ProviderId { get; set; }
        public double Amount { get; set; }
        public string Description { get; set; }
    }
}
