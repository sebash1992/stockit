﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class ProductStockEditorDto
    {
        public int CostPrice { get; set; }
        public int SalePrice { get; set; }
        public int ProductId { get; set; }
        public int LocalId { get; set; }
        public int Quantity { get; set; }
    }
}
