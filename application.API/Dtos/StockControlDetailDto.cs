﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class StockControlDetailDto
    {
        public int Id { get; set; }
        public int StockControlId { get; set; }
        public int ProductId { get; set; }
        public int ActualStock { get; set; }
        public int CountedStock { get; set; }
        public string Name { get; set; }
        public string BarCode { get; set; }
    }
}
