﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class StocktakingDto
    {
        public DateTime Date { get; set; }
        public double Sales { get; set; }
        public double Expenses { get; set; }

    }
}
