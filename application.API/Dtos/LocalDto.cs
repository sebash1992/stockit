﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class LocalDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
