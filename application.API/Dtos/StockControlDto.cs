﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class StockControlDto
    {

        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Name { get; set; }
        public virtual ICollection<StockControlDetailDto> StockControlDetail { get; set; }
        public int LocalId { get; set; }
        public string TenantId { get; set; }

    }
}
