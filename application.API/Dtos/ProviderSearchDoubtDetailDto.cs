﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class ProviderSearchDoubtDetailDto
    {
        public DateTime Date { get; set; }
        public double Ammount { get; set; }
        public string Description { get; set; }
    }
}
