﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class RoleListDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
