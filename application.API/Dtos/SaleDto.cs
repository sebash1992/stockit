﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class SaleDto
    {
        public SaleDto()
        {
            this.ProductsSent = new List<SaleDetailDto>();
            this.ServicesSent = new List<SaleServiceDto>();
        }
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Subtotal { get; set; }
        public double Discount { get; set; }
        public double Total { get; set; }
        public double Cost { get; set; }
        public int PayMethod { get; set; }
        public int LocalId { get; set; }
        public IEnumerable<SaleDetailDto> ProductsSent { get; set; }
        public IEnumerable<SaleServiceDto> ServicesSent { get; set; }
    }
}
