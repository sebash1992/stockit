﻿using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class ReportDto
    {
        public List<StocktakingDto> mergeSalesAndExpenses(IEnumerable<Sale> sales, IEnumerable<Expense> expenses, ProviderSearchDoubtDto search)
        {
            List<StocktakingDto> tor = new List<StocktakingDto>();
            DateTime pivot = search.From;
            var salesGroup = sales.GroupBy(x => x.Date.Date).Select(y => new StocktakingResumeDto { Date = y.Key, Ammount = y.Sum(z => z.SubTotal - z.Discount) });
            var expensesGroup = expenses.GroupBy(x =>x.Date.Date).Select(y => new StocktakingResumeDto { Date = y.Key , Ammount = y.Sum(z => z.Amount) });
            while (pivot < search.To)
            {
                var sale = salesGroup.Where(s => s.Date.Date == pivot.Date).FirstOrDefault();
                var expense = expensesGroup.Where(s => s.Date.Date == pivot.Date).FirstOrDefault();

                StocktakingDto aux = new StocktakingDto()
                {
                    Date = pivot,
                    Sales = sale != null ? sale.Ammount : 0 ,
                    Expenses = expense != null ? expense.Ammount : 0
                };
                tor.Add(aux);
                pivot = pivot.AddDays(1);
                }
            return tor;
        }



    }
    public class StocktakingResumeDto
    {
        public DateTime Date { get; set; }
        public double Ammount { get; set; }
    }
}
