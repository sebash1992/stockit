﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class SaleDetailDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ProductBarCode { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public double UnityPrice { get; set; }
        public int SaleId { get; set; }
    }

    public class SaleServiceDto
    {
        public int Id { get; set; }
        public string Service { get; set; }
        public double Price { get; set; }
    }
}
