﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class ProductListDto
    {
        public int Id { get; set; }
        public string BarCode { get; set; }
        public string Name { get; set; }
        public double SalePrice { get; set; }

    }
}
