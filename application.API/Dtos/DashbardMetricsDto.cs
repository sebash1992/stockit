﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class DashbardMetricsDto
    {
        public double TodaySales { get; set; }
        public double TodayExpenses { get; set; }
        public int ProductWithMinimumStock { get; set; }
        public string ProductWithMostSales { get; set; }
    }
}
