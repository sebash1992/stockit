﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Dtos
{
    public class ProductStockDto
    {
        public string Local { get; set; }
        public int LocalId { get; set; }
        public int Quantity { get; set; }
    }
}
