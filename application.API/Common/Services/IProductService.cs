﻿using application.API.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Common.Services
{
    public interface IProductService : IActivableModelService<Product>
    {
        void UpdateSalePrice(int id, float salePrice);
        IEnumerable<Product> ProductWithMinimumStock(int localId);
        string ProductWithMostSales(int localId);
        string CreateProductFromFile(IFormFile file, int localId);
        string GenerateBarcode();
    }
}
