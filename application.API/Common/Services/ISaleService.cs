﻿using application.API.Dtos;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Common.Services
{
    public interface ISaleService : IActivableModelService<Sale>
    {
        double GetTodaySales(int localId);
        IEnumerable<GraphDto> GetSalesInPeriord(int id,int localId);
    }
}
