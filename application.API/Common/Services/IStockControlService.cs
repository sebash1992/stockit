﻿using application.API.Dtos;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Common.Services
{
    public interface IStockControlService : IActivableModelService<StockControl>
    {
        void CreateRandom(StockControl entity);
        void FinishControl(int id);
    }
}
