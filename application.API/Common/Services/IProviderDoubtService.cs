﻿using application.API.Dtos;
using application.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Common.Services
{
    public interface IProviderDoubtService : IService<ProviderDoubts>
    {
        IEnumerable<ProviderSearchDoubtDetailDto> GetProviderDoubt(ProviderSearchDoubtDto search);
    }
}
