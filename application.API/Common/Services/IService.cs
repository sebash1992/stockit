﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Common.Services
{
    public interface IService<T> : IReadonlyService<T> where T : /*class, */ IModel
    {
        /// <summary>
        /// Delete an entity
        /// </summary>
        /// <param name="id">entity Id</param>
        void Delete(object id);

        /// <summary>
        /// Create an entity
        /// </summary>
        /// <param name="entity">entity to create</param>
        void Create(T entity);

        /// <summary>
        /// Update an entity
        /// </summary>
        /// <param name="entity">entity to update</param>
        void Update(T entity);
    }
}
