﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Common.Services
{
    public interface IActivableModelService<T> : IService<T> where T : /*class, */ IActivableModel
    {
        /// <summary>
        /// Toggle IsActive property
        /// </summary>
        /// <param name="id">entity Id</param>
        void ToggleActive(object id);

    }
}
