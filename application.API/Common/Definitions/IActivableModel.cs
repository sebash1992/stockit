﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Common.Definitions
{
    public interface IActivableModel : IModel
    {
        /// <summary>
        /// Is the entity active?
        /// </summary>
        bool IsActive { get; set; }
    }
}
