﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Common.Definitions
{
    public interface IModel //marker
    {
        int Id { get; set; }
    }
}
