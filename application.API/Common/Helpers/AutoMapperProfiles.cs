﻿using application.API.Dtos;
using application.API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Common.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Product, ProductListDto>();

            CreateMap<Provider, ProviderListDto>().ReverseMap();

            CreateMap<Category, CategoryListDto>().ReverseMap();
            CreateMap<RegistrationDto, AppUser>().ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Email));

            CreateMap<PayMethod,PayMethodListDto>()
                .ForMember(dest => dest.Name, opt =>
                {
                    opt.MapFrom(src => src.Method);
                })
                .ReverseMap();

            CreateMap<Product, ProductDetailDto>()
                    .ForMember(dest => dest.CostPrice, opt =>
                    {
                        opt.MapFrom(src => src.Stock.LastOrDefault().CostPrice);
                    })
                    .ForMember(dest => dest.Stocks, opt =>
                    {
                        opt.MapFrom(src => src.Stock.GroupBy(d => d.Local)
                        .Select(
                            g => new ProductStockDto
                            {
                                Local = g.Key.Name,
                                LocalId = g.Last().LocalId,
                                Quantity = g.Sum(s => s.Quantity)
                            }));
                    });

            CreateMap<ProductDetailDto, Product>()
                    .ForMember(dest => dest.CategoryId, opt =>
                    {
                        opt.MapFrom(src => src.Category.Id);
                    })
                    .ForMember(dest => dest.ProviderId, opt =>
                    {
                        opt.MapFrom(src => src.Provider.Id);
                    })
                    .ForMember(x => x.Category, opt => opt.Ignore())
                    .ForMember(x => x.Provider, opt => opt.Ignore());

            CreateMap<ProductStockDto, Stock>()
                    .ForMember(x => x.Local, opt => opt.Ignore())
                    .ForMember(dest => dest.LocalId, opt =>
                    {
                        opt.MapFrom(src => src.LocalId);
                    })
                    .ForMember(dest => dest.Quantity, opt =>
                    {
                        opt.MapFrom(src => src.Quantity);
                    });
            CreateMap<Local, LocalDto>();

            CreateMap<ProductStockEditorDto, Stock>()
            .ForMember(x => x.Local, opt => opt.Ignore())
            .ForMember(x => x.Product, opt => opt.Ignore())
            .ForMember(dest => dest.LocalId, opt =>
            {
                opt.MapFrom(src => src.LocalId);
            })
            .ForMember(dest => dest.Quantity, opt =>
            {
                opt.MapFrom(src => src.Quantity);
            })
            .ForMember(dest => dest.EnterDate, opt =>
            {
                opt.MapFrom(src => DateTime.UtcNow);
            });

            CreateMap<SaleDto, Sale>()
            .ForMember(x => x.Local, opt => opt.Ignore())
            .ForMember(x => x.PayMethod, opt => opt.Ignore())
            .ForMember(dest => dest.Id, opt =>
            {
                opt.MapFrom(src => src.Id);
            })
            .ForMember(dest => dest.SubTotal, opt =>
            {
                opt.MapFrom(src => src.Subtotal);
            })
            .ForMember(dest => dest.Discount, opt =>
            {
                opt.MapFrom(src => src.Discount);
            })
            .ForMember(dest => dest.Total, opt =>
            {
                opt.MapFrom(src => src.Subtotal - src.Discount);
            })
            .ForMember(dest => dest.SaleDetail, opt =>
            {
                opt.MapFrom(src => src.ProductsSent);
            })
            .ForMember(dest => dest.SaleServiceDetail, opt =>
            {
                opt.MapFrom(src => src.ServicesSent);
            })
            .ForMember(dest => dest.PayMethodId, opt =>
            {
                opt.MapFrom(src => src.PayMethod);
            })
            .ForMember(dest => dest.LocalId, opt =>
            {
                opt.MapFrom(src => src.LocalId);
            }).ReverseMap();

            CreateMap<SaleDetailDto, SaleDetail>()
                .ForMember(x => x.Product, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt =>
                {
                    opt.MapFrom(src => src.Id);
                })
                .ForMember(dest => dest.SaleId, opt =>
                {
                    opt.MapFrom(src => src.SaleId);
                })
                .ForMember(dest => dest.Quantity, opt =>
                {
                    opt.MapFrom(src => src.Quantity);
                })
                .ForMember(dest => dest.ProductId, opt =>
                {
                    opt.MapFrom(src => src.ProductId);
                })
                .ForMember(dest => dest.Price, opt =>
                {
                    opt.MapFrom(src => src.UnityPrice);
                });


            CreateMap<SaleServiceDto, SaleServiceDetail>()
                .ForMember(dest => dest.IsActive, opt =>
                {
                    opt.MapFrom(src => true);
                });


            CreateMap<SaleDetail,SaleDetailDto > ()
                .ForMember(dest => dest.Id, opt =>
                {
                    opt.MapFrom(src => src.Id);
                })
                .ForMember(dest => dest.ProductId, opt =>
                {
                    opt.MapFrom(src => src.ProductId);
                })
                .ForMember(dest => dest.Quantity, opt =>
                {
                    opt.MapFrom(src => src.Quantity);
                })
                .ForMember(dest => dest.UnityPrice, opt =>
                {
                    opt.MapFrom(src => src.Price);
                })
                .ForMember(dest => dest.SaleId, opt =>
                {
                    opt.MapFrom(src => src.SaleId);
                })
                .ForMember(dest => dest.ProductBarCode, opt =>
                {
                    opt.MapFrom(src => src.Product.BarCode);
                })
                .ForMember(dest => dest.ProductName, opt =>
                {
                    opt.MapFrom(src => src.Product.Name);
                });

            CreateMap<SaleServiceDetail, SaleServiceDto>();

            CreateMap<Sale, SaleListDto>()
            .ForMember(dest => dest.Date, opt =>
            {
                opt.MapFrom(src => src.Date);
            })
            .ForMember(dest => dest.PayMethod, opt =>
            {
                opt.MapFrom(src => src.PayMethod.Method);
            });

            CreateMap<Provider, ProviderDetailDto>()
            .ForMember(dest => dest.Doubt, opt =>
            {
                opt.MapFrom(src => src.ProviderDoubts.Sum(d => d.Amount));
            });
            CreateMap<AppUser, UserDetailDto>()
                .ForMember(x => x.Password, opt => opt.Ignore());

            CreateMap<ProviderDetailDto, Provider>()
                .ForMember(x => x.Product, opt => opt.Ignore())
                .ForMember(x => x.ProviderDoubts, opt => opt.Ignore());

            CreateMap<ProviderDoubtDto, ProviderDoubts>()
                .ForMember(x => x.Provider, opt => opt.Ignore())
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(dest => dest.ProviderId, opt =>
                {
                    opt.MapFrom(src => src.ProviderId);
                })
                .ForMember(dest => dest.PaidDate, opt =>
                {
                    opt.MapFrom(src => DateTime.UtcNow);
                })
                .ForMember(dest => dest.Amount, opt =>
                {
                    opt.MapFrom(src => src.Amount);
                });

            CreateMap<ProviderDoubts, ProviderDoubtDto>();
            CreateMap<IdentityRole, RoleListDto>();
            CreateMap<UserDetailDto, AppUser>().ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Email));
            CreateMap<StockControlDetail, StockControlDetailDto>()
                .ForMember(au => au.BarCode, map => map.MapFrom(vm => vm.Product.BarCode))
                .ForMember(au => au.Name, map => map.MapFrom(vm => vm.Product.Name));
            CreateMap<StockControl, StockControlDto>();
            CreateMap<StockControlDetailDto, StockControlDetail>()
                .ForMember(x => x.Product, opt => opt.Ignore())
                .ForMember(x => x.StockControl, opt => opt.Ignore())
                .ForMember(au => au.IsActive, map => map.MapFrom(vm => true));
            CreateMap<StockControlDto, StockControl>()
                .ForMember(x => x.Local, opt => opt.Ignore())
                .ForMember(au => au.IsActive, map => map.MapFrom(vm => true));
        }
    }
}
