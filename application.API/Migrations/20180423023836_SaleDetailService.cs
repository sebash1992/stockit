﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace application.API.Migrations
{
    public partial class SaleDetailService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ProviderDoubts",
                maxLength: 64,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SaleServiceDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    SaleId = table.Column<int>(nullable: true),
                    Service = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleServiceDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SaleServiceDetail_Sale_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sale",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SaleServiceDetail_SaleId",
                table: "SaleServiceDetail",
                column: "SaleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SaleServiceDetail");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "ProviderDoubts");
        }
    }
}
