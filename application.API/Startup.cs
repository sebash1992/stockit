﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using application.API.Auth;
using application.API.Business.Repositories;
using application.API.Business.Services;
using application.API.Common.Definitions;
using application.API.Common.Helpers;
using application.API.Common.Repositories;
using application.API.Common.Services;
using application.API.Data;
using application.API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Http;

namespace application.API
{
    public class Startup
    {
        private const string SecretKey = "iNivDmHLpUA223sqsfhqGbMRdRj1PVkH"; // todo: get this from somewhere secure
        private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // services.AddDbContext<DataContext>(x => x.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<DataContext>(ServiceLifetime.Scoped);
            //   services.AddScoped(typeof(IRepository<Local>), typeof(LocalRepository));

            services.AddScoped<ILocalService, LocalService>();
            services.AddScoped<IRepository<Local>, LocalRepository>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IRepository<Product>, ProductRepository>();
            services.AddScoped<IProviderService, ProviderService>();
            services.AddScoped<IRepository<Provider>, ProviderRepository>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IRepository<Category>, CategoryRepository>();
            services.AddScoped<ILocalService, LocalService>();
            services.AddScoped<IRepository<Local>, LocalRepository>();
            services.AddScoped<IStockService, StockService>();
            services.AddScoped<IRepository<Stock>, StockRepository>();
            services.AddScoped<ISaleService, SaleService>();
            services.AddScoped<IRepository<Sale>, SalesRepository>();
            services.AddScoped<IPayMethodService, PayMethodService>();
            services.AddScoped<IReadOnlyRepository<PayMethod>, PayMethodRepository>();
            services.AddScoped<IProviderDoubtService, ProviderDoubtService>();
            services.AddScoped<IRepository<ProviderDoubts>, ProviderDoubtRepository>();
            services.AddScoped<ITenantService, TenantService>();
            services.AddScoped<IRepository<Tenant>, TenantRepository>();
            services.AddScoped<IExpenseService, ExpenseService>();
            services.AddScoped<IRepository<Expense>, ExpenseRepository>();
            services.AddScoped<IMonthlyExpenseService, MonthlyExpenseService>();
            services.AddScoped<IRepository<MonthlyExpenses>, MonthlyExpenseRepository>();
            services.AddScoped<IRepository<StockControlDetail>, StockControlDetailRepository>();
            services.AddScoped<IStockControlService, StockControlService>();
            services.AddScoped<IRepository<StockControl>, StockControlRepository>();
            services.AddSingleton<IJwtFactory, JwtFactory>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddAutoMapper();

            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            // Configure JwtIssuerOptions
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(configureOptions =>
            {
                configureOptions.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                configureOptions.TokenValidationParameters = tokenValidationParameters;
                configureOptions.SaveToken = true;
            });

            // api user claim policy
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiUser", policy => policy.RequireClaim(Constants.Strings.JwtClaimIdentifiers.Rol, Constants.Strings.JwtClaims.ApiAccess));
            });

            var builder = services.AddIdentityCore<AppUser>(o =>
            {
                // configure identity options
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 6;
            });
            builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole), builder.Services);
            builder.AddEntityFrameworkStores<DataContext>().AddDefaultTokenProviders();

            services.AddMvc();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
            app.UseMvc(routes => {
                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Fallback", action = "Index" }
                    );
            });
        }
    }
}
