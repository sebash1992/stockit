﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public partial class ProviderDoubts: IModel
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public DateTime PaidDate { get; set; }
        public double Amount { get; set; }
        [MaxLength(64)]
        public string Description { get; set; }
        public virtual Provider Provider { get; set; }
    }
}
