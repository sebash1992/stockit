﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public partial class SaleServiceDetail
    {
        public int Id { get; set; }
        public string Service { get; set; }
        public double Price { get; set; }
        public bool IsActive { get; set; }

        public virtual Sale Sale { get; set; }
    }
}
