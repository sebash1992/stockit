﻿using application.API.Common.Definitions;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public class AppUser : IdentityUser ,ITenantEnableModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TenantId { get; set; }
    }
}
