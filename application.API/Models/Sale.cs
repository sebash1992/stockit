﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public partial class Sale : IActivableModel, ITenantEnableModel
    {
        public Sale()
        {
            SaleDetail = new HashSet<SaleDetail>();
        }

        public int Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public double SubTotal { get; set; }
        public double Discount { get; set; }
        public double Total { get; set; }
        public int PayMethodId { get; set; }
        public double Cost { get; set; }
        public bool IsActive { get; set; }
        public int LocalId { get; set; }
        [StringLength(128)]
        public string TenantId { get; set; }
        public virtual ICollection<SaleDetail> SaleDetail { get; set; }
        public virtual ICollection<SaleServiceDetail> SaleServiceDetail { get; set; }
        public virtual Local Local { get; set; }
        public virtual PayMethod PayMethod { get; set; }
    }
}

