﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public class Product: IActivableModel, ITenantEnableModel
    {
        public Product()
        {
            SaleDetail = new HashSet<SaleDetail>();
            Stock = new HashSet<Stock>();
            StockControlDetail = new HashSet<StockControlDetail>();
        }

        public int Id { get; set; }
        [StringLength(128)]
        [Required]
        public string BarCode { get; set; }
        [Required]
        [StringLength(64)]
        public string Name { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public int ProviderId { get; set; }
        public string ProductLink { get; set; }
        public double SalePrice { get; set; }
        public int MinQuantity { get; set; }
        public bool IsActive { get; set; }
        [StringLength(128)]
        public string TenantId { get; set; }

        public virtual ICollection<SaleDetail> SaleDetail { get; set; }
        public virtual ICollection<Stock> Stock { get; set; }
        public virtual ICollection<StockControlDetail> StockControlDetail { get; set; }
        public virtual Category Category { get; set; }
        public virtual Provider Provider { get; set; }
    }
}
