﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public partial class Stock : IActivableModel
    {
        public int Id { get; set; }
        public DateTime EnterDate { get; set; }
        public int Quantity { get; set; }
        public double CostPrice { get; set; }
        public int ProductId { get; set; }
        public bool IsActive { get; set; }
        public int LocalId { get; set; }

        public virtual Local Local { get; set; }
        public virtual Product Product { get; set; }
    }
}
