﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public partial class StockControl :IActivableModel, ITenantEnableModel
    {
        public StockControl()
        {
            StockControlDetail = new HashSet<StockControlDetail>();
        }

        public int Id { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(64)]
        [Required]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsFinished { get; set; }
        [Required]
        public int LocalId { get; set; }
        [StringLength(128)]
        public string TenantId { get; set; }

        public virtual ICollection<StockControlDetail> StockControlDetail { get; set; }
        public virtual Local Local { get; set; }
    }
}
