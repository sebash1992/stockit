﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public partial class StockControlDetail:IModel
    {
        public int Id { get; set; }
        public int StockControlId { get; set; }
        public int ProductId { get; set; }
        public int ActualStock { get; set; }
        public bool IsActive { get; set; }
        public int CountedStock { get; set; }

        public virtual Product Product { get; set; }
        public virtual StockControl StockControl { get; set; }
    }
}
