using application.API.Common.Definitions;
using System.ComponentModel.DataAnnotations;

namespace application.API.Models
{
    public class Local: IActivableModel, ITenantEnableModel
    {
        public int Id {get;set;}
        [StringLength(64)]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        [StringLength(128)]
        public string TenantId { get; set; }

    }
}