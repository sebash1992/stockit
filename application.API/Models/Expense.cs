﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public partial class Expense : IModel, ITenantEnableModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        [StringLength(64)]
        [Required]
        public string Description { get; set; }
        public bool IsMonthly { get; set; }
        public int LocalId { get; set; }
        public virtual Local Local { get; set; }
        [StringLength(128)]
        public string TenantId { get; set; }
    }
}
