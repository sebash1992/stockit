﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public partial class PayMethod: IModel
    {
        public PayMethod()
        {
            Sale = new HashSet<Sale>();
        }

        public int Id { get; set; }
        [StringLength(64)]
        [Required]
        public string Method { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<Sale> Sale { get; set; }
    }
}
