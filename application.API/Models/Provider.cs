﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public partial class Provider: IActivableModel, ITenantEnableModel
    {
        public Provider()
        {
            Product = new HashSet<Product>();
            ProviderDoubts = new HashSet<ProviderDoubts>();
        }

        public int Id { get; set; }
        [StringLength(64)]
        [Required]
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public bool IsActive { get; set; }
        [StringLength(128)]
        public string TenantId { get; set; }

        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<ProviderDoubts> ProviderDoubts { get; set; }
    }
}
