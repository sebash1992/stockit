using application.API.Common.Definitions;
using System;
using System.ComponentModel.DataAnnotations;

namespace application.API.Models
{
    public partial class Tenant : IActivableModel
    {
            public int Id {get;set;}
            [StringLength(128)]
            public string TenantId { get; set; }
            [StringLength(64)]
            public string TenantName { get; set; }
            public bool IsActive {get; set;}
    }
}