﻿using application.API.Common.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace application.API.Models
{
    public partial class Category : IActivableModel, ITenantEnableModel
    {
        public Category()
        {
            Product = new HashSet<Product>();
        }

        public int Id { get; set; }
        [StringLength(64)]
        [Required]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        [StringLength(128)]
        public string TenantId { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
