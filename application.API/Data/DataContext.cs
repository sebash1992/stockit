using application.API.Common.Helpers;
using application.API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using System;

namespace application.API.Data
{
    public class DataContext: IdentityDbContext<AppUser>
    {
        private string tenantId;
        private SqlConnection connection;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _context;
        public DataContext(IHttpContextAccessor context,DbContextOptions<DataContext>options, IConfiguration configuration) :base(options)
        {
            _configuration = configuration;
            _context = context;
            tenantId = "";
        }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Expense> Expense { get; set; }
        public virtual DbSet<Local> Local { get; set; }
        public virtual DbSet<MonthlyExpenses> MonthlyExpenses { get; set; }
        public virtual DbSet<PayMethod> PayMethod { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Provider> Provider { get; set; }
        public virtual DbSet<ProviderDoubts> ProviderDoubts { get; set; }
        public virtual DbSet<Sale> Sale { get; set; }
        public virtual DbSet<SaleDetail> SaleDetail { get; set; }
        public virtual DbSet<SaleServiceDetail> SaleServiceDetail { get; set; }
        public virtual DbSet<Stock> Stock { get; set; }
        public virtual DbSet<StockControl> StockControl { get; set; }
        public virtual DbSet<StockControlDetail> StockControlDetail { get; set; }
        public virtual DbSet<Tenant> Tenant { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = _configuration.GetConnectionString("DefaultConnection");
            connection = new SqlConnection(connectionString);
           connection.StateChange += Connection_StateChange;

            optionsBuilder.UseSqlServer(connection);

            base.OnConfiguring(optionsBuilder);
        }

        private void Connection_StateChange(object sender, StateChangeEventArgs e)
        {
            if (e.CurrentState == ConnectionState.Open)
            {

                var tenant = _context.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.TenantId);
                var selectedtenant = _context.HttpContext.User.FindFirst(Constants.Strings.JwtClaimIdentifiers.SelectedTenant);
                if (selectedtenant != null)
                {
                    tenantId = selectedtenant.Value;
                }
                else if (tenant != null)
                {
                    tenantId = tenant.Value;
                }


                var cmd = connection.CreateCommand();
                cmd.CommandText = @"EXEC sp_set_session_context @key=N'TenantId', @value='"+ tenantId+"'";
                //cmd.Parameters.AddWithValue("@tenantid", tenantId);
                cmd.ExecuteNonQuery();
            }
        }



    }
}